Calka_NL <- function(FUN,a,b)
{
  return(FUN(b) - FUN(a))
}

MetodaProstokatow <- function(fun,a,b,n)
{
  dx = (b - a) / n
  
  suma = 0
  for(i in 1 : n)
  {
    suma = suma + (fun(a + i/n * (b - a)))
  }

  return(dx*suma)
}

MetodaTrapezow <- function(fun,a,b,n)
{
  dx = (b - a) / n
  
  suma = 0
  for(i in 1 : (n-1))
  {
    suma = suma + fun(a + i/n * (b - a)) 
  }
  
  return(dx*(suma + (fun(a) + fun(b)) / 2))
}

Blad <- function(Pierwotna, Metoda)
{
  return(abs(Pierwotna - Metoda))
}

#Przyklad 1
Test1 <- function(x)
{
  return(x^9 + 2*x^8 + 3*x^7 + 4*x^6 + 5*x^5 + 6*x^4 + 7*x^3 + 8*x^2 + 9*x)
}

Pierwotna1 <- function(x)
{
  return(1/10*x^10 + 2/9*x^9 + 3/8*x^8 + 4/7*x^7 + 5/6*x^6 + 6/5*x^5 + 7/4*x^4 + 8/3*x^3 + 9/2*x^2)
}

#duzy, maly, duzy, maly, pierwotna
Wyniki1 <- c(MetodaProstokatow(Test1,-1,1,0.1),MetodaProstokatow(Test1,-1,1,10),
             MetodaTrapezow(Test1,-1,1,0.1),MetodaTrapezow(Test1,-1,1,10), 
             Calka_NL(Pierwotna1,-1,1))
Blad1 <- c(Blad(Wyniki1[5],Wyniki1[1]),
           Blad(Wyniki1[5],Wyniki1[2]),
           Blad(Wyniki1[5],Wyniki1[3]),
           Blad(Wyniki1[5],Wyniki1[4]))
#Przyklad 2
Test2 <- function(x)
{
  return(2*sin(x)*cos(x))
}

Pierwotna2 <- function(x)
{
  return(1/2 * sin(2*x))
}



#Przyklad 3
Test1 <- function(x)
{
  return(exp(x))
}

Pierwotna1 <- function(x)
{
  return(exp(x))
}
