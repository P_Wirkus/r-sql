#Instalowanie potrzebnych pakietow
library(MASS)
library(base)
install.packages("dplyr")
library(dplyr)
install.packages("glmnet")
library(glmnet)
library(ISLR)
install.packages("glmnetUtils")
library(glmnetUtils)

#wczytanie danych
dane=Boston
head(dane)

#1. Zmienna zliczajaca
ile_domow=function(indeks){
  if (indeks %in% dane$rad) {
    zl=table(dane$rad)
    ktore=which(names(zl)==indeks,arr.ind = TRUE) #ktore zliczenie odpowiada podanemu wydawcy
    wynik=zl[[ktore]]
    cat("Ilosc domow w Bostonie posiadajacych indeks dostepnosci do autostrad rowny", indeks, "wynosi:", wynik)
  } else {
    cat("Ilosc domow w Bostonie posiadajacych indeks dostepnosci do autostrad rowny", indeks, "wynosi:", 0)}
  }

ile_domow(1)
ile_domow(5)
ile_domow(10)
ile_domow(20)


#2. Zmienna zliczajaca przedzialowo
ilosc_pokoi=function(ile){
  x = dane$rm
  y=cut(x,breaks=c(3,5,7,9))
  levels(y)=c("mala", "srednia", "duza")
  podzial=table(y)
  ktore=which(names(podzial)==ile, arr.ind = TRUE) #ktore zliczenie odpowiada podanemu wydawcy
  wynik=podzial[[ktore]] 
  cat( ile, "ilosc pokoi wystepuje w", wynik, "terenach na przedmiesciach Bostonu")
}
ilosc_pokoi("mala")
ilosc_pokoi("srednia")
ilosc_pokoi("duza")


#3. Analiza danych - testowanie hipotez
# Na poziomie istotnosci alpha=0.05 zweryfikujemy hipoteze, ze wartosc przecietna nieruchomosci w Bostonie 
#wynosi H0:m=25 tys. dolarow wobec hipotezy alternatywnej HA:m<25tys. dolarow.

#Dane
n<-length(dane$medv)
x<-mean(dane$medv)
s<-sd(dane$medv)
mu=25
alpha=0.05

#Obliczanie statystyki testowej
z=(x-mu)*sqrt(n)/s
z

# Wyznaczenie zbioru krytycznego
cat("W = (-infty,",qnorm(1-alpha),"]")

#Weryfikowanie hipotezy na podstawie zbioru odrzucen
if(z< (-qnorm(1-alpha))){
  cat("Mamy podstawy do odrzucenia H0")
}else{
  cat("Nie mamy podstaw do odrzucenia H0")
}
#Czyli wartosc przecietna jest mniejsza niz 25 tys. dolarow

#Zamiast statystyki testowej mozemy wyznaczyc p-value
p_value=pnorm(z)
cat("p-value = ",p_value)

#Weryfikowanie hipotezy na podstawie p-value
if(p_value<alpha){
  cat("Mamy podstawy do odrzucenia H0")
}else{
  cat("Nie mamy podstaw do odrzucenia H0")
}
#Ten sam wynik co wczesniej

#Przedstawienie zbioru krytycznego na wykresie
pn=seq(-5,5,length=1000)
plot(pn, dnorm(pn,mean=0,sd=1),type="l",xlab="", ylab="",main="Zbior krytyczny")
text(0.1,0.1,expression(1-alpha),cex=2)
i<-pn<(-qnorm(1-alpha))
polygon(c(pn[i],-qnorm(1-alpha)),c(dnorm(pn,0,1)[i],0),col="gray")


#4. Analiza danych - model regresji

#wybor probki do zbioru treningowego i testowego
set.seed(123)
sampl <- sample(seq_len(nrow(dane)), size = floor(0.7*nrow(dane)))
trening=dane[sampl,]
test=dane[-sampl,]

#regresja liniowa
model=lm(medv~.,data=trening)

summary(model)

y_lm_test=predict.lm(model,test)
y_lm_trening=predict.lm(model,trening)

#regresja lasso
lasso_model=cv.glmnet(medv~.,data=trening,alpha=1)

coef(lasso_model,s=0.02)

print(lasso_model)
plot(lasso_model)

y_lasso_test=predict(lasso_model,test,s=0.02)
y_lasso_trening=predict(lasso_model,trening,s=0.02)

#regresja ridge
ridge_model=cv.glmnet(medv~.,data=trening,alpha=0)

print(ridge_model)
plot(ridge_model)
y_ridge_test=predict(ridge_model,test,s=0.7)
y_ridge_trening=predict(ridge_model,trening,s=0.7)

#MSE dla regresji liniowej,ridge i lasso na zbiorze testowym
print(mean((y_lm_test-test$medv)^2))
print(mean((y_lasso_test-test$medv)^2))
print(mean((y_ridge_test-test$medv)^2))

#MSE dla regresji liniowej,ridge i lasso na zbiorze treningowym
print(mean((y_lm_trening-trening$medv)^2))
print(mean((y_lasso_trening-trening$medv)^2))
print(mean((y_ridge_trening-trening$medv)^2))
