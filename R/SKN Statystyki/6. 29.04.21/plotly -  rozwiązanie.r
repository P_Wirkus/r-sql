#####plotly#####
#cheatsheet
#https://images.plot.ly/plotly-documentation/images/r_cheat_sheet.pdf
#wi�cej o ustawieniach wykresu https://plotly.com/r/reference/#scatter
library(plotly)
fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length)
fig


#####wykres punktowy####
fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length,
               marker = list(size = 10,
                             color = 'rgba(255, 182, 193, .9)',
                             line = list(color = 'rgba(152, 0, 0, .8)',
                                         width = 2)))
fig <- fig %>% layout(title = 'Styled Scatter',
                      yaxis = list(zeroline = FALSE),
                      xaxis = list(zeroline = FALSE))

fig

#serie danych
trace_0 <- rnorm(100, mean = 5)
trace_1 <- rnorm(100, mean = 0)
trace_2 <- rnorm(100, mean = -5)
x <- c(1:100)

data <- data.frame(x, trace_0, trace_1, trace_2)

fig <- plot_ly(data, x = ~x)
fig <- fig %>% add_trace(y = ~trace_0, name = 'trace 0',mode = 'lines')
#fig <- plot_ly(data, x = ~x, y = ~trace_0, name = 'trace 0', type = 'scatter', mode = 'lines')
fig <- fig %>% add_trace(y = ~trace_1, name = 'trace 1', mode = 'lines+markers')
fig <- fig %>% add_trace(y = ~trace_2, name = 'trace 2', mode = 'markers')

fig

#dodatkowe skale
fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length, color = ~Species)

fig
#colorbrewer
fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length, color = ~Species, colors = "Set2")

fig
#w�asna skala kolor�w
pal <- c("red", "blue", "green")
pal <- setNames(pal, c("virginica", "setosa", "versicolor"))

fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length, color = ~Species, colors = pal)

fig
#kszta�ty
fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length, type = 'scatter',
               mode = 'markers', symbol = ~Species, symbols = c('circle','x','o'),
               color = I('black'), marker = list(size = 10))

fig
#��czenie skal
d <- diamonds[sample(nrow(diamonds), 1000), ]

fig <- plot_ly(
  d, x = ~carat, y = ~price,
  color = ~carat, size = ~carat
)

fig
#tekst na podpisach
fig <- plot_ly(
  d, x = ~carat, y = ~price,
  # Hover text:
  text = ~paste("Price: ", price, '$<br>Cut:', cut),
  color = ~carat, size = ~carat
)

fig
#####wykres liniowy#####
x <- c(1:100)
random_y <- rnorm(100, mean = 0)
data <- data.frame(x, random_y)

fig <- plot_ly(data, x = ~x, y = ~random_y, type = 'scatter', mode = 'lines')

fig

#stylizacja wykres�w liniowych
month <- c('January', 'February', 'March', 'April', 'May', 'June', 'July',
           'August', 'September', 'October', 'November', 'December')
high_2000 <- c(32.5, 37.6, 49.9, 53.0, 69.1, 75.4, 76.5, 76.6, 70.7, 60.6, 45.1, 29.3)
low_2000 <- c(13.8, 22.3, 32.5, 37.2, 49.9, 56.1, 57.7, 58.3, 51.2, 42.8, 31.6, 15.9)
high_2007 <- c(36.5, 26.6, 43.6, 52.3, 71.5, 81.4, 80.5, 82.2, 76.0, 67.3, 46.1, 35.0)
low_2007 <- c(23.6, 14.0, 27.0, 36.8, 47.6, 57.7, 58.9, 61.2, 53.3, 48.5, 31.0, 23.6)
high_2014 <- c(28.8, 28.5, 37.0, 56.8, 69.7, 79.7, 78.5, 77.8, 74.1, 62.6, 45.3, 39.9)
low_2014 <- c(12.7, 14.3, 18.6, 35.5, 49.9, 58.0, 60.0, 58.6, 51.7, 45.2, 32.2, 29.1)

data <- data.frame(month, high_2000, low_2000, high_2007, low_2007, high_2014, low_2014)

#Kolejno�� b�dzie alfabetyczna, chyba �e okre�limy to jak poni�ej:
data$month <- factor(data$month, levels = data[["month"]])

fig <- plot_ly(data, x = ~month, y = ~high_2014, name = 'High 2014', type = 'scatter', mode = 'lines',
               line = list(color = 'rgb(205, 12, 24)', width = 4)) 
fig <- fig %>% add_trace(y = ~low_2014, name = 'Low 2014', line = list(color = 'rgb(22, 96, 167)', width = 4)) 
fig <- fig %>% add_trace(y = ~high_2007, name = 'High 2007', line = list(color = 'rgb(205, 12, 24)', width = 4, dash = 'dash')) 
fig <- fig %>% add_trace(y = ~low_2007, name = 'Low 2007', line = list(color = 'rgb(22, 96, 167)', width = 4, dash = 'dash')) 
fig <- fig %>% add_trace(y = ~high_2000, name = 'High 2000', line = list(color = 'rgb(205, 12, 24)', width = 4, dash = 'dot')) 
fig <- fig %>% add_trace(y = ~low_2000, name = 'Low 2000', line = list(color = 'rgb(22, 96, 167)', width = 4, dash = 'dot')) 
fig <- fig %>% layout(title = "Average High and Low Temperatures in New York",
                      xaxis = list(title = "Months"),
                      yaxis = list (title = "Temperature (degrees F)"))

fig
#rodzaje linii
x <- c(1:5)
y <- c(1, 3, 2, 3, 1)

fig <- plot_ly(x = ~x) 
fig <- fig %>% add_lines(y = ~y, name = "linear", line = list(shape = "linear")) 
fig <- fig %>% add_lines(y = y + 5, name = "spline", line = list(shape = "spline")) 
fig <- fig %>% add_lines(y = y + 10, name = "vhv", line = list(shape = "vhv")) 
fig <- fig %>% add_lines(y = y + 15, name = "hvh", line = list(shape = "hvh")) 
fig <- fig %>% add_lines(y = y + 20, name = "vh", line = list(shape = "vh")) 
fig <- fig %>% add_lines(y = y + 25, name = "hv", line = list(shape = "hv"))

fig

#wype�nienie mi�dzy liniami
month <- c('January', 'February', 'March', 'April', 'May', 'June', 'July',
           'August', 'September', 'October', 'November', 'December')
high_2014 <- c(28.8, 28.5, 37.0, 56.8, 69.7, 79.7, 78.5, 77.8, 74.1, 62.6, 45.3, 39.9)
low_2014 <- c(12.7, 14.3, 18.6, 35.5, 49.9, 58.0, 60.0, 58.6, 51.7, 45.2, 32.2, 29.1)
data <- data.frame(month, high_2014, low_2014)
data$average_2014 <- rowMeans(data[,c("high_2014", "low_2014")])

#Kolejno�� b�dzie alfabetyczna, chyba �e okre�limy to jak poni�ej:
data$month <- factor(data$month, levels = data[["month"]])

fig <- plot_ly(data, x = ~month, y = ~high_2014, type = 'scatter', mode = 'lines',
               line = list(color = 'transparent'),
               showlegend = FALSE, name = 'High 2014') 
fig <- fig %>% add_trace(y = ~low_2014, type = 'scatter', mode = 'lines',
                         #one of ( "none" | "tozeroy" | "tozerox" | "tonexty" | "tonextx" | "toself" | "tonext" )
                         fill = 'tonexty', fillcolor='rgba(0,100,80,0.2)', line = list(color = 'transparent'),
                         showlegend = FALSE, name = 'Low 2014') 
fig <- fig %>% add_trace(x = ~month, y = ~average_2014, type = 'scatter', mode = 'lines',
                         line = list(color='rgb(0,100,80)'),
                         name = 'Average') 
fig
#fig <- fig %>% layout(title = "Average, High and Low Temperatures in New York",
#                      paper_bgcolor='rgb(255,255,255)', plot_bgcolor='rgb(229,229,229)',
#                      xaxis = list(title = "Months",
#                                   gridcolor = 'rgb(255,255,255)',
#                                   showgrid = TRUE,
#                                   showline = FALSE,
#                                   showticklabels = TRUE,
#                                   tickcolor = 'rgb(127,127,127)',
#                                   ticks = 'outside',
#                                   zeroline = FALSE),
#                      yaxis = list(title = "Temperature (degrees F)",
#                                   gridcolor = 'rgb(255,255,255)',
#                                   showgrid = TRUE,
#                                   showline = FALSE,
#                                   showticklabels = TRUE,
#                                   tickcolor = 'rgb(127,127,127)',
#                                   ticks = 'outside',
#                                   zeroline = FALSE))


# wykres g�sto�ci
dens <- with(diamonds, tapply(price, INDEX = cut, density))
df <- data.frame(
  x = unlist(lapply(dens, "[[", "x")),
  y = unlist(lapply(dens, "[[", "y")),
  cut = rep(names(dens), each = length(dens[[1]]$x))
)

fig <- plot_ly(df, x = ~x, y = ~y, color = ~cut) 
fig <- fig %>% add_lines()

fig

#####wykres s�upkowy####
fig <- plot_ly(
  x = c("giraffes", "orangutans", "monkeys"),
  y = c(20, 14, 23),
  name = "SF Zoo",
  type = "bar"
)

fig
#zgrypowane kolumny
Animals <- c("giraffes", "orangutans", "monkeys")
SF_Zoo <- c(20, 14, 23)
LA_Zoo <- c(12, 18, 29)
data <- data.frame(Animals, SF_Zoo, LA_Zoo)

fig <- plot_ly(data, x = ~Animals, y = ~SF_Zoo, type = 'bar', name = 'SF Zoo')
fig <- fig %>% add_trace(y = ~LA_Zoo, name = 'LA Zoo')
fig <- fig %>% layout(yaxis = list(title = 'Count'), barmode = 'group')

fig
#skumlowany
fig <- plot_ly(data, x = ~Animals, y = ~SF_Zoo, type = 'bar', name = 'SF Zoo')
fig <- fig %>% add_trace(y = ~LA_Zoo, name = 'LA Zoo')
fig <- fig %>% layout(yaxis = list(title = 'Count'), barmode = 'stack')

fig
#etykiety
x <- c('Product A', 'Product B', 'Product C')
y <- c(20, 14, 23)
text <- c('27% market share', '24% market share', '19% market share')
data <- data.frame(x, y, text)

fig <- plot_ly(data, x = ~x, y = ~y, type = 'bar',
               text = y, textposition = 'auto',
               marker = list(color = 'rgb(158,202,225)',
                             line = list(color = 'rgb(8,48,107)', width = 1.5)))
fig <- fig %>% layout(title = "January 2013 Sales Report",
                      xaxis = list(title = ""),
                      yaxis = list(title = ""))

fig
#kolor s�upk�w
x <- c('Feature A', 'Feature B', 'Feature C', 'Feature D', 'Feature E')
y <- c(20, 14, 23, 25, 22)
data <- data.frame(x, y)

fig <- plot_ly(data, x = ~x, y = ~y, type = 'bar', color = I("black"))
fig <- fig %>% layout(title = "Features",
                      xaxis = list(title = ""),
                      yaxis = list(title = ""))

fig
#kolor pojedynczych kolumn
fig <- plot_ly(data, x = ~x, y = ~y, type = 'bar',
               marker = list(color = c('rgba(204,204,204,1)', 'rgba(222,45,38,0.8)',
                                       'rgba(204,204,204,1)', 'rgba(204,204,204,1)',
                                       'rgba(204,204,204,1)')))
fig <- fig %>% layout(title = "Least Used Features",
                      xaxis = list(title = ""),
                      yaxis = list(title = ""))

fig
#dodawanie w�asnych kolumn
fig <- plot_ly()
fig <- fig %>% add_bars(
  x = c("2016", "2017", "2018"),
  y = c(500,600,700),
  base = c(-500,-600,-700),
  marker = list(
    color = 'red'
  ),
  name = 'expenses'
)
fig <- fig %>% add_bars(
  x = c("2016", "2017", "2018"),
  y = c(300,400,700),
  base = 0,
  marker = list(
    color = 'blue'
  ),
  name = 'revenue'
)

fig
#kolumny hotyzontalnie
fig <- plot_ly(x = c(20, 14, 23), y = c('giraffes', 'orangutans', 'monkeys'), type = 'bar', orientation = 'h')

fig
#skala kolor�w
library(dplyr)

fig <- ggplot2::diamonds
fig <- fig %>% count(cut, clarity)
fig <- fig %>% plot_ly(x = ~cut, y = ~n, color = ~clarity)

fig
#gantt chart
  #pobieramy dane
df <- read.csv("https://cdn.rawgit.com/plotly/datasets/master/GanttChart-updated.csv", stringsAsFactors = F)
df$Start <- as.Date(df$Start, format = "%m/%d/%Y")
client = "Sample Client"
  #wybieramy kolory na podtsawie liczby zasob�w
cols <- RColorBrewer::brewer.pal(length(unique(df$Resource)), name = "Set3")
df$color <- factor(df$Resource, labels = cols)
  #pusty wykres
fig <- plot_ly()
for(i in 1:(nrow(df) - 1)){
  fig <- add_trace(fig,
                   x = c(df$Start[i], df$Start[i] + df$Duration[i]),  # x0, x1
                   y = c(i, i),  # y0, y1
                   mode = "lines",
                   line = list(color = df$color[i], width = 20),
                   showlegend = F,
                   hoverinfo = "text",
                   
                   # tekst
                   
                   text = paste("Task: ", df$Task[i], "<br>",
                                "Duration: ", df$Duration[i], "days<br>",
                                "Resource: ", df$Resource[i]),
                   evaluate = T 
  )
}

fig
#####wykresy 3d#####
#punktowy
mtcars$am[which(mtcars$am == 0)] <- 'Automatic'
mtcars$am[which(mtcars$am == 1)] <- 'Manual'
mtcars$am <- as.factor(mtcars$am)

fig <- plot_ly(mtcars, x = ~wt, y = ~hp, z = ~qsec, color = ~am, colors = c('#BF382A', '#0C4B8E'))
fig <- fig %>% add_markers()
fig <- fig %>% layout(scene = list(xaxis = list(title = 'Weight'),
                                   yaxis = list(title = 'Gross horsepower'),
                                   zaxis = list(title = '1/4 mile time')))

fig
#liniowy
data <- read.csv('https://raw.githubusercontent.com/plotly/datasets/master/3d-line1.csv')
data$color <- as.factor(data$color)

fig <- plot_ly(data, x = ~x, y = ~y, z = ~z, type = 'scatter3d', mode = 'lines',
               opacity = 1, line = list(width = 6, color = ~color, reverscale = FALSE))

fig
#powierzchnia
fig <- plot_ly(z = ~volcano)
fig <- fig %>% add_surface()

fig
#####customizacja osi#####
#etykiety
a <- list(
  autotick = FALSE,
  ticks = "outside",
  tick0 = 0,
  dtick = 0.25,
  ticklen = 5,
  tickwidth = 2,
  tickcolor = toRGB("blue")
)
s <- seq(1, 4, by = 0.25)
fig <- plot_ly(x = ~s, y = ~s)
fig <- fig %>% layout(xaxis = a, yaxis = a)
fig
#format
f1 <- list(
  family = "Arial, sans-serif",
  size = 18,
  color = "lightgrey"
)
f2 <- list(
  family = "Old Standard TT, serif",
  size = 14,
  color = "black"
)
a <- list(
  title = "AXIS TITLE",
  titlefont = f1,
  showticklabels = TRUE,
  tickangle = 45,
  tickfont = f2,
  exponentformat = "E"
)

s <- seq(1e6, 1e7, length.out = 10)
fig <- plot_ly(x = ~s, y = ~s)
fig <- fig %>% add_markers()
fig <- fig %>% add_markers(y = ~rev(s))
fig <- fig %>% layout(xaxis = a, yaxis = a, showlegend = FALSE)

fig

#osie wsp�rz�dnych
ax <- list(
  zeroline = TRUE,
  showline = TRUE,
  mirror = "ticks",
  gridcolor = toRGB("gray50"),
  gridwidth = 2,
  zerolinecolor = toRGB("red"),
  zerolinewidth = 4,
  linecolor = toRGB("black"),
  linewidth = 6
)
s <- seq(-1, 4)
fig <- plot_ly(x = ~s, y = ~s)
fig <- fig %>% layout(xaxis = ax, yaxis = ax)

fig
#chowanie osi
ax <- list(
  title = "",
  zeroline = FALSE,
  showline = FALSE,
  showticklabels = FALSE,
  showgrid = FALSE
)

fig <- plot_ly(x = c(1, 2), y = c(1, 2))
fig <- fig %>% layout(xaxis = ax, yaxis = ax)

fig
#o� odwrotna
fig <- plot_ly(x = c(1, 2), y = c(1, 2))
fig <- fig %>% layout(xaxis = list(autorange = "reversed"))

fig
#skale logarytmiczne
s <- seq(1, 8)
fig <- plot_ly(x = ~s)
fig <- fig %>% add_trace(y = ~exp(s), name = "exponential")
fig <- fig %>% add_trace(y =  ~s, name = "linear")
fig <- fig %>% layout(yaxis = list(type = "log"))

fig

#podkategorie - trick polega na utworzeniu wci�cia na adnotacje i dodanie ich
fig <- plot_ly(orientation='h', line=list(color='gray'), height=400, width=600)
fig <- fig %>% add_boxplot(x=c(2,3,1,5), y=c('A','A','A','A'), name='A')
fig <- fig %>% add_boxplot(x=c(8,3,6,5), y=c('B','B','B','B'), name='B')
fig <- fig %>% add_boxplot(x=c(2,3,2,5), y=c('C','C','C','C'), name='C')
fig <- fig %>% add_boxplot(x=c(7.5,3,6,4), y=c('D','D','D','D'), name='D')
fig <- fig %>% layout(
  title = '',
  yaxis = list(
    autorange = TRUE, 
    categoryorder = "category descending", 
    domain = c(0, 1), 
    range = c(-0.5, 3.5), 
    showline = TRUE, 
    title = "", 
    type = "category"
  ),
  margin = list(
    r = 10, 
    t = 25, 
    b = 40, 
    l = 110
  ),
  legend = list(
    x = 0.986145833333, 
    y = 0.936263886049
  ), 
  shapes = list(
    list(
      line = list(
        color = "rgba(68, 68, 68, 0.5)", 
        width = 1
      ), 
      type = "line", 
      x0 = -0.3, 
      x1 = 1.2, 
      xref = "paper", 
      y0 = 0.5, 
      y1 = 0.5, 
      yref = "paper"
    ), 
    list(
      line = list(
        color = "rgba(68, 68, 68, 0.63)", 
        width = 1
      ), 
      type = "line", 
      x0 = -0.3, 
      x1 = 1.2, 
      xref = "paper", 
      y0 = 1, 
      y1 = 1, 
      yref = "paper"
    )
  ),
  annotations = list(
    list(
      x = -0.0951769406393, 
      y = 1.06972670892, 
      showarrow = FALSE, 
      text = "Subgroup", 
      xref = "paper", 
      yref = "paper"
    ), 
    list(
      x = -0.235516552511, 
      y = 1.07060587474, 
      showarrow = FALSE, 
      text = "Group", 
      xref = "paper", 
      yref = "paper"
    ), 
    list(
      x = -0.235516552511, 
      y = 0.922906017856, 
      showarrow = FALSE, 
      text = "One", 
      xref = "paper", 
      yref = "paper"
    ), 
    list(
      x = -0.235516552511, 
      y = 0.375, 
      showarrow = FALSE, 
      text = "Two", 
      xref = "paper", 
      yref = "paper"
    )
  )
)

fig

#####modyfikowanie legendy#####
#nazwy
library(tidyr)
library(plyr)

data <- spread(Orange, Tree, circumference)
data <- rename(data, c("1" = "Tree1", "2" = "Tree2", "3" = "Tree3", "4" = "Tree4", "5" = "Tree5"))

fig <- plot_ly(data, x = ~age, y = ~Tree1, type = 'scatter', mode = 'lines', name = 'Tree 1')
fig <- fig %>% add_trace(y = ~Tree2, name = 'Tree 2')
fig <- fig %>% add_trace(y = ~Tree3, name = 'Tree 3'#, showlegend = FALSE
                         )
fig <- fig %>% add_trace(y = ~Tree4, name = 'Tree 4')
fig <- fig %>% add_trace(y = ~Tree5, name = 'Tree 5')
fig
#ukrycie legendy
#fig <- fig %>% layout(showlegend = FALSE) 
# pozycja legendy
fig <- fig %>% layout(legend = list(x = 0.1, y = 0.9))
fig <- fig %>% layout(legend = list(x = 100, y = 0.5))
#orientacja legendy
fig <- fig %>% layout(legend = list(orientation = 'h'))
#stylizacja
l <- list(
  font = list(
    family = "sans-serif",
    size = 12,
    color = "#000"),
  bgcolor = "#E2E2E2",
  bordercolor = "#FFFFFF",
  borderwidth = 2)
fig <- fig %>% layout(legend = l)
#nazwa
fig <- fig %>% layout(legend=list(title=list(text='<b> Trend </b>')))
fig

#####tickmode#####
#linear

fig <- plot_ly(
  type = "scatter",
  x = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12), 
  y = c(28.8, 28.5, 37, 56.8, 69.7, 79.7, 78.5, 77.8, 74.1, 62.6, 45.3, 39.9), 
  mode = "markers+lines") 
fig <- fig %>%
  layout(
    xaxis = list(
      dtick = 0.75, 
      tick0 = 0.5, 
      tickmode = "linear"
    ))

#array
fig <- plot_ly(
  type = "scatter",
  x = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12), 
  y = c(28.8, 28.5, 37, 56.8, 69.7, 79.7, 78.5, 77.8, 74.1, 62.6, 45.3, 39.9), 
  mode = "markers+lines") 
fig <- fig %>%
  layout(
    xaxis = list(
      ticktext = list("One", "Three", "Five", "Seven", "Nine", "Eleven"), 
      tickvals = list(1, 3, 5, 7, 9, 11),
      tickmode = "array"
    ))

fig
#procenty
fig <- plot_ly(
  type = "scatter",
  x = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12), 
  y = c(0.18, 0.38, 0.56, 0.46, 0.59, 0.4, 0.78, 0.77, 0.74, 0.42, 0.45, 0.39), 
  mode = "markers+lines") 
fig <- fig %>%
  layout(
    yaxis = list(
      tickformat = "%"
    ))

fig
#data
df <- read.csv('https://raw.githubusercontent.com/plotly/datasets/master/finance-charts-apple.csv')

fig <- plot_ly(
  type = "scatter",
  x = df$Date, 
  y = df$AAPL.High,
  name = 'AAPL High',
  mode = "lines",
  line = list(
    color = '#17BECF'
  )) 
fig <- fig %>%
  add_trace(
    type = "scatter",
    x = df$Date, 
    y = df$AAPL.Low,
    name = 'AAPL Low',
    mode = "lines",
    line = list(
      color = '#7F7F7F'
    )) 
fig <- fig %>%
  layout(
    title = "Time Series with Custom Date-Time Format",
    xaxis = list(
      type = 'date',
      tickformat = "%d %B (%a)<br>%Y"
    ))

fig
#####hover text####
fig <- plot_ly(type = 'scatter', mode = 'markers') 
fig <- fig %>%
  add_trace(
    x = c(1:5), 
    y = rnorm(5, mean = 5),
    text = c("Text A", "Text B", "Text C", "Text D", "Text E"),
    hoverinfo = 'text',
    marker = list(color='green'),
    showlegend = F
  )

fig
#po��czony tekst
trace_0 <- rnorm(100, mean = 5)
trace_1 <- rnorm(100, mean = 0)
trace_2 <- rnorm(100, mean = -5)
x <- c(1:100)

data <- data.frame(x, trace_0, trace_1, trace_2)

fig <- plot_ly(data, x = ~x, y = ~trace_0, name = 'trace 0', type = 'scatter', mode = 'lines') 
fig <- fig %>% add_trace(y = ~trace_1, name = 'trace 1', mode = 'lines+markers') 
fig <- fig %>% add_trace(y = ~trace_2, name = 'trace 2', mode = 'markers')
fig <- fig %>%
  layout(hovermode = "x unified")
fig
#hovertemplate
# %{variable}  - zmienne
fig <- plot_ly() 
fig <- fig %>%
  add_trace(
    type = 'scatter',
    mode = 'lines+markers',
    x = c(1,2,3,4,5),
    y = c(2.02825,1.63728,6.83839,4.8485,4.73463),
    text = c("Text A", "Text B", "Text C", "Text D", "Text E"),
    hovertemplate = paste('<i>Price</i>: $%{y:.2f}',
                          '<br><b>X</b>: %{x}<br>',
                          '<b>%{text}</b>'),
    showlegend = FALSE
  ) 
fig <- fig %>%
  add_trace(
    type = 'scatter',
    mode = 'lines+markers',
    x = c(1,2,3,4,5),
    y = c(3.02825,2.63728,4.83839,3.8485,1.73463),
    hovertemplate = 'Price: %{y:$.2f}<extra></extra>',
    showlegend = FALSE
  )

fig
#####adnotacje#####
#pojedyncza
m <- mtcars[which.max(mtcars$mpg), ]

a <- list(
  x = m$wt,
  y = m$mpg,
  text = rownames(m),
  xref = "x",
  yref = "y",
  showarrow = TRUE,
  arrowhead = 5,
  ax = 20,
  ay = -40
)

fig <- plot_ly(mtcars, x = ~wt, y = ~mpg)
fig <- fig %>% add_markers()
fig <- fig %>% layout(annotations = a)

fig
#kilka
data <- mtcars[which(mtcars$am == 'Manual' & mtcars$gear == 4),]

fig <- plot_ly(data, x = ~wt, y = ~mpg, type = 'scatter', mode = 'markers',
               marker = list(size = 10))
fig <- fig %>% add_annotations(x = data$wt,
                               y = data$mpg,
                               text = rownames(data),
                               xref = "x",
                               yref = "y",
                               showarrow = TRUE,
                               arrowhead = 4,
                               arrowsize = .5,
                               ax = 20,
                               ay = -40)

fig
#3d
fig <- plot_ly()
fig <- fig %>% add_trace(
  x = c("2017-01-01", "2017-02-10", "2017-03-20"), 
  y = c("A", "B", "C"), 
  z = c(1, 1000, 100000), 
  name = "z", 
  type = "scatter3d"
)
fig <- fig %>% layout(
  scene = list(
    aspectratio = list(
      x = 1,
      y = 1,
      z = 1
    ),
    camera = list(
      center = list(
        x = 0,
        y = 0,
        z = 0
      ),
      eye = list(
        x = 1.96903462608,
        y = -1.09022831971,
        z = 0.405345349304
      ),
      up = list(
        x = 0,
        y = 0,
        z = 1
      )
    ),
    dragmode = "turntable",
    xaxis = list(
      title = "",
      type = "date"
    ),
    yaxis = list(
      title = "",
      type = "category"
    ),
    zaxis = list(
      title = "",
      type = "log"
    ),
    annotations = list(list(
      showarrow = F,
      x = "2017-01-01",
      y = "A",
      z = 0,
      text = "Point 1",
      xanchor = "left",
      xshift = 10,
      opacity = 0.7
    ), list(
      x = "2017-02-10",
      y = "B",
      z = 4,
      text = "Point 2",
      textangle = 0,
      ax = 0,
      ay = -75,
      font = list(
        color = "black",
        size = 12
      ),
      arrowcolor = "black",
      arrowsize = 3,
      arrowwidth = 1,
      arrowhead = 1
    ), list(
      x = "2017-03-20",
      y = "C",
      z = 5,
      ax = 50,
      ay = 0,
      text = "Point 3",
      arrowhead = 1,
      xanchor = "left",
      yanchor = "bottom"
    )
    )),
  xaxis = list(title = "x"),
  yaxis = list(title = "y")
)

fig

#####markery#####
#obw�dki

x <- runif(500, min=3, max=6)
y <- runif(500, min=3, max=6)

fig <- plot_ly(type = 'scatter', mode = 'markers') 
fig <- fig %>%
  add_trace(
    x = x,
    y = y,
    marker = list(
      color = 'rgb(17, 157, 255)',
      size = 20,
      line = list(
        color = 'rgb(231, 99, 250)',
        width = 2
      )
    ),
    showlegend = F
  ) 
fig <- fig %>%
  add_trace(
    x = c(2),
    y = c(4.5),
    marker = list(
      color = 'rgb(17, 157, 255)',
      size = 120,
      line = list(
        color = 'rgb(231, 99, 250)',
        width = 12
      )
    ),
    showlegend = F
  )

fig
#prze�roczysto��
x <- runif(500, min=3, max=6)
y <- runif(500, min=3, max=6)

fig <- plot_ly(type = 'scatter', mode = 'markers') 
fig <- fig %>%
  add_trace(
    x = x,
    y = y,
    marker = list(
      color = 'rgb(17, 157, 255)',
      size = 20,
      opacity = 0.5,
      line = list(
        color = 'rgb(231, 99, 250)',
        width = 2
      )
    ),
    showlegend = F
  ) 
fig <- fig %>%
  add_trace(
    x = c(2,2),
    y = c(4.25,4.75),
    marker = list(
      color = 'rgb(17, 157, 255)',
      size = 120,
      opacity = 0.5,
      line = list(
        color = 'rgb(231, 99, 250)',
        width = 12
      )
    ),
    showlegend = F
  )

fig

#####wykresy interaktywne####
#przyciski
#methods
#"restyle": modyfikacja danych albo  parametr�w zwiaznych z nimi
#"relayout": modyfikacja wygl�du
#"update": modyfikacja danych i  parametr�w zwiaznych z nimi
#"animate": start/pauza animacji
#restyle
x <- seq(-2*pi, 2*pi, length.out = 1000)
df <- data.frame(x, y1 = sin(x))

fig <- plot_ly(df, x = ~x)
fig <- fig %>% add_lines(y = ~y1)


fig <- fig %>% layout(
  title = "Button Restyle",
  xaxis = list(domain = c(0.1, 1)),
  yaxis = list(title = "y"),
  updatemenus = list(
    list(
      type = "buttons",
      y = 0.8,
      buttons = list(
        
        list(method = "restyle",
             args = list("line.color", "blue"),
             label = "Blue"),
        
        list(method = "restyle",
             args = list("line.color", "red"),
             label = "Red")))
  ))

fig
#kilka element�w jednocze�nie
fig <- plot_ly(z = ~volcano, type = "heatmap", colorscale='Rainbow')

  # rodzaj wykresu
chart_types <- list(
  type = "buttons",
  direction = "right",
  xanchor = 'center',
  yanchor = "top",
  pad = list('r'= 0, 't'= 10, 'b' = 10),
  x = 0.5,
  y = 1.27,
  buttons = list(
    
    list(method = "restyle",
         args = list("type", "heatmap"),
         label = "Heatmap"),
    
    list(method = "restyle",
         args = list("type", "contour"),
         label = "Contour"),
    
    list(method = "restyle",
         args = list("type", "surface"),
         label = "Surface")
  ))

  # kolory 
color_types <- list(
  type = "buttons",
  direction = "right",
  xanchor = 'center',
  yanchor = "top",
  pad = list('r'= 0, 't'= 10, 'b' = 10),
  x = 0.5,
  y = 1.17,
  buttons = list(
    
    list(method = "restyle",
         args = list("colorscale", "Rainbow"),
         label = "Rainbow"),
    
    list(method = "restyle",
         args = list("colorscale", "Jet"),
         label = "Jet"),
    
    list(method = "restyle",
         args = list("colorscale", "Earth"),
         label = "Earth"),
    
    list(method = "restyle",
         args = list("colorscale", "Electric"),
         label = "Electric")
  ))

annot <- list(list(text = "Chart<br>Type", x=0.2, y=1.25, xref='paper', yref='paper', showarrow=FALSE),
              list(text = "Color<br>Type", x=0.2, y=1.15, xref='paper', yref='paper', showarrow=FALSE))

  #wykres
fig <- fig %>% layout(
  xaxis = list(domain = c(0.1, 1)),
  yaxis = list(title = "y"),
  updatemenus = list(chart_types,color_types),
  annotations = annot)

fig

#obszary
x0 <- rnorm(400, mean=2, sd=0.4)
y0 <- rnorm(400, mean=2, sd=0.4)
x1 <- rnorm(400, mean=3, sd=0.6)
y1 <- rnorm(400, mean=6, sd=0.4)
x2 <- rnorm(400, mean=4, sd=0.2)
y2 <- rnorm(400, mean=4, sd=0.4)

  # shapes components
cluster0 = list(
  type = 'circle',
  xref ='x', yref='y',
  x0=min(x0), y0=min(y0),
  x1=max(x0), y1=max(y0),
  opacity=0.25,
  line = list(color="#835AF1"),
  fillcolor="#835AF1")

cluster1 = list(
  type = 'circle',
  xref ='x', yref='y',
  x0=min(x1), y0=min(y1),
  x1=max(x1), y1=max(y1),
  opacity=0.25,
  line = list(color="#7FA6EE"),
  fillcolor="#7FA6EE")

cluster2 = list(
  type = 'circle',
  xref ='x', yref='y',
  x0=min(x2), y0=min(y2),
  x1=max(x2), y1=max(y2),
  opacity=0.25,
  line = list(color="#B8F7D4"),
  fillcolor="#B8F7D4")

  # przyciski
updatemenus <- list(
  list(
    active = -1,
    type = 'buttons',
    buttons = list(
      
      list(
        label = "None",
        method = "relayout",
        args = list(list(shapes = c()))),
      
      list(
        label = "Cluster 0",
        method = "relayout",
        args = list(list(shapes = list(cluster0, c(), c())))),
      
      list(
        label = "Cluster 1",
        method = "relayout",
        args = list(list(shapes = list(c(), cluster1, c())))),
      
      list(
        label = "Cluster 2",
        method = "relayout",
        args = list(list(shapes = list(c(), c(), cluster2)))),
      
      list(
        label = "All",
        method = "relayout",
        args = list(list(shapes = list(cluster0,cluster1,cluster2))))
    )
  )
)

fig <- plot_ly(type = 'scatter', mode='markers') 
fig <- fig %>% add_trace(x=x0, y=y0, mode='markers', marker=list(color='#835AF1')) 
fig <- fig %>% add_trace(x=x1, y=y1, mode='markers', marker=list(color='#7FA6EE')) 
fig <- fig %>% add_trace(x=x2, y=y2, mode='markers', marker=list(color='#B8F7D4')) 
fig <- fig %>% layout(title = "Highlight Clusters", showlegend = FALSE,
                      updatemenus = updatemenus)

fig
#update
library(quantmod)

d <- quantmod::getSymbols("AAPL")

df <- data.frame(Date=index(AAPL),coredata(AAPL))

high_annotations <- list(
  x=df$Date[df$AAPL.High == max(df$AAPL.High)], 
  y=max(df$AAPL.High),
  xref='x', yref='y',
  text=paste0('High: $',max(df$AAPL.High)),
  ax=0, ay=-40
)

low_annotations <- list(
  x=df$Date[df$AAPL.Low == min(df$AAPL.Low)], 
  y=min(df$AAPL.Low),
  xref='x', yref='y',
  text=paste0('Low: $',min(df$AAPL.Low)),
  ax=0, ay=40
)

# updatemenus component
updatemenus <- list(
  list(
    active = -1,
    type= 'buttons',
    buttons = list(
      list(
        label = "High",
        method = "update",
        args = list(list(visible = c(FALSE, TRUE)),
                    list(title = "Apple High",
                         annotations = list(c(), high_annotations)))),
      list(
        label = "Low",
        method = "update",
        args = list(list(visible = c(TRUE, FALSE)),
                    list(title = "Apple Low",
                         annotations = list(low_annotations, c() )))),
      list(
        label = "Both",
        method = "update",
        args = list(list(visible = c(TRUE, TRUE)),
                    list(title = "Apple",
                         annotations = list(low_annotations, high_annotations)))),
      list(
        label = "Reset",
        method = "update",
        args = list(list(visible = c(TRUE, TRUE)),
                    list(title = "Apple",
                         annotations = list(c(), c())))))
  )
)

fig <- df %>% plot_ly(type = 'scatter', mode = 'lines') 
fig <- fig %>% add_lines(x=~Date, y=~AAPL.High, name="High",
                         line=list(color="#33CFA5")) 
fig <- fig %>% add_lines(x=~Date, y=~AAPL.Low, name="Low",
                         line=list(color="#F06A6A")) 
fig <- fig %>% layout(title = "Apple", showlegend=FALSE,
                      xaxis=list(title="Date"),
                      yaxis=list(title="Price ($)"),
                      updatemenus=updatemenus)


fig
#####lista rozwijana#####
library(MASS)

covmat <- matrix(c(0.8, 0.4, 0.3, 0.8), nrow = 2, byrow = T)
df <- mvrnorm(n = 10000, c(0,0), Sigma = covmat)
df <- as.data.frame(df)

colnames(df) <- c("x", "y")
fig <- plot_ly(df, x = ~x, y = ~y, alpha = 0.3)
fig <- fig %>% add_markers(marker = list(line = list(color = "black", width = 1)))
fig <- fig %>% layout(
  title = "Drop down menus - Plot type",
  xaxis = list(domain = c(0.1, 1)),
  yaxis = list(title = "y"),
  updatemenus = list(
    list(
      y = 0.8,
      buttons = list(
        
        list(method = "restyle",
             args = list("type", "scatter"),
             label = "Scatter"),
        
        list(method = "restyle",
             args = list("type", "histogram2d"),
             label = "2D Histogram")))
  ))

fig
#dwie listy
x <- seq(-2 * pi, 2 * pi, length.out = 1000)
df <- data.frame(x, y1 = sin(x), y2 = cos(x))

fig <- plot_ly(df, x = ~x)
fig <- fig %>% add_lines(y = ~y1, name = "A")
fig <- fig %>% add_lines(y = ~y2, name = "B", visible = F)
fig <- fig %>% layout(
  title = "Drop down menus - Styling",
  xaxis = list(domain = c(0.1, 1)),
  yaxis = list(title = "y"),
  updatemenus = list(
    list(
      y = 0.8,
      buttons = list(
        
        list(method = "restyle",
             args = list("line.color", "blue"),
             label = "Blue"),
        
        list(method = "restyle",
             args = list("line.color", "red"),
             label = "Red"))),
    
    list(
      y = 0.7,
      buttons = list(
        list(method = "restyle",
             args = list("visible", list(TRUE, FALSE)),
             label = "Sin"),
        
        list(method = "restyle",
             args = list("visible", list(FALSE, TRUE)),
             label = "Cos")))
  )
)

fig
#####suwaki#####
library(quantmod)
#zakres
getSymbols(Symbols = c("AAPL", "MSFT"), from = '2018-01-01', to = '2019-01-01')
ds <- data.frame(Date = index(AAPL), AAPL[,6], MSFT[,6])

fig <- plot_ly(ds, x = ~Date)
fig <- fig %>% add_lines(y = ~AAPL.Adjusted, name = "Apple")
fig <- fig %>% add_lines(y = ~MSFT.Adjusted, name = "Microsoft")
fig <- fig %>% layout(
  title = "Stock Prices",
  xaxis = list(
    rangeselector = list(
      buttons = list(
        list(
          count = 3,
          label = "3 mo",
          step = "month",
          stepmode = "backward"),
        list(
          count = 6,
          label = "6 mo",
          step = "month",
          stepmode = "backward"),
        list(
          count = 1,
          label = "1 yr",
          step = "year",
          stepmode = "backward"),
        list(
          count = 1,
          label = "YTD",
          step = "year",
          stepmode = "todate"),
        list(step = "all"))),
    
    rangeslider = list(type = "date")),
  
  yaxis = list(title = "Price"))

fig

#suwak
x <- seq(0,10, length.out = 1000)
aval <- list()
for(step in 1:11){
  aval[[step]] <-list(visible = FALSE,
                      name = paste0('v = ', step),
                      x=x,
                      y=sin(step*x))
}
aval[3][[1]]$visible = TRUE

# kroki
steps <- list()
fig <- plot_ly()
for (i in 1:11) {
  fig <- add_lines(fig,x=aval[i][[1]]$x,  y=aval[i][[1]]$y, visible = aval[i][[1]]$visible, 
                   name = aval[i][[1]]$name, type = 'scatter', mode = 'lines', hoverinfo = 'name', 
                   line=list(color='00CED1'), showlegend = FALSE)
  
  step <- list(args = list('visible', rep(FALSE, length(aval))),
               method = 'restyle')
  step$args[[2]][i] = TRUE  
  steps[[i]] = step 
}  

# dodanie suwaka
fig <- fig %>%
  layout(sliders = list(list(active = 3,
                             currentvalue = list(prefix = "Frequency: "),
                             steps = steps)))

fig

#mapa
df <- read.csv('https://raw.githubusercontent.com/plotly/datasets/master/globe_contours.csv')
df$id <- seq_len(nrow(df))

library(tidyr)
d <- df %>%
  gather(key, value, -id) %>%
  separate(key, c("l", "line"), "\\.") %>%
  spread(l, value)

geo <- list(
  showland = TRUE,
  showlakes = TRUE,
  showcountries = TRUE,
  showocean = TRUE,
  countrywidth = 0.5,
  landcolor = 'rgb(230, 145, 56)',
  lakecolor = 'rgb(0, 255, 255)',
  oceancolor = 'rgb(0, 255, 255)',
  projection = list(
    type = 'orthographic',
    rotation = list(
      lon = -100,
      lat = 40,
      roll = 0
    )
  ),
  lonaxis = list(
    showgrid = TRUE,
    gridcolor = toRGB("gray40"),
    gridwidth = 0.5
  ),
  lataxis = list(
    showgrid = TRUE,
    gridcolor = toRGB("gray40"),
    gridwidth = 0.5
  )
)

## dodanie zdarze�

  # rozwijana lista
projections = data.frame(type = c("equirectangular", "mercator", "orthographic", "natural earth","kavrayskiy7", 
                                  "miller", "robinson", "eckert4", "azimuthal equal area","azimuthal equidistant", 
                                  "conic equal area", "conic conformal", "conic equidistant", "gnomonic", "stereographic", 
                                  "mollweide", "hammer", "transverse mercator", "albers usa", "winkel tripel"))

all_buttons <- list()
for (i in 1:length(projections[,])) { 
  all_buttons[[i]] <- list(method = "relayout",
                           args = list(list(geo.projection.type = projections$type[i])),
                           label = projections$type[i])
}

  # suwaki
lon_range = data.frame(seq(-180, 180, 10))
lat_range = data.frame(seq(-90, 90, 10))
colnames(lon_range) <- "x"
colnames(lat_range) <- "x"

all_lat <- list()
for (i in 1:length(lat_range[,])) { 
  all_lat[[i]] <- list(method = "relayout",
                       args = list(list(geo.projection.rotation.lat = lat_range$x[i])),
                       label = lat_range$x[i])
}

all_lon <- list()
for (i in 1:length(lon_range[,])) {  
  all_lon[[i]] <- list(method = "relayout", 
                       args = list(list(geo.projection.rotation.lon = lon_range$x[i])),
                       label = lon_range$x[i]) 
} 

  # adnotacje
annot <- list(x = 0, y=0.8, text = "Projection", yanchor = 'bottom', 
              xref = 'paper', xanchor = 'right',
              showarrow = FALSE)


  # ziemia 3d
fig<- plot_geo(d) 
fig <- fig %>% group_by(line) 
fig <- fig %>% add_lines(x = ~lon, y = ~lat, color = ~line, colors = 'Reds') 
fig <- fig %>% layout(
  showlegend = FALSE, geo = geo
)

  # wykres
fig<- fig
fig <- fig %>% layout(annotations = annot,
                      updatemenus = list(list(active = 2, x = 0, y = 0.8, 
                                              buttons=all_buttons)),
                      sliders = list(
                        
                        list(
                          active = (length(lon_range[,])-1)/2, 
                          currentvalue = list(prefix = "Longitude: "), 
                          pad = list(t = 20), 
                          
                          steps = all_lon),
                        
                        list(
                          active = (length(lat_range[,])-1)/2, 
                          currentvalue = list(prefix = "Latitude: "), 
                          pad = list(t = 100), 
                          
                          steps = all_lat)))

fig