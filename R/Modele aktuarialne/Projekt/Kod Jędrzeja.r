##################################### ZADANIE 1 ###########################################
zad_1 <- function(a, b, d){
  Sx <- function(x){(b/(x+b))^(a)}
  Exl = integrate(Sx, lower=d, upper=Inf)$value
  Exp = Exl/Sx(d)
  Ex = b/(a-1)
  LER = (Ex-Exl)/Ex
  Exd = Ex*LER
  return(list(Exp, LER*100, Exd))
}
result1 = zad_1(2, 5, 1)
ans1 = paste('Odpowiedz: \n Wartosc EXp = ',
            round(unlist(result1[1]),digits=2), '\n',
            'Redukcja skladki netto wyniosla (LER(d)): ',
            round(unlist(result1[2]),digits=2), '% \n',
            'Wartosc E(X ^ d) = ',
            round(unlist(result1[3]),digits=2))
cat(ans1)
##################################### ZADANIE 2 ###########################################
zad_2 <- function(a, b, d, u){
  Sx <- function(x){(b/(x+b))^(a)}
  EXld = integrate(Sx, lower=d, upper=Inf)$value
  EXlu = integrate(Sx, lower=u, upper=Inf)$value
  EXt = EXld-EXlu
  return(round(EXt, digits=2))
}
result2 = zad_2(5, 100, 10, 50)
ans2 = paste ('Odpowiedz: \n EXt wynosi:', result2)
cat(ans2)
##################################### ZADANIE 3 ###########################################
zad_3 <- function(r, p, a, b, d){
  EN = r*p/(1-p)
  VarN = r * p/(1-p)^2
  EX = b/(a-1)
  VarX = (a*b^2)/((a-1)^2-(a-2))
  ES = EN*EX
  VarS = EN*VarX + VarN * EX^2
  Sx = function(t){(b/(t+b))^a}
  v = Sx(d)
  beta = (1-p)/p
  beta_new = beta*v
  gamma_new = 1/(beta_new+1)
  EN_new = r*gamma_new/(1-gamma_new)
  VarN_new = r * gamma_new/(1-gamma_new)^2
  EX_new = (b+d)/(a-1)
  VarX_new = (a*(b+d)^2)/((a-1)^2-(a-2))
  ES_new = EN_new*EX_new
  VarS_new = EN_new*VarX_new + VarN_new * EX_new^2
  return(list(ES, VarS, ES_new, VarS_new))
}
result3 = zad_3(3, 0.3, 3, 20, 5)
ans3 = paste('Odpowiedz: \n Wartosc ES = ',
             round(unlist(result3[1]),digits=2), '\n',
             'VarS =',
             round(unlist(result3[2]),digits=2), '\n',
             'ES^ (S z daszkiem) =',
             round(unlist(result3[3]),digits=2), '\n',
             'VarS^ (S z daszkiem) =',
             round(unlist(result3[4]),digits=2))
cat(ans3)