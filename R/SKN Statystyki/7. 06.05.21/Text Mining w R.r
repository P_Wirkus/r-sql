#WAZNE! Nie chcemy aby dane tekstowe byly czytane jako zmienne kategoryczne 
options(stringsAsFactors=F)
Sys.setlocale('LC_ALL','C')

if (!require("stringi")) install.packages("stringi")
if (!require("stringr")) install.packages("stringr")
if (!require("qdap")) install.packages("qdap")
if (!require("tm")) install.packages("tm")
if (!require("tidyverse")) install.packages("tidyverse")
if (!require("tokenizers")) install.packages("tokenizers")
if (!require("tidytext")) install.packages("tidytext")
if (!require("SnowballC")) install.packages("SnowballC")
if (!require("syuzhet")) install.packages("syuzhet")
if (!require("gridExtra")) install.packages("gridExtra")
if (!require("wordcloud")) install.packages("wordcloud")
if (!require("smacof")) install.packages("smacof")
if (!require("ggrepel")) install.packages("ggrepel")
if (!require("ggfortify")) install.packages("ggfortify")
if (!require("ggthemes")) install.packages("ggthemes")
if (!require("quanteda")) install.packages("quanteda")

#zaladowanie danych
restaurants <- read.csv("C:/Users/uzytkownik/Documents/Projekt/Restaurants.csv")

restaurants$X <- as.factor(restaurants$X)
restaurants$Restaurant <- as.factor(restaurants$Restaurant)
restaurants$Reviewer <- as.factor(restaurants$Reviewer)

df <- restaurants[!duplicated(restaurants$Review), ]

df[1:5,4]

#zastepowanie pierwszego wystapienia slowa 'good' slowem 'great' w pierwszym dokumencie
sub('good','great', df[1,4], ignore.case=T)

sub('good','great', df[1:5,4], ignore.case=T)

gsub('good','great', df[1:5,4])

gsub(c('good | amazing'), 'great', df[1:5,4])

#wyszukanie dokumentow, w ktorych wystepuje slowo 'music' (przynajmniej raz);
#nie liczy liczby wystapien
grep('music', df$Review, ignore.case=T)

df[3643,4]

grep(c('pizza | burger'), df$Review, ignore.case=T)

df[258,4]

#sprawdzenie (logiczne, czyli TRUE lub FALSE) czy w danym dokumencie wystepuje slowo 'pizza' (przynajmniej raz)
#nie liczy liczby wystapien
grepl('pizza', df$Review, ignore.case=T)

pizza <- grepl('pizza', df$Review, ignore.case=T)
sum(pizza)/nrow(df)
#slowo 'pizza' wystepuje (przynajmniej raz) w 4.12% recenzji

example <- 'Ohhh my god, the food was delicious, the service was perfect and 
I loved the music. We ordered butter chicken with rice and biryani. The price 
could be a bit lower but we loved the whole experience.'
#oh my god
#ooohhh my god
#ooohhh my goddd
#omg
#OMG

#gsub(c('oh my god | ooohhh my god | ooohhh my goddd | OMG'), 'omg', df$Review)

#Pierwsze spojrzenie na dane
review_words <- df %>% unnest_tokens(word, Review, to_lower=FALSE, token = "words") 
#W sumie, w recenzjach wystepuje 505 528 slow

#Ile jest unikalnych slow?
counts <- review_words %>% count(word, sort=TRUE)
#W recenzjach wystepuje 21 120 unikalnych slow
#Czy na pewno?

#Ktore slowa wystepuja najczjczesciej?
#the, and, was, The, etc. 

#Czyszczenie / przygotowywanie danych
#wszystko z malych liter 
#usuniecie bledow, cyfr, dodatkowych spacji, linijek, zbednych znaków
#usuniecie stop words
#stemming
#usuniecie reszty znakow interpuncyjnych
#usuniecie rzadkich i najczestszych slow

tryTolower <- function(x){
  y = NA
  try_error = tryCatch(tolower(x), error = function(e) e)
  if (!inherits(try_error, 'error'))
    y = tolower(x)
  return(y)
}

#Sposob pierwszy czyszczenia danych

df[41,4]
df[1,4]

df$ReviewCopy <- df$Review

df$Review <- enc2utf8(df$Review)

df$Review <- as.character(df$Review) %>%
  tryTolower() %>% 
  {gsub("[^[:alnum:][:blank:]!'*,-./:;?`]", "",.)} %>%
  {gsub("u2019", "'",.)} %>%
  {gsub("\\n", " ", .)} %>%
  {gsub("[?!]+",".", .)} %>%
  {gsub("[0-9]"," ", .)} %>%
  {gsub("-"," ", .)} %>%
  {gsub("\"+"," ", .)} %>%
  {gsub("\\.+","\\. ", .)} %>%
  {gsub(" +"," ", .)} %>%
  {gsub("\\. \\.","\\. ", .)}

df[41,9]
df[41,4]

df[1,9]
df[1,4]

?stopwords
stopwords_en <- as.data.frame(stopwords("en"))
names(stopwords_en)[1] <- "word"
stopwords_smart <- as.data.frame(stopwords(source = "smart"))
names(stopwords_smart)[1] <- "word"

?stop_words
stop_words <- stop_words

j<-1
for (j in 1:nrow(df)) {
  temp_review <- anti_join(df[j,] %>% 
                    unnest_tokens(word, Review, drop=FALSE, to_lower=FALSE, token = "words"), stopwords_smart)
  
  stemmed_review <- wordStem(temp_review[,"word"], language = "porter")
  
  df[j,"Review"] <- paste((stemmed_review), collapse = " ")
  
}

df[1,9]
df[1,4]

df[41,9]
df[41,4]

cleaned_words <- df %>% unnest_tokens(word, Review, to_lower=FALSE, token = "words") 

cleaned_counts <- cleaned_words %>% count(word, sort=TRUE)

cleaned_counts %>% 
  mutate(word = reorder(word,n)) %>% 
  top_n(20, word) %>%
  ggplot(aes(word,n)) +  
  geom_col() + 
  labs(x = NULL, y = "Liczba wystapien") + 
  coord_flip() + 
  theme(text = element_text(size = 17)) + 
  ggtitle("Histogram")

#13 030 unikalnych slow

0.001*nrow(df)
0.01*nrow(df)

infrequent <- cleaned_counts %>% filter(n<0.001*nrow(df))
#10 933 rzadkich slow 

j<-1 
for (j in 1:nrow(df)) {
  temp_review <- anti_join((df[j,] %>% 
                              unnest_tokens(word, Review, to_lower=FALSE, token = "words")), infrequent)
  
  df[j,"Review"] <- paste((temp_review[,"word"]),collapse = " ")
  
}

df[1,9]
df[1,4]

#wizualizacja najczestszych slow
wordcount_final <- df %>%
  unnest_tokens(word, Review, to_lower=FALSE, token = "words") %>%
  count(word, sort=TRUE)

#wyczyscic

set.seed(1223)
wordcloud(words = wordcount_final$word, freq = wordcount_final$n,
          max.words=70, random.order=FALSE, 
          colors = brewer.pal(8, "Dark2"))

#wyczyscic

set.seed(999)
wordcloud(words = wordcount_final$word, freq = wordcount_final$n,
          max.words=70, random.order=FALSE,
          colors = brewer.pal(8, "Dark2"))

#bigramy
bigramcount_final <- df %>%
  unnest_tokens(word, Review, to_lower=FALSE, token = "ngrams", n = 2) %>%
  count(word, sort=TRUE)

#trigramy
trigramcount_final <- df %>%
  unnest_tokens(word, Review, to_lower=FALSE, token = "ngrams", n = 3) %>%
  count(word, sort=TRUE)

#Budowanie korpusu
#Sposob 1
corpus1.1 <- corpus(df, docid_field = "X", text_field = "Review")

#Sposob 2
df1.2 <- data.frame(doc_id = df$X, text = df$Review)
corpus1.2 <- VCorpus(DataframeSource(df1.2))

#Budowanie TDM i DTM
tdm1.1 <- TermDocumentMatrix(corpus1.1)
tdm1.2 <- TermDocumentMatrix(corpus1.2)

tdm.1.2.m <- as.matrix(tdm1.2)

dtm1.2 <- DocumentTermMatrix(corpus1.2)

#Sposob drugi czyszczenia danych
df$Review2 <- df$ReviewCopy

df$Review2 <- as.character(df$Review2) %>%
  {gsub("[^[:alnum:][:blank:]!'*,-./:;?`]", "",.)} %>%
  {gsub("U2019", "'",.)} %>%
  {gsub("\\n", " ", .)}

df[41, 9]
df[41, 10]

df2 <- data.frame(doc_id = df$X, text = df$Review2)

corpus2 <- VCorpus(DataframeSource(df2))

clean.corpus<-function(corpus){
  corpus <- tm_map(corpus, content_transformer(tryTolower))
  corpus <- tm_map(corpus, removeWords, stopwords(source = "smart"))
  corpus <- tm_map(corpus, removePunctuation)
  corpus <- tm_map(corpus, stripWhitespace)
  corpus <- tm_map(corpus, removeNumbers)
  corpus <- tm_map(corpus, stemDocument)
  corpus <- tm_map(corpus, removeWords, stopwords(source = "smart"))
  return(corpus)
}

corpus2 <- clean.corpus(corpus2)
tdm2 <- TermDocumentMatrix(corpus2)
tdm2 <- removeSparseTerms(tdm2, sparse = 0.999)

tdm.2.m <- as.matrix(tdm2)

dtm2 <- DocumentTermMatrix(corpus2)
dtm2 <- removeSparseTerms(dtm2, sparse = 0.999)

corpus2 <- as.Corpus(tdm2)


#Znajdowanie powiazan mijdzy slowami
#przyklad: servic
servic <- findAssocs(tdm1.2, 'servic', 0.12)

servic <- as.data.frame(servic)
servic$terms <- row.names(servic)

servic$terms <- factor(servic$terms,
                           levels=servic$terms)

ggplot(servic, aes(y=terms)) +
  geom_point(aes(x=servic), data=servic,size=2)+
  theme_gdocs()+ 
  geom_text(aes(x=servic,label=servic),colour="darkred",hjust=-.25,size=4)+
  theme(text=element_text(size=15),axis.title.y=element_blank())



#Mapowanie wyrazow
#MDS - multidimensional scaling

co_occurrence_matrix1 <- fcm(x = corpus1.1, context = "document", count = "frequency", tri=FALSE)
#context = "document" lub "window"
#count = "frequency" lub "boolean"
#window = wielkosc okna

dfm <- dfm(corpus1.1)
counts <- colSums(as.matrix(dfm)) 

co_occurrence_matrix1 <- as.matrix(co_occurrence_matrix1)
diag(co_occurrence_matrix1) <- counts

sortedcounts <- counts %>% sort(decreasing=TRUE)
sortednames <- names(sortedcounts)
nwords <- 200

co_occurrence_matrix1 <- co_occurrence_matrix1[sortednames[1:nwords], sortednames[1:nwords]]
co_occurrence_matrix1[1:10,1:10]

distances <- sim2diss(co_occurrence_matrix1, method = "cooccurrence")
distances[1:10,1:10]

MDS_map <- smacofSym(distances)

ggplot(as.data.frame(MDS_map$conf), aes(D1, D2, label = rownames(MDS_map$conf))) +
  geom_text(check_overlap = TRUE) + theme_minimal(base_size = 15) + xlab('') + ylab('') +
  scale_y_continuous(breaks = NULL) + scale_x_continuous(breaks = NULL)

#2
co_occurrence_matrix2 <- fcm(x = corpus1.1, context = "window", window=1, count = "boolean", tri=FALSE)

co_occurrence_matrix2 <- as.matrix(co_occurrence_matrix2)
diag(co_occurrence_matrix2) <- counts

nwords <- 300

co_occurrence_matrix2 <- co_occurrence_matrix2[sortednames[1:nwords], sortednames[1:nwords]]
co_occurrence_matrix2[1:10,1:10]

distances <- sim2diss(co_occurrence_matrix2, method = "cooccurrence")
distances[1:10,1:10]

MDS_map <- smacofSym(distances)

ggplot(as.data.frame(MDS_map$conf), aes(D1, D2, label = rownames(MDS_map$conf))) +
  geom_text(check_overlap = TRUE) + theme_minimal(base_size = 15) + xlab('') + ylab('') +
  scale_y_continuous(breaks = NULL) + scale_x_continuous(breaks = NULL)

#3
co_occurrence_matrix3 <- fcm(x = corpus1.1, context = "window", window=3, count = "boolean", tri=FALSE)

co_occurrence_matrix3 <- as.matrix(co_occurrence_matrix3)
diag(co_occurrence_matrix3) <- counts

nwords <- 300

co_occurrence_matrix3 <- co_occurrence_matrix3[sortednames[1:nwords], sortednames[1:nwords]]
co_occurrence_matrix3[1:10,1:10]

distances <- sim2diss(co_occurrence_matrix3, method = "cooccurrence")
distances[1:10,1:10]

MDS_map <- smacofSym(distances)

ggplot(as.data.frame(MDS_map$conf), aes(D1, D2, label = rownames(MDS_map$conf))) +
  geom_text(check_overlap = TRUE) + theme_minimal(base_size = 15) + xlab('') + ylab('') +
  scale_y_continuous(breaks = NULL) + scale_x_continuous(breaks = NULL)

#######################################################################
#Analiza sentymentu

data(emoticon)
head(emoticon)

df$Review3 <- df$ReviewCopy

df$Review3 <- as.character(df$Review3) %>%
  {mgsub(emoticon[,2], emoticon[,1], .)} %>%
  tryTolower() %>% 
  {gsub("[^[:alnum:][:blank:]!'*,-./:;?`]", "",.)} %>%
  {gsub("u2019", "'",.)} %>%
  {gsub("\\n", " ", .)} %>%
  {gsub("[?!]+",".", .)} %>%
  {gsub("[0-9]"," ", .)} %>%
  {gsub("-"," ", .)} %>%
  {gsub("\"+"," ", .)} %>%
  {gsub("\\.+","\\. ", .)} %>%
  {gsub(" +"," ", .)} %>%
  {gsub("\\. \\.","\\. ", .)}

set.seed(999)
index <- sample(1:nrow(df), 500)

subset <- df[index, ]

all_words <- subset[,] %>%
  unnest_tokens("Review3", token = "words", output = "word") %>%
  anti_join(stopwords_en, by = "word") %>%
  count(word, sort = TRUE) %>%
  filter(n>20)

sentiment_scores <- polarity(all_words$word)$all
all_words$sentiment <- sentiment_scores[,"polarity"]

all_words %>%
  filter(sentiment != 0) %>%
  mutate(n = ifelse(sentiment == -1, -n, n)) %>%
  mutate(word = reorder(word, n)) %>%
  mutate(Sentiment = ifelse(sentiment == 1, "Postive","Negative")) %>%
  ggplot(aes(word, n, fill = Sentiment)) +
  geom_col() +
  coord_flip() +
  labs(y = "Wplyw na sentyment", x = "Slowo (min. l. wystapien = 20)")

pol <- polarity(subset[, "Review3"])$all

subset$polarity <- pol[, "polarity"]
subset$sent_bing <- get_sentiment(subset$Review3, method = "bing")

all_neg_review_words <- subset %>% filter(subset$polarity<0)

all_neg_review_words <- all_neg_review_words %>%
  unnest_tokens(word, Review3) %>%
  anti_join(stopwords_en, by = "word")

plot_neg <- all_neg_review_words %>%
  count(word, sort=TRUE) %>%
  mutate(word = reorder(word,n)) %>%
  top_n(25, word) %>%
  ggplot(aes(word,n)) +
  geom_col() +
  labs(x = NULL, y = "Liczba wystapien") +
  coord_flip() +
  theme(text = element_text(size = 17)) +
  ggtitle("Slowa: negatywne recenzje")

all_pos_review_words <- subset %>% filter(subset$polarity>0)

all_pos_review_words <- all_pos_review_words %>%
  unnest_tokens(word, Review3) %>%
  anti_join(stopwords_en, by = "word")

plot_pos <- all_pos_review_words %>%
  count(word, sort=TRUE) %>%
  mutate(word = reorder(word,n)) %>%
  top_n(25, word) %>%
  ggplot(aes(word,n)) +
  geom_col() +
  labs(x = NULL, y = "Liczba wystapien") +
  coord_flip() +
  theme(text = element_text(size = 17)) +
  ggtitle("Slowa: pozytywne recenzje")

grid.arrange(plot_neg, plot_pos, ncol=2)

subset <- cbind(subset, sentence_sent_polarity = 0*cbind(1:nrow(subset)),
                    sentence_sent_bing = 0*cbind(1:nrow(subset)))

totalNoSentences = 0
for (j in 1:nrow(subset)) {

  if (j%%1000==0){
    print(100*j/nrow(subset))
  }
  
  localSentences <- get_sentences(subset[j,]$Review3)
  totalNoSentences = totalNoSentences + length(localSentences)
  subset[j,]$sentence_sent_polarity <- 
    polarity(localSentences)$all[,"polarity"] %>%
    sign() %>% 
    sum()
  
  subset[j,]$sentence_sent_bing <-
    get_sentiment(localSentences, method = "bing", language = "english") %>%
    sign() %>%
    sum()
}

all_sentences <- data.frame(sentence = rep(0, totalNoSentences),
                            sentiment = rep(0, totalNoSentences),
                            polarity = rep(0, totalNoSentences),
                            User_ID = rep(subset[1,"X"], totalNoSentences),
                            Rating = rep(df[1,"Rating"], totalNoSentences),
                            stringsAsFactors = FALSE)

sentenceIndex = 1
for (j in 1:nrow(subset)) {
  if (j%%2000==0){
    print(100*j/nrow(subset))
  }
  
  localSentences <- subset[j,]$Review3 %>% get_sentences()
  
  all_sentences[sentenceIndex : (sentenceIndex+length(localSentences)-1), "sentence"] <- localSentences
  
  all_sentences[sentenceIndex : (sentenceIndex+length(localSentences)-1), "sentiment"] <- get_sentiment(localSentences, method = "bing")
  all_sentences[sentenceIndex : (sentenceIndex+length(localSentences)-1), "polarity"] <- polarity(localSentences)$all[,"polarity"]
  
  all_sentences[sentenceIndex : (sentenceIndex+length(localSentences)-1), "X"]  <- subset[j,]$X
  all_sentences[sentenceIndex : (sentenceIndex+length(localSentences)-1), "Rating"] <- subset[j,]$Rating
  
  sentenceIndex = sentenceIndex + length(localSentences)
}

all_pos_sentences <- all_sentences %>% filter(polarity>0)
all_neg_sentences <- all_sentences %>% filter(polarity<0)

all_neg_sentences_words <- all_neg_sentences %>%
  unnest_tokens(word, sentence) %>%
  anti_join(stopwords_en, by = "word")

plot_neg_sen <- all_neg_sentences_words %>%
  count(word, sort=TRUE) %>%
  mutate(word = reorder(word,n)) %>%
  top_n(25, word) %>%
  ggplot(aes(word,n)) +
  geom_col() +
  labs(x = NULL, y = "Liczba wystapien") +
  coord_flip() +
  theme(text = element_text(size = 17)) +
  ggtitle("Slowa: negatywne zdania")

all_pos_sentences_words<- all_pos_sentences %>%
  unnest_tokens(word,sentence) %>%
  anti_join(stopwords_en, by = "word")

plot_pos_sen <- all_pos_sentences_words %>%
  count(word, sort=TRUE) %>%
  mutate(word = reorder(word,n)) %>%
  top_n(25, word) %>%
  ggplot(aes(word,n)) +
  geom_col() +
  labs(x = NULL, y = "Liczba wystapien") +
  coord_flip() +
  theme(text = element_text(size = 17)) +
  ggtitle("Slowa: pozytywne zdania")

grid.arrange(plot_neg_sen, plot_pos_sen, ncol=2)


all_words2 <- subset[,] %>%
  unnest_tokens("Review3", token = "words", output = "word") %>%
  anti_join(stopwords_en, by = "word") %>%
  count(word, sort = TRUE)

all_words2 <- all_words2[-c(3208),]
emotion <- get_nrc_sentiment(all_words2$word)
rownames(emotion) <- all_words2$word

#wyczyscic

comparison.cloud(emotion[,c("anger","fear","disgust","anticipation","joy","sadness","surprise","trust")],
                 scale=c(1, 0.3), title.size=1)
 
