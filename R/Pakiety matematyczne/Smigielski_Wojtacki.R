main = function(N) {
  
  l_porzadkowa = c(1:N)
  
  age = sample(18:60,N, replace = TRUE)
  
  sex = sample(c("M","F"),N,replace = TRUE)
  
  weight <- c(1:N)
  for (i in 1:N) {
      
    if(sex[i] == "K") {
      weight[i] = rnorm(1, mean = 68, sd = 7)
      
                       }
    else               {
      weight[i] = rnorm(1, mean = 78, sd = 9)
                       }
                 }
  
  height = c(1:N)
  for (i in 1:N) {
    if(sex[i] == "K") {
      height[i] = round(rnorm(1, mean = 165, sd = 8),0)
                       }
    else               {
      height[i] = round(rnorm(1, mean = 177, sd = 10),0)
                       }
                 }
  
  s_c = c(1:N)
  for (i in 1:N) {
    if(sex[i] == "K") {
      s_c[i] = sample(c("Mezatka","Panna","Po rozwodzie","Wdowa"),1,replace = TRUE)
                       }
    else               {
      s_c[i] = sample(c("Zonaty","Kawaler","Po rozwodzie","Wdowiec"),1,replace = TRUE)
                       }
                 }
  
  kids = c(1:N)
  for (i in 1:N) {
      
    if(s_c[i] == "Zonaty" || s_c[i] == "Mezatka" || s_c[i] == "Wdowa" 
       || s_c[i] == "Wdowiec" || s_c[i] == "Po rozwodzie"
       && age[i] > 20)
           {
      kids[i] = sample(0:6,1)
           }
           else {
      kids[i] = 0
           }
                 }
  
  eyes = sample(c("Szare", "Niebieskie", "Piwne", "Zielone"),N,replace = TRUE)
  hair = sample(c("Blond","Rude","Brazowe"),N,replace = TRUE)
  
  bmi = c(1:N)
  for (i in 1:N) {
    bmi[i] = weight[i]/((height[i]/100)^2)
  }
  
  values = c(1:N)
  for (i in 1:N) {
    if(bmi[i] < 18.5) {
      values[i] = "Niedowaga"
                      }
    else if (bmi[i] >= 18.5 && bmi[i] <= 25)
                      {
      values[i] = "Norma"
                      }
    else if (bmi[i] > 25 && bmi[i] < 30)
                      {
      values[i] = "Nadwaga"
                      }
    else              {
      values[i] = "Otylosc"
                      }
                 }
  
  dane = data.frame(age, sex, height, weight, s_c, kids,eyes,hair,bmi,values)
  
  wykres = dane[7]
  labels = names(table(wykres))
  pct = round(table(wykres)/sum(table(wykres))*100)
  labels = paste(labels, pct) 
  labels = paste(labels,"%",sep="") 
  pie(table(wykres),labels = labels, col=rainbow(length(labels)),main="Kolor oczu")
  
  
  wykres = dane[8]
  labels = names(table(wykres))
  pct = round(table(wykres)/sum(table(wykres))*100)
  labels = paste(labels, pct) 
  labels = paste(labels,"%",sep="") 
  pie(table(wykres),labels = labels, col=rainbow(length(labels)),main="Kolor wlosow")
  
  with(dane, hist(weight))
  with(dane, hist(bmi))
  
  return(list(dane,summary(dane)))
  
}
