library(numbers)
library(distr)

# Z pierwszego projektu: rozklad jednostajny

prng <- function(n, m = 2^31-1, par1 = 16807, plots = FALSE){
  if(n == 1){
    return(sample(1:(m-1),1)/m)
  }
  # Warunki
  if(n!=floor(n)|m!=floor(m)|par1!=floor(par1)|n<2|m<1|par1<1){
    return(message('Co najmniej jeden parametr nie nalezy do liczb naturalnych'))
  }
  if(m<100000){       ### Sprawdzenie pierwiastkow pierwotnych dla wiekszych m trwa za dlugo
    if(par1 %in% numbers::primroot(m,all = TRUE) == FALSE){
      message('Uwaga: a nie jest pierwiastkiem pierwotnym m, wyniki moga byc niepoprawne.')
    }
  }
  if(par1 > sqrt(m)){
    return(message('Parametr a jest za duzy wzgledem parametru m.'))
  }
  # Algorytm
  rand <- c()
  rand[1] <- sample(1:(m-1),1)
  r <- m%%par1
  q <- floor(m/par1)
  for(i in 1:(n-1)){
    k <- floor(rand[i]/q)
    rand[i+1] <- par1*(rand[i]-k*q)-k*r
    if(rand[i+1]<0){
      rand[i+1] <- rand[i+1] + m
    }
  }
  unif <- rand/(m-1)
  if(length(unique(unif))!=length(unif)){
    message('Uwaga: wygenerowane wartosci powtarzaja sie.')
  }
  # Wykresy
  if(plots == TRUE){
    hist(unif, sub = paste('m = ', m, ', a = ', par1), col.sub='red')
    plot(ecdf(unif), main = 'Dystrybuanta empiryczna', sub = paste('m = ', m, ', a = ', par1),  col.sub='red', xlab = 'x', ylab = 'y')
  }
  unif
}


# Rozklad wykladniczy

prng_exp <- function(n, L){
  return(-(1/L)*log(prng(n)))
}
# Wykresy
plot(ecdf(prng_exp(10000,3)), main = 'Dystrybuanta empiryczna (moja funkcja)')
plot(ecdf(rexp(10000,3)), main = 'Dystrybuanta empiryczna (funkcja R)')
hist(prng_exp(10000,3), main = 'Histogram (moja funkcja)')
hist(rexp(10000,3), main = 'Histogram (funkcja R)')
# Statystyki
mean(prng_exp(10000,3))
sd(prng_exp(10000,3))
quantile(prng_exp(10000,3))


# Rozklad Weibulla

prng_weibull <- function(n, L, k){
  return(L*((log(1/(1-prng(n))))^(1/k)))
}
# Wykresy
plot(ecdf(prng_weibull(10000,22,3)), main = 'Dystrybuanta empiryczna (moja funkcja)')
plot(ecdf(rweibull(10000,3,22)), main = 'Dystrybuanta empiryczna (funkcja R)')
hist(prng_weibull(10000,22,3), main = 'Histogram (moja funkcja)')
hist(rweibull(10000,3,22), main = 'Histogram (funkcja R)')
# Statystyki
mean(prng_weibull(10000,22,3))
sd(prng_weibull(10000,22,3))
quantile(prng_weibull(10000,22,3))


# Rozklad arcsin

prng_arcsin <- function(n){
  return(1/2-(1/2)*cos(pi*prng(n)))
}
# Wykresy
plot(ecdf(prng_arcsin(10000)), main = 'Dystrybuanta empiryczna (moja funkcja)')
plot(ecdf(r(Arcsine())(10000)), main = 'Dystrybuanta empiryczna (funkcja R)')
hist(prng_arcsin(10000), main = 'Histogram (moja funkcja)')
hist(r(Arcsine())(10000), main = 'Histogram (funkcja R)')
# Statystyki
mean(prng_arcsin(10000))
sd(prng_arcsin(10000))
quantile(prng_arcsin(10000))


# Rozklad Poissona (z wykorzystaniem prng_exp)

prng_poisson <- function(n,L){
  x <- c()
  for(i in 1:n){
    T <- 0
    m <- -1
    while(T < 1){
      T <- T + prng_exp(1,L)
      m <- m+1
    }
    x[i] <- m
  }
  return(x)
}
# Wykresy
hist(prng_poisson(10000,5), main = 'Histogram (moja funkcja)')
hist(rpois(10000,5), main = 'Histogram (funkcja R)')
hist(prng_poisson(10000,0.3), main = 'Histogram (moja funkcja)')
hist(rpois(10000,0.3), main = 'Histogram (funkcja R)')
# Statystyki
mean(prng_poisson(10000,5))
sd(prng_poisson(10000,5))
quantile(prng_poisson(10000,5))


# Rozklad normalny

prng_norm <- function(n, mu, sd, metoda = 'dexp'){
  if(metoda == 'dexp'){   ### Metoda podwojnego wykladniczego
    p <- c()
    for(i in 1:n){
      v <- prng(3)
      x <- -log(v[1])
      while(v[2] > exp(-0.5*(x-1)^2)){
        v <- prng(3)
        x <- -log(v[1])
      }
      if(v[3] < 0.5){
        x <- -x
      }
      p[i] <- x
    }
    return(p*sd+mu)
  }
  else if(metoda == 'bm'){   ### Metoda Boxa-Mullera
    p <- c()
    for(i in 1:n){
      v <- prng(2)
      R <- -2*log(v[1])
      T <- 2*pi*v[2]
      z <- c(sqrt(R)*cos(T), sqrt(R)*sin(T))
      p[i] <- sample(z,1)
    }
    return(p*sd+mu)
  }
  else{
    return('Nieznana metoda')
  }
}
# Wykresy
plot(ecdf(prng_norm(10000,200,40)), main = 'Dystrybuanta empiryczna (pod. wykladniczy)')
plot(ecdf(prng_norm(10000,200,40, metoda = 'bm')), main = 'Dystrybuanta empiryczna (Box-Muller)')
plot(ecdf(rnorm(10000,200,40)), main = 'Dystrybuanta empiryczna (funkcja R)')
hist(prng_norm(10000,200,40), main = 'Histogram (pod. wykladniczy)')
hist(prng_norm(10000,200,40, metoda = 'bm'), main = 'Histogram (Box-Muller)')
hist(rnorm(10000,200,40), main = 'Histogram (funkcja R)')
# Statystyki
mean(prng_norm(10000,200,40))
sd(prng_norm(10000,200,40))
quantile(prng_norm(10000,200,40))
mean(prng_norm(10000,200,40, metoda = 'bm'))
sd(prng_norm(10000,200,40, metoda = 'bm'))
quantile(prng_norm(10000,200,40, metoda = 'bm'))


# Problem - model wypadkowego kierowcy (tresc w sprawozdaniu)
n <- 10000
B <- 6
L <- 0.1
t <- c()
for(i in 1:n){
  w <- prng_exp(1,L)
  t[i] <- w
  while(w>B){
    w <- prng_exp(1,L)
    t[i] <- t[i] + w
  }
}
sum(t)/n