library(ggplot2)

Pochodna <- function(f, x, eps = 0.0001)
{
  return ((f(x + eps) - f(x))/eps)
}

Pochodna2 <- function(f, x, eps = 0.0001)
{
  return ((f(x + 2*eps) - 2 * f(x + eps) + f(x)) / (eps^2))
}

Zero <- function(x)
{
  return (0)
}

#Newton

Newton <- function(f,a,b, eps = 0.0001, max = 64)
{
  if(f(a) * f(b) >= 0)
    return ("Funkcja nie jest r�nych znak�w !!")
  
  if(Pochodna(f,a, eps) * Pochodna(f, b, eps) < 0)
    return ("Pierwsze pochodne s� r�nych znak�w !!")
  if(Pochodna2(f,a, eps) * Pochodna2(f, b, eps) < 0)
    return ("Drugie pochodne s� r�nych znak�w !!")
  
  wynik <- c()
  wynik[1] <- b
  wynik[2] <- a
  
  i = 2
  while(abs(f(wynik[i])) > eps & abs(wynik[i] - wynik[i - 1]) > eps)
  {
    wynik[i + 1] <- wynik[i] - f(wynik[i])/Pochodna(f, wynik[i], eps)
    i = i + 1 
      
    if(i == max)
    {
      wynik[i] <- "Przekroczony limit iteracji"
      break
    }
    
  }
    
  return (wynik[3:length(wynik)])
}

#Koniec Newtona
#--------------
#Sieczna

Sieczna <- function(f,a,b, eps = 0.0001, max = 64)
{
  if(f(a) * f(b) >= 0)
    return ("Funkcja nie jest r�nych znak�w !!")
  
  if(Pochodna(f, a, eps) * Pochodna(f, b, eps) < 0)
    return ("Pierwsze pochodne s� r�nych znak�w !!")
  
  wynik <- c()
  wynik[1] <- a
  wynik[2] <- b
  
  i = 2
  while(abs(f(wynik[i])) > eps & abs(wynik[i] - wynik[i - 1]) > eps)
  {
    wynik[i + 1] <- wynik[i] - f(wynik[i])*(wynik[i] - wynik[i - 1])/(f(wynik[i]) - f(wynik[i - 1]))
    i = i + 1
    
    if(i == max)
    {
      wynik[i] <- "Przekroczony limit iteracji"
      break
    }
  }
  return (wynik[3:length(wynik)])
  
}

#Koniec Siecznej
#--------------
#Bisekcja

Bisekcja <- function(f,a,b, eps = 0.0001, max = 64)
{
  if(f(a) * f(b) >= 0)
    return ("Funkcja nie jest r�nych znak�w !!")
  
  wynik <- c()
  wynik[1] <- a
  wynik[2] <- b
  
  i = 2
  while(abs(f(wynik[i])) > eps & abs(wynik[2] - wynik[1]) > eps)
  {
    i = i+1
    wynik[i] <- (wynik[1] + wynik[2]) / 2
    
    if(f(wynik[1]) * f(wynik[i]) < 0)
    {
      wynik[2] <- wynik[i]
    }
    else
    {
      wynik[1] <- wynik[i]
    }
    
    
    if(i == max)
    {
      wynik[i] <- "Przekroczony limit iteracji"
      break
    }
  }
  
  return (wynik[3:length(wynik)])
}

#Koniec Bisekcji
#---------------
#Przyk�ady 1
f <- function(x)
{
  return (x*x - 2)
}

data_n = Newton(f, 1, 3)
dataf_n = data.frame(x = data_n, y = f(data_n))

data_s = Sieczna(f, 1, 3)
dataf_s = data.frame(x = data_s, y = f(data_s))

data_b = Bisekcja(f, 1, 3)
dataf_b = data.frame(x = data_b, y = f(data_b))

ggplot(data.frame(x=c(1, 3), y = c(1, 3)), aes(x, y)) + stat_function(fun=f) + stat_function(fun=Zero,size = 1.1, color = "white") + geom_point(data = dataf_n, color="red")  + ggtitle("Newton")
ggplot(data.frame(x=c(1, 3), y = c(1, 3)), aes(x, y)) + stat_function(fun=f)  + stat_function(fun=Zero,size = 1.1, color = "white") + geom_point(data = dataf_s, color="blue") + ggtitle("Sieczna")
ggplot(data.frame(x=c(1, 3), y = c(1, 3)), aes(x, y)) + stat_function(fun=f)  + stat_function(fun=Zero,size = 1.1, color = "white") + geom_point(data = dataf_b, color="green") + ggtitle("Bisekcja")

dataf_n
dataf_s
dataf_b
#Przyk�ady 2
g <- function(x)
{
  return (x^3 + x^2 - 3)
}

data_n = Newton(g, 1, 10)
dataf_n = data.frame(x = data_n, y = g(data_n))

data_s = Sieczna(g, 1, 10)
dataf_s = data.frame(x = data_s, y = g(data_s))

data_b = Bisekcja(g, 1, 10)
dataf_b = data.frame(x = data_b, y = g(data_b))

ggplot(data.frame(x=c(1, 10), y = c(1, 10)), aes(x, y)) + stat_function(fun=g) + stat_function(fun=Zero,size = 1.1, color = "white") + geom_point(data = dataf_n, color="red")  + ggtitle("Newton")
ggplot(data.frame(x=c(1, 10), y = c(1, 10)), aes(x, y)) + stat_function(fun=g)  + stat_function(fun=Zero,size = 1.1, color = "white") + geom_point(data = dataf_s, color="blue") + ggtitle("Sieczna")
ggplot(data.frame(x=c(1, 10), y = c(1, 10)), aes(x, y)) + stat_function(fun=g)  + stat_function(fun=Zero,size = 1.1, color = "white") + geom_point(data = dataf_b, color="green") + ggtitle("Bisekcja")

dataf_n
dataf_s
dataf_b

