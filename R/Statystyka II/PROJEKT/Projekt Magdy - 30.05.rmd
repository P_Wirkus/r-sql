---
title: "Projekt"
author: "Magdalena Sienkiewicz"
date: "8 05 2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(tree)
library(randomForest)
library(gbm)
library(plotly)
```
Poniższy projekt dotyczy analizy danych dotyczących udaru mózgu. Na początku musimy wczytać zbiór.

```{r data, echo=FALSE}
udar <- read.csv("C:/Users/Patryk/Desktop/healthcare-dataset-stroke-data.csv")
```
Zanim przejdziemy do analizy i modelowania musimy wyczyścić dane. Zaczynamy od sprawdzenia zakresów zmiennych.

```{r , echo=FALSE}
summary(udar)
```
To tak, id nas tu nie interesuje, ew można sprawdzić czy przez pomyłkę nie dubluje się któryś. W 'gender' trzeba sprawdzić czy ta 'Other' to jest NaN czy coś innego. Wiek jest od ok. 1 miesiąc do 82 lata (tak mi się wydaje), więc to jest raczej bez poprawek. 'hypertension' to jest nadciśnienie i odpowiedzi są 0-1 jak 'Nie'-'Tak' - trzeba tu sprawdzić czy przypadkiem nie ma innych wpisanych wartości. 'ever_married' wygląda dobrze. 'work_type' - 'children' określa, że to jest dziecko, dobrze wygląda. 'Residence_type' - good. 'avg_glucose_level' - średni poziom glukozy może taki być, chociaż zwróćmy uwagę, że średnia wychodzi ok 106 (norma, taka ogólna, to 100, może tam jest inna). W 'bmi' mamy sporo braków danych, ale trzeba to sprawdzic jeszcze pod względem zakresu, bo tutaj tego nie mamy. Znowu w 'smoking_status' mamy sporo braków - trzeba to będzie uzupełnić 'no_information' lub coś w tym stylu. Pozostało 'stroke', czyli udar - wartości 0-1, ale trzeba sprawdzić, czy przez przypadek nie ma gdzieś wartości pomiędzy.

Zauważmy, że w zmiennej płec nie mamy braku danych tylko po prostu trzecią kategorię.

Z TEGO TUTAJ NA RAZIE REZYGNUJEMY - BO NIE ZROBIMY KORELACJI Z NIMI.

Dalej, zamieniamy typy zmiennych, które są binarne.
```{r}
#udar[,'hypertension'] <- as.factor(udar[,'hypertension'])
#udar[,'heart_disease'] <- as.factor(udar[,'heart_disease'])
udar[,'stroke'] <- as.factor(udar[,'stroke'])
#sapply(udar, class)
```
ALE TYLKO Z TEGO FACTOR 

Ponownie wyświetlamy podsumowanie.
```{r , echo=FALSE}
summary(udar)
```

Zatem wszystkie dane w tej kolumnie mają wartości 0 lub 1. Od razu, poprzez to z sumą, wiemy, że nie mamy tu brakóW danych.
Zajmiemy się teraz zmienną 'heart_disease' tak jak wyżej (potem może poprawić, żeby to jakoś ładniej wyglądało).

Czyli, jak wyżej, nie mamy tu nic do roboty. Kolejna zmienna to 'stroke', gdzie musimy tylko sprawdzić tak jak poprzednio, czy nie mamy wartości pomiędzy 0 a 1. 

No dobrze, zatem ta zmienna jest poprawnie zapisana, bez braków danych. Została nam jedna zmienna do sprawdzenia jeszcze, 'bmi'.

Mamy w niej sporo brakóW. Najpierw jednak sprawdźmy minimalny i maksymalny dostępny poziom oraz dlaczego nie jest to zmienna liczbowa tylko factor. Spróbujemy zmienić to.
```{r}
sapply(udar, class)
```
Źródło do transformacji (samo transform z czegoś innego ale incognito).
# https://www.geeksforgeeks.org/convert-factor-to-numeric-and-numeric-to-factor-in-r-programming/
```{r}
udar
udar = transform(udar, bmi = as.numeric(as.character(bmi)))
sapply(udar, class)
```
Ok, trochę dziwnie się wyświetla, ale chyba 'bmi' jest już numeryczne. To sprawdźmy teraz min i max.
```{r}
min(udar[['bmi']])
max(udar[['bmi']])
mean(udar[['bmi']])
```
oo, no i kalpa. W każdej z powyższych dostajemy NA. Nawet w tym momencie nie możemy zamienić tych wartości bo nie mamy na co.

Trzeba coś z tym zrobić. Na poniższej stronie jest rozwiązanie.
# https://statisticsglobe.com/r-max-min-function/
```{r}
min(udar[['bmi']], na.rm = TRUE)
max(udar[['bmi']], na.rm = TRUE)
mean(udar[['bmi']], na.rm = TRUE)
```
No dobrze, biorąc pod uwagę, że mówimy tu o bmi, to poziom maksymalny jako 97.6 wydaję się niemożliwy. Sprawdźmy w internecie jakie są przewidziane wartości.
Taka wartość maksymalna oznaczałaby, że ta osoba ważyła ok. 280 kg. Spróbujmy wyświetlić cały wiersz.
```{r}
which(udar[['bmi']] == 97.6)
udar[2129,]
```
Zatem są to dane 17-latka, z nadciśnieniem, bez chorób serca, kawaler ze wsi, glukoza w normie, brak danych o paleniu, bez udaru. Tak duża waga u tak młodego chlopaka? Jedyne co mogłoby wskazywać na poprawność tej danej to nadciśnienie, ale prawie 300 kg?
```{r}
#sort(udar[['bmi']], decreasing=FALSE)
udar[order(udar[,'bmi']), , drop=FALSE]
udar[order(udar[,'bmi'], decreasing = TRUE), , drop=FALSE]

```
Teraz widzimy, że mamy 2 wartości powyżej 90 i 2 powyżej 70. NArysujmy jeszcze histogram.
```{r}
hist(udar[,'bmi'])
boxplot(udar[,'bmi'])
```
Dobra, to nwm co teraz zrobić. Wg boxplota, wszystkie wartości powyżej ok 50 są odstające, ale widzimy 4 oddzielne kropki (wspomniane wczesniej wartości powyżej 70). Może najlepiej byłoby je usunąć?
```{r}
udar[,'bmi'] <- replace(udar[,'bmi'],which(udar[,'bmi']>70), NA)
max(udar[,'bmi'], na.rm = TRUE)
```
Zatem usunęliśmy 4 najbardziej odstające dane. Teraz musimy uzupełnić braki danych. Z wcześniejszych działań wiemy, że jedyne braki danych mamy w klumnie 'bmi'. Musimy jeszcze ustalić w jaki sposób uzupełniemy dane, ponieważ nie będziemy usuwać tych rekordów. Możliwe są średnia bądź mediana. Sprawdźmy najpierw jakie są to wartości.

USUNĄĆ WSZYSTKIE ODSTAJĄCE - WE WSZYSTKIM!!!

ile braków danych w jakich kolumnach mamy.
```{r}
colSums(is.na(udar))
```

```{r}
mean(udar[,'bmi'], na.rm = TRUE)
median(udar[,'bmi'], na.rm = TRUE)
```
Zatem sa one do siebie zbliżone. Uzupełnijmy może dane medianą.
```{r}
udar[,'bmi'] <- replace(udar[,'bmi'],which(is.na(udar[,'bmi'])), 28)
sum(is.na(udar[,'bmi']))
```
I ponownie sprawdzamy braki danych.
```{r}
colSums(is.na(udar))
```

W tym momencie możemy sprawdzić, czy w naszych danych obserwujemy w ogóle udar dla obydwu płci. (YouTube StatQuest)
```{r}
xtabs(~ stroke + gender, data = udar)
```
Dobrze, czyli mamy udar dla obydwu płci ('Other' nie miał/o udaru).

Jeszcze trochę eksploracji danych przed nami. Sprawdźmy histogramy zmiennych numerycznych (te, których jeszcze nie robiliśmy).
```{r}
hist(udar[,'age'])
hist(udar[,'avg_glucose_level'])
```
No tak, zapomnieliśmy o średnim poziomie cukru. Jak widzimy, mamy tu mały 'ogon'.
```{r}
sum(udar[,'avg_glucose_level']>250)
boxplot(udar[,'avg_glucose_level'])
hist(udar[,'avg_glucose_level'])
```
Teraz tak, takie wartości są jak najbardziej możeliwe, gdy ktoś ma cukrzycę. Ponadto nie wiemy jaki jest poprawny zakres. 
Nie zmieniamy nic w tej kolumnie, ponieważ cukrzyca ma znaczący wpływ na możliwość wystapienia udaru, a wysoki poziom cukru może świadczyć o cukrzycy (>200 to na pewno cukrzyca).

Zatem przechodzimy do następnej części projektu EKSPLORACYJNA ANALIZA DANYCH a właściwie dalej ją robimy (patrz lab regresja logistyczna).

USUNĄĆ ID, bo nic nie wniesie do modelu. 
```{r}
udar <- udar %>% select(-id)
```

spradzenie zbalansowania zbioru (bo zmienna stroke przyjmuje dwie wartości).
```{r}
table(udar$stroke)
ggplot(udar, aes(x = stroke, fill = stroke)) + geom_bar() + theme_bw()
```
Zatem mamy zdecydowanie niezbilansowany zbiór. Co w takim razie zrobić? Spróbujemy oversamplingu.
```{r}
library(ROSE)
```

No to spróbujmy stworzyć nowe, zbalansowane dane. 
Doczytać 
https://www.analyticsvidhya.com/blog/2016/03/practical-guide-deal-imbalanced-classification-problems/
```{r}
udar_rose <- ovun.sample(stroke ~ ., data=udar)$data
table(udar_rose$stroke)
ggplot(udar_rose, aes(x = stroke, fill = stroke)) + geom_bar() + theme_bw()
```
Myślałam, że będzą raczej dodatkowe dane niż zamiana w przypadkowych wierszach. Ale cóż, zobaczymy jak to będzie wyglądało w zależności od zbalansowanego lub nie zbioru.

Sprawdźmy teraz podsumowanie (czy czasem nie mamy ZNOWU dopisanych zmiennych poza zakresem)
```{r , echo=FALSE}
summary(udar_rose)
```



Zanim przejdziemy do medelowania, to próbujemy przeskalowania zmiennych. Właściwie przeskalować musimy tylko zmienne 'age', 'avg_..' i 'bmi', bo pozostałe są 0-1 lub kategoryczne (tekstowe).
Skorzystamy tutaj z książki 'Język R i analiza danych ...' Listing 4.9

# mean_age <- mean(udar_rose$age)
# std_age <- sd(udar_rose$age)
# print(mean_age + c(-std_age, std_age))
# udar2 <- udar_rose
# udar2$age_s <-(udar_rose$age - mean_age)/std_age
# head(udar2)

Można róWnież użyć po prostu funkcji scale. I przy niej zostaniemy
```{r}
udar2 <- udar_rose
udar2$avg_glucose_level <- scale(udar2$avg_glucose_level)
udar2$age <- scale(udar2$age)
udar2$bmi <- scale(udar2$bmi)
head(udar2)
```
Zatem mamy już przeskalowane zmienne numeryczne (0 jest teraz chyba środkiem).

To teraz spróbujemy modelu regresji logistycznej.
```{r}
library(corrplot)
library(mctest)
library(caret)
library(pROC)
```
Sprawdzimy teraz czy nie mamy jakiś współliniowych zmiennych. Najpierw spróbujemy korelacji.
```{r}
#M <- cor(udar2[ , c(2,3,4,8,9)])
#corrplot(M, method = "circle", type = "full", order = "hclust", addrect = 4)
#colinear_cols <- findCorrelation(M, cutoff = 0.7, names = TRUE)
#print(colinear_cols)
#print(M)
# wdbc <- wdbc %>% select(-one_of(colinear_cols))
```
Wartości w macierzy M pokazują, że nie mamy współliniowości: wartości korelacji są mniejsze od 0.4.

W kolejnej części zajmiemy się zamodelowaniem zmiennej stroke. Musimy tu użyć randomforest, a poza tym 2 inne metody.

Z racji, że zmienna modelowana jest binarna, to nie robimy regresji liniowej, tylko próbujemy regresję logistyczną.

Na razie bez standaryzacji i podziału na zbiór testowy i treningowy.
Regresja logistyczna dla zbilansowanych danych.
```{r}
set.seed(1234)
udar_log_reg <- glm(data = udar2, formula = stroke ~ ., family = binomial(link = "logit"))
summary(udar_log_reg)
```
Mamy tutaj dużo nieistotnych zmiennych, zatem wykorzystujemy regresję krokową.
```{r}
null_model <- glm(data = udar2, formula = stroke ~ 1, family = binomial(link = "logit"))
full_model <- glm(data = udar2, formula = stroke ~ ., family = binomial(link = "logit"))
step_model <- step(null_model, scope = list(lower = null_model, upper = full_model),
                   direction = "both") # c("both", "backward", "forward")
summary(step_model)
```
Mamy wciąż kilka nieistotnych zmiennych. Usuwamy je ręcznie.
glm(formula = stroke ~ age + avg_glucose_level + hypertension + 
    smoking_status + heart_disease + work_type + bmi + Residence_type, family = binomial(link = "logit"), 
    data = udar_rose)
```{r}
set.seed(1234)
udar_log_reg <- glm(data = udar2, formula = stroke ~ age + avg_glucose_level + hypertension + smoking_status + heart_disease + work_type + bmi, family = binomial(link = "logit"))
summary(udar_log_reg)
```
```{r}
set.seed(1234)
udar_log_reg <- glm(data = udar2, formula = stroke ~ age + avg_glucose_level + hypertension + heart_disease + work_type + bmi, family = binomial(link = "logit"))
summary(udar_log_reg)
```
Dla tego modelu też sprawdzimy tabelę pomyłek - jest o ok. 10 osób lepsza na każdy błąd, więc chyba raczej idziemy dalej.


```{r}
set.seed(1234)
udar_log_reg <- glm(data = udar2, formula = stroke ~ age + avg_glucose_level + hypertension + heart_disease + bmi, family = binomial(link = "logit"))
summary(udar_log_reg)
```
```{r}
set.seed(1234)
udar_log_reg <- glm(data = udar2, formula = stroke ~ age + avg_glucose_level + hypertension + heart_disease, family = binomial(link = "logit"))
summary(udar_log_reg)
```

Teraz mamy tylko zmienne istotne.
Pozostaje jeszcze sprawdzić istotność naszych modeli.
Możemy sorawdzić jak zmienia się prawdopodobieństwo, gdy zmienimy jedną zmienną o ileś procent.
No i miary do policzenia.

Na razie policzymy tabelę pomyłek.
```{r}
udar_pred <- predict(udar_log_reg, udar2, type = "response") %>%
  bind_cols(udar2 %>% select(stroke), preds = .)
```

```{r}
cut05 <- udar_pred %>%
  mutate(predicted = ifelse(preds >= 0.5, 1, 0)) %>%
  select(-preds) %>%
  select(predicted, stroke) %>%
  mutate_all(list(~ factor(., levels = c(1, 0)))) %>%
  table()
cut05
```
No to widzimy, że dla wartości 1 regresja logistyczna niezbyt dobrze sobie radzie (ok 198/(1298+198) dobrych przewidywań)
Policzymy jeszcze accuracy.
```{r}
acc <- sum(diag(cut05)) / sum(cut05)
print(acc)
```
I inne metryki.
```{r}
# Precision = TP / (TP + FP)
pre <- cut05[1, 1] / sum(cut05[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cut05[1, 1] / sum(cut05[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cut05[2, 2] / sum(cut05[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar_pred$stroke, udar_pred$preds)
plot(wdbc_roc)
```
Ten model może nie jest tragiczny, ale zdecydowanie mógłby być lepszy.
Najpierw spróbujmy z random_state. Niestety ten pomysł nie był dobry.
W jaki inny sposób można poprawić regresję logistyczną?

Przejdźmy teraz do drugiego modelu: lasów losowych.
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(tree)
library(randomForest)
library(gbm)
library(plotly)
```

Dobrze, to teraz spróbujemy zbioróW testowych i treningowych.
```{r}
index <- sample(2, nrow(udar2), replace = TRUE, prob = c(0.8, 0.2))
udar_train <- udar2[index==1, ]
udar_test <- udar2[index==2, ]
```

Przykładowy las losowy Ctrl+C z zajęć
```{r}
set.seed(1234)
## Random Forests
rf_model <- randomForest(stroke ~ .,
                         data = udar_train,
                         importance = TRUE,
                         ntree = 5)

rf_model2 <- randomForest(stroke ~ .,
                         data = udar_train,
                          importance = TRUE,
                          ntree = 100)
rf_model
rf_model2
```
confusion matrix z modelu: po lewej są dane prawdziwe, a u góry - predykcje.

Metryki
```{r}
# predykcje z rf_model
print('rf_model')
udar_rf_pred <- predict(rf_model, udar_test)
# confusion matrix
cm <- table(udar_test$stroke, udar_rf_pred)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar_test$stroke, as.numeric(udar_rf_pred))
plot(wdbc_roc)
print('rf_model2')
# predykcje z rf_model2
udar_rf_pred2 <- predict(rf_model2, udar_test)
# confusion matrix
cm2 <- table(udar_test$stroke, udar_rf_pred2)
acc2 <- sum(diag(cm2)) / sum(cm2)
print(acc2)
# Precision = TP / (TP + FP)
pre2 <- cm2[1, 1] / sum(cm2[1, ])
print(pre2)
# Recall (Sensivity) = TP / (TP + FN)
rec2 <- cm2[1, 1] / sum(cm2[, 1])
print(rec2)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f12 <- 2 * pre2 * rec2 / (pre2 + rec2)
print(f12)
# Specificity = TN / (TN + FP)
spec2 <- cm2[2, 2] / sum(cm2[, 2])
print(spec2)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc2 <- roc(udar_test$stroke, as.numeric(udar_rf_pred2))
plot(wdbc_roc2)
```
Zosatajemy przy modelu 'rf_model' - jest dobre na testowym i na treningowym.
Podejście z YouTube - StatQuest - dodać proximity=TRUE żeby mieć macierz pomyłek.


I sprawdzamy co możemy wyciągnąć z modelu.
```{r}
summary(rf_model)
rf_model$importance
rf_model$importanceSD
```
Patrząc na MeanDecreaseGini najbardziej istotne zmienne to (malejąco): age, avg_glucose_level, bmi, smoking_status i ever_married. W przypadku MeanDecreaseAccuracy pierwsze 4 istotne zmienne są takie same + work_type.

Sama funkcja importance z pakietu randomForest.
```{r}
importance(rf_model)
```

I jeszcze metoda graficzna.
```{r}
varImpPlot(rf_model)
```
Tak więc nwm czy można na podstawie tego usuwać zmienne czy też nie. Trzeba doczytać !!!

# Model 3

Drzewo decyzyjne

Tak jak na zajęciach zaczynamy od prostego modelu
```{r}
tree <- tree(stroke ~ ., data = udar2)
tree
summary(tree)
```
Sprawdźmy jakieś metryki, żeby się zorientować czy to co dostaliśmy jest dobre.
```{r}
# predykcje z tree
print('tree')
udar_tree_pred <- predict(tree, udar2, type='class')
```


```{r}
# confusion matrix
cm <- table(udar2$stroke, udar_tree_pred)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar2$stroke, as.numeric(udar_tree_pred))
plot(wdbc_roc)
```
Zatem możemy myśleć (o ile wersja z 29.05.2021 jest poprawnie liczona), że ten model można jeszcze polepszyć.

Możemy, jak na zajęciach, próbować zwiększyć ilość liści (albo ustawić max ilość na liściu, itp.).
```{r}
tree2 <- tree(stroke ~ ., data = udar2, 
              #mindev = 0.001
              mindev = 0.005)
tree2
summary(tree2)
```

No to teraz patrzymy, czy z macierzy pomyłek coś wyciągniemy.
```{r}
udar_tree_pred2 <- predict(tree2, udar2, type='class')
# confusion matrix
cm <- table(udar2$stroke, udar_tree_pred2)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar2$stroke, as.numeric(udar_tree_pred2))
plot(wdbc_roc)
```
Czyli jest lepiej niż poprzednio.
sprawdźmy, co się stanie jak uwzględnimy podział na zbiór treningowy i testowy.
```{r}
tree2 <- tree(stroke ~ ., data = udar_train, 
              #mindev = 0.001
              mindev = 0.005)
tree2
summary(tree2)
```

```{r}
print('treningowy')
udar_tree_pred2 <- predict(tree2, udar_train, type='class')
# confusion matrix
cm <- table(udar_train$stroke, udar_tree_pred2)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar_train$stroke, as.numeric(udar_tree_pred2))
plot(wdbc_roc)

print('testowy')
udar_tree_pred2 <- predict(tree2, udar_test, type='class')
# confusion matrix
cm <- table(udar_test$stroke, udar_tree_pred2)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar_test$stroke, as.numeric(udar_tree_pred2))
plot(wdbc_roc)
```
Zatem wyniki są niezłe.

Inny podział na treningowy/testowy.
```{r}
set.seed(1234)
index <- sample(2, nrow(udar2), replace = TRUE, prob = c(0.7, 0.3))
udar_train <- udar2[index==1, ]
udar_test <- udar2[index==2, ]
```
I ponownie tworzymy model.
```{r}
set.seed(1234)
tree2 <- tree(stroke ~ ., data = udar_train, 
              #mindev = 0.001
              mindev = 0.003)
tree2
summary(tree2)
```

```{r}
print('treningowy')
udar_tree_pred2 <- predict(tree2, udar_train, type='class')
# confusion matrix
cm <- table(udar_train$stroke, udar_tree_pred2)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar_train$stroke, as.numeric(udar_tree_pred2))
plot(wdbc_roc)

print('testowy')
udar_tree_pred2 <- predict(tree2, udar_test, type='class')
# confusion matrix
cm <- table(udar_test$stroke, udar_tree_pred2)
acc <- sum(diag(cm)) / sum(cm)
print(acc)
# Precision = TP / (TP + FP)
pre <- cm[1, 1] / sum(cm[1, ])
print(pre)
# Recall (Sensivity) = TP / (TP + FN)
rec <- cm[1, 1] / sum(cm[, 1])
print(rec)
# Miara F1 = 2*Precision*Recall / (Precision + Recall)
f1 <- 2 * pre * rec / (pre + rec)
print(f1)
# Specificity = TN / (TN + FP)
spec <- cm[2, 2] / sum(cm[, 2])
print(spec)
# Krzywa ROC (receiver operating characteristic curve) i AUC
wdbc_roc <- roc(udar_test$stroke, as.numeric(udar_tree_pred2))
plot(wdbc_roc)
```
Jak na razie jest to najlepsze drzewo ,tylko czy aby nie przeuczone?
