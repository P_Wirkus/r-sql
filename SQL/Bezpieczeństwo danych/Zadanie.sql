-- ZADANIA:

-- [brak=-2 pkt] Dopisz przykładowe rekordy z walkami (dodaj co najmniej 20 walk):

INSERT INTO walka VALUES ('22','1','3','0','1','2020-01-03');
INSERT INTO walka VALUES ('23','3','1','1','1','2020-01-03');
INSERT INTO walka VALUES ('24','6','1','0','10','2020-01-04');
INSERT INTO walka VALUES ('25','7','2','2','9','2020-01-04');
INSERT INTO walka VALUES ('26','6','3','2','8','2020-01-06');
INSERT INTO walka VALUES ('27','5','9','2','7','2020-01-06');
INSERT INTO walka VALUES ('28','4','7','1','7','2020-01-06');
INSERT INTO walka VALUES ('29','3','6','1','6','2020-01-07');
INSERT INTO walka VALUES ('30','2','4','0','6','2020-01-08');
INSERT INTO walka VALUES ('31','2','4','2','3','2020-01-08');
INSERT INTO walka VALUES ('32','8','5','0','10','2020-01-09');
INSERT INTO walka VALUES ('33','9','6','2','5','2020-01-09');
INSERT INTO walka VALUES ('34','7','7','1','2','2020-01-09');
INSERT INTO walka VALUES ('35','10','8','2','1','2020-01-09');
INSERT INTO walka VALUES ('36','9','1','0','8','2020-01-13');
INSERT INTO walka VALUES ('37','7','10','2','8','2020-01-13');
INSERT INTO walka VALUES ('38','6','2','1','5','2020-01-14');
INSERT INTO walka VALUES ('39','5','3','2','4','2020-01-16');
INSERT INTO walka VALUES ('40','4','4','1','3','2020-01-16');
INSERT INTO walka VALUES ('41','6','5','2','9','2020-01-18');
INSERT INTO walka VALUES ('42','5','6','1','10','2020-01-18');

-- Przyklad:
-- Wyświetlić imię, nazwisko, wagę najcięższego zawodnika
-- oraz nazwę klubu, w którym trenuje.

-- SELECT b.imie, b.nazwisko, b.waga, k.nazwa
-- FROM bokser b JOIN klub k ON b.klub_id=k.id
-- WHERE b.waga = (SELECT MAX(waga) FROM bokser);

-- [5 pkt, po 0,5 pkt za zadanie] Dopisać zapytania SQL 
-- realizujące poniższe zadania:

-- 1. Wypisać sześciu zawodników o największych wagach.
--    Jeżeli kilku ma taką samą wagę, wypisać tych o
--    nazwiskach wcześniejszych alfabetycznie.

SELECT * FROM (SELECT id, imie, MIN(nazwisko) AS nazwisko, data_ur, waga,wzrost, klub_id FROM bokser GROUP BY waga) t ORDER BY t.waga DESC LIMIT 6;

-- 2. Wypisać kategorie wagowe, w których nie występował żaden bokser.

SELECT * FROM kat_wagowa WHERE id NOT IN (SELECT kat_wagowa_id FROM walka GROUP BY kat_wagowa_id);

-- 3. Wypisać tych zawodników, dla których imiona i nazwiska
--    są takie same, ale wartości indentyfikatorów są różne.

SELECT a.* FROM bokser a JOIN (SELECT imie, nazwisko, COUNT(*) FROM bokser GROUP BY imie, nazwisko HAVING COUNT(*) > 1) b ON 
	a.imie = b.imie AND a.nazwisko = b.nazwisko;

-- 4. Wypisać nazwy klubów, w których nikt nie walczy.

SELECT nazwa FROM klub k WHERE k.id IN (SELECT klub_id FROM bokser b WHERE b.id NOT IN (SELECT bokser_id1 FROM walka) AND b.id NOT IN (SELECT bokser_id2 FROM walka));

-- 5. Wypisać nazwiska bokserów, którzy nie przegrali żadnej walki.

SELECT nazwisko FROM bokser b WHERE b.id NOT IN (SELECT bokser_id1 FROM walka w WHERE w.zwyciezca = 2) AND b.id NOT IN (SELECT bokser_id2 FROM walka w WHERE w.zwyciezca = 1);

-- 6. Wypisać nazwy klubów z liczbą zwycięstw ich zawodników.

SELECT nazwa, SUM(zwyciestwa) FROM klub k
INNER JOIN (SELECT klub_id, IFNULL(l.c, 0) + IFNULL(r.c, 0) AS zwyciestwa FROM bokser b          
LEFT JOIN (SELECT bokser_id1, COUNT(bokser_id1) AS c FROM walka WHERE zwyciezca = 1 GROUP BY bokser_id1) l
ON b.id = l.bokser_id1
LEFT JOIN (SELECT bokser_id2, COUNT(bokser_id2) AS c FROM walka WHERE zwyciezca = 2 GROUP BY bokser_id2) r
ON b.id = r.bokser_id2) kl
ON k.id = kl.klub_id
GROUP BY k.id; 

-- 7. Wypisać nazwy klubów, których zawodnicy wygrali
--    ponad 20% wszystkich walk.

SELECT nazwa FROM klub k
LEFT JOIN
(SELECT klub_id, SUM(IFNULL(lz.c,0) + IFNULL(rz.c, 0)) AS zwyciezkie, SUM(IFNULL(l.c,0) + IFNULL(r.c, 0)) AS wszystkie FROM bokser b
LEFT JOIN (SELECT bokser_id1, COUNT(bokser_id1) AS c FROM walka GROUP BY bokser_id1) l
ON b.id = l.bokser_id1
LEFT JOIN (SELECT bokser_id2, COUNT(bokser_id2) AS c FROM walka GROUP BY bokser_id2) r
ON b.id = r.bokser_id2
LEFT JOIN (SELECT bokser_id1, COUNT(bokser_id1) AS c FROM walka WHERE zwyciezca = 1 GROUP BY bokser_id1) lz
ON b.id = lz.bokser_id1
LEFT JOIN (SELECT bokser_id2, COUNT(bokser_id2) as c FROM walka WHERE zwyciezca = 2 GROUP BY bokser_id2) rz
ON b.id = rz.bokser_id2
GROUP BY klub_id) w
ON k.id = w.klub_id
WHERE w.zwyciezkie >= w.wszystkie * 0.2 AND w.zwyciezkie != 0;

-- 8. Wypisać nazwiska bokserów z liczbą ich zwycięstw,
--    w każdej startowanej kategorii wagowej.
--    Wynik posortuj malejęco po ilości zwycięstw, 
--    następnie alfabetycznie po nazwisku i imieniu.

SELECT nazwisko, cr.kat_wagowa_id, IFNULL(l.c1,0) + IFNULL(r.c2,0) as zwyciestwa FROM (SELECT b.nazwisko,b.imie, b.id AS bokser_id, w.id AS kat_wagowa_id FROM bokser b, kat_wagowa w) cr
LEFT JOIN (SELECT kat_wagowa_id, bokser_id1, COUNT(bokser_id1) AS c1 FROM walka WHERE zwyciezca = 1 GROUP BY bokser_id1, kat_wagowa_id) l
ON cr.bokser_id = l.bokser_id1 AND cr.kat_wagowa_id = l.kat_wagowa_id
LEFT JOIN (SELECT kat_wagowa_id, bokser_id2, COUNT(bokser_id2) AS c2 FROM walka WHERE zwyciezca = 2 GROUP BY bokser_id2, kat_wagowa_id) r
ON cr.bokser_id = r.bokser_id2 AND cr.kat_wagowa_id = r.kat_wagowa_id
WHERE IFNULL(l.c1,0) + IFNULL(r.c2,0) != 0
ORDER BY zwyciestwa DESC, nazwisko, cr.imie;


-- 9. Dla każdego klubu wypisać maksymalną liczbę zwycięstw
--    jednego zawodnika tego klubu.

SELECT id, nazwa, ranking, MAX(tab.s) AS zwyciestwa FROM klub k
JOIN (SELECT klub_id, SUM((IFNULL(cb1,0) + IFNULL(cb2,0))) AS s FROM bokser b
LEFT JOIN (SELECT bokser_id1, COUNT(bokser_id1) AS cb1 FROM walka WHERE zwyciezca = 1 GROUP BY bokser_id1) l
ON b.id = l.bokser_id1
LEFT JOIN (SELECT bokser_id2, COUNT(bokser_id2) AS cb2 FROM walka WHERE zwyciezca = 2 GROUP BY bokser_id2) r
ON b.id = r.bokser_id2 GROUP BY imie, nazwisko) AS tab
ON k.id = tab.klub_id GROUP BY klub_id;


-- 10. Dla każdego klubu posiadającego zawodnika wypisać jego nazwę oraz
--     imię, nazwisko i wagę najlżejszego i najcięższego zawodnika.

SELECT nazwa, m1.imie, m1.nazwisko, m1.waga, m2.imie, m2.nazwisko, m2.waga FROM klub kk
INNER JOIN 
(SELECT k.klub_id, k.imie, k.nazwisko, k.waga FROM bokser k INNER JOIN
(SELECT klub_id, MIN(waga) AS minimum FROM bokser GROUP BY klub_id) k1
ON k.klub_id = k1.klub_id WHERE k.waga = k1.minimum GROUP BY k.klub_id) m1
ON kk.id = m1.klub_id
INNER JOIN
(SELECT k.klub_id, k.imie, k.nazwisko, k.waga FROM bokser k INNER JOIN
(SELECT klub_id, MAX(waga) AS minimum FROM bokser GROUP BY klub_id) k1
ON k.klub_id = k1.klub_id WHERE k.waga = k1.minimum GROUP BY k.klub_id) m2
ON kk.id = m2.klub_id;

