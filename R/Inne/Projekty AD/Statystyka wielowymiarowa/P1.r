
library(matlib)
library(ggplot2)
library(rgl)
library(car)

#====================================
#=============czesc I================
#====================================

#zadanie 1

#wczytujemy zbior danych
dane <- read.csv("C:\\Users\\Szymon Tokarski\\Desktop\\danePCA.csv", sep = ';')

head(dane)

#wymiar wynosi 569 32 czyli wszystkie dane wczytaly sie poprawnie
dim(dane)

#id jest zmienna calkowita, diagnosis to ciag znakow. Pozostale zmienne sa numeryczne
str(dane)

#tworzymy macierz danych z kolumn od 3 do 32
X = matrix(unlist(dane[,3:32]), ncol = 30)

#ilosc wierszy w zbiorze
nrow = dim(X)[1]

#ilosc kolumn w zbiorze
ncol = dim(X)[2]

#zadanie 2

#tworzymy wektor o wymiarze 'nrow' skladajacy sie z samych 1. Jest on wymagany w dalszych obliczeniach
one = matrix(rep(1,nrow), nrow, 1)

#wyznaczyamy wektor srednich
X_sr = 1/nrow * t(X) %*% one

#porownujemy otrzymany wektor z funkcja wbudowana w R
all.equal(as.matrix(colMeans(X)), X_sr, check.attributes = FALSE, tolerance=0.0000001)

#wyznaczamy macierz kowariancji
S = 1 / (nrow - 1) * t(X) %*% (diag(1, nrow, nrow) - 1/nrow * one %*% t(one)) %*% X

#porownujemy otrzymana macierz z funkcja wbudowana w R
all.equal(as.matrix(cov(X)), S, check.attributes = FALSE, tolerance=0.0000001)

#rozpoczynamy standaryzacje danych
Y = X

#odejmujemy od kazdej kolumny odpowiadajacom jej srednia 
for(i in 1:30)
{
  #matrix(rep(X_sr[i], nrow), nrow, 1) sluzy by utworzyc wektor o dlugosci nrow ze sreniej z danej kolumny aby nie tworzyc
  #dodatkowej petli i odjac wszystko na raz.
  Y[,i] = Y[,i] - matrix(rep(X_sr[i], nrow), nrow, 1)
}

#obliczamy odchyleniea standardowe
D = diag(sqrt(diag(S)))

#dzielimy kolumne przez jej odchylenie standardowe czyli mnozymy przez odwrotna macierz D
Y = Y %*% inv(D)

#sprawdzamy poprawnosc wystandaryzowanych danych z funkcja wbudowana w R
all.equal(scale(X), Y, check.attributes = FALSE, tolerance=0.0000001)

#obliczamy srednia dla kazdej kolumny dla zmiennej Y, analogicznie jak dla X
Y_sr = 1/nrow * t(Y) %*% one

#porownujemy otrzymany wektor z funkcja wbudowana w R
all.equal(as.matrix(colMeans(Y)), Y_sr)

#wyznaczamy macierz kowariancji dla Y, analogicznie jak dla X
Sy = 1 / (nrow - 1) * t(Y) %*% (diag(1,nrow,nrow) - 1/nrow * one %*% t(one)) %*% Y

#porownujemy otrzymana macierz z funkcja wbudowana w R
all.equal(as.matrix(cov(Y)), Sy, check.attributes = FALSE, tolerance=0.0000001)

#zadanie 3

#wartosc wlasna
eval = eigen(Sy)$values

#wektor wlasny
evec = eigen(Sy)$vectors

#zadanie 4

#proporcja wariancji
pw = rep(0, 30)

#suma bezwladnosci
sb = tr(t(Y) %*% Y)

#skumulowana proporcja
sp = rep(0, 30)

for(i in 1 : 30)
{
  z = Y %*% evec[,i]
  pw[i] = t(z) %*% z
  sp[i] = sum(pw)
}

sp = sp/sb
pw = pw/sb

tabela = data.frame(wariancja=eval, proporcja_wariancji=pw, skumulowana_proporcja=sp)

#zadanie 5

Z =  Y %*% evec
z1 = Z[,1]
z2 = Z[,2]

#wyjasniona wariancja
var_explainded = eval / sum(eval)


#wykres wyjasnionej wariancji do skladowych glownych
qplot(c(1:30), var_explainded) + 
  geom_line() +
  geom_point(size=5, colour="white") + 
  geom_point(size=2) + 
  ylab("wyjasniana wariancja") + 
  xlab("skladowe glowne") +
  scale_x_continuous(breaks=seq(0,30,by = 5)) +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#wykres wariancji do skladowych glownych
qplot(c(1:30), eval) + 
  geom_line() +
  geom_point(size=5, colour="white") + 
  geom_point(size=2) +
  geom_hline(yintercept = 1, colour="red", linetype ="dashed") +
  ylab("wariancja") + 
  xlab("skladowe glowne") +
  scale_x_continuous(breaks=seq(0,30,by = 5)) +
  scale_y_continuous(breaks=seq(0,12,by = 2)) +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#zadanie 6

#obliczamy odchyleniea standardowe
Xtemp = as.matrix(data.frame(z1,z2,Y))
Stemp = 1 / (nrow - 1) * t(Xtemp) %*% (diag(1,nrow,nrow) - 1/nrow * one %*% t(one)) %*% Xtemp
InvDtemp = inv(diag(sqrt(diag(Stemp))))
Rtemp = InvDtemp %*% Stemp %*% InvDtemp

Rtemp = Rtemp[3:32, 1:2]

#korelacja Yi, z1
rz1  = Rtemp[,1]

#korelacja Y1, z2
rz2  = Rtemp[,2]

#porownujemy otrzymana macierz z funkcja wbudowana w R
all.equal(as.matrix(cor(Y, z1)), as.matrix(rz1), check.attributes = FALSE, tolerance=0.0000001)
all.equal(as.matrix(cor(Y, z2)), as.matrix(rz2), check.attributes = FALSE, tolerance=0.0000001)

#funkcja pomocnicza do narysowania okregu
circleFun <- function(center = c(0,0),diameter = 1, npoints = 100){
  r = diameter / 2
  tt <- seq(0,2*pi,length.out = npoints)
  xx <- center[1] + r * cos(tt)
  yy <- center[2] + r * sin(tt)
  return(data.frame(x = xx, y = yy))
}

#punkty okregu na wykresie
circle <- circleFun(diameter = 2)

#wykres proporcje w przestrzeni pierwszych dw�ch sk�adowych g��wnych 
ggplot(data = data.frame(rz1,rz2), aes(x = rz1, y = rz2)) + 
  geom_point() + 
  geom_text(label = seq(1,30), vjust = 0, nudge_y = 0.02) +
  geom_path(data = circle, mapping = aes(x=x,y=y)) +
  ylab("PC2") + 
  xlab("PC1") +
  scale_x_continuous(breaks=seq(-1,1,by = 0.5)) +
  scale_y_continuous(breaks=seq(-1,1,by = 0.5)) +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#zadanie 7

#wybieramy wartosci gdzie, wariancja jest wieksza od 1
WiekszaOd1 = Z[,eval > 1]

qplot(c(1:30), tabela[,3]) + 
  geom_hline(yintercept = tabela[dim(WiekszaOd1)[2],3], colour="red", linetype ="dashed") +
  geom_vline(xintercept = dim(WiekszaOd1)[2], colour="red", linetype ="dashed") +
  geom_line() +
  geom_point(size=5, colour="white") + 
  geom_point(size=2) +
  ylab("wyjasniana wariancja") + 
  xlab("skladowe glowne") +
  scale_x_continuous(breaks=seq(0,30,by = 5)) +
  scale_y_continuous(breaks=seq(0.5, 1,by = 0.1)) +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#zadanie 8

#tworzymy zmienna pomocnicza
z3 = Z[,3]

#rysujemy wykresy dla r�nych projkcji punkt�w PC1, Pc2, PC3
stripchart(z1, pch = 16, main = "z1")
stripchart(z2, pch = 16, main = "z2")
stripchart(z3, pch = 16, main = "z3")

plot(z1,z2, pch = 16)
plot(z1,z3, pch = 16)
plot(z2,z3, pch = 16)

plot3d(z1,z2,z3,pch = 16)

#zadanie 9

#dzielimy dane na 2 zbiory. Jeden zbior zawiera tylko diagnoze raka zlosliwego a drugi lagodnego
zlosliwa = data.frame(PC1 = z1[dane[,2] == "M"], PC2 = z2[dane[,2] == "M"])
lagodna = data.frame(PC1 = z1[dane[,2] == "B"], PC2 = z2[dane[,2] == "B"])

#obliczamy srednia dla 2 zbiorow
Z_sr = 1/dim(zlosliwa)[1] * t(zlosliwa) %*% matrix(rep(1, dim(zlosliwa)[1]),dim(zlosliwa)[1],1)
l_sr = 1/dim(lagodna)[1] * t(lagodna) %*% matrix(rep(1, dim(lagodna)[1]),dim(lagodna)[1],1)

#wyznaczamy punkty srodkow zbiorow
srodek = data.frame(x = c(Z_sr[1], l_sr[1]), y = c(Z_sr[2], l_sr[2]))

#wyznaczamy punkty elisy
ellipseZ = data.frame(ellipse(colMeans(zlosliwa), cov(zlosliwa), radius = sqrt(qchisq(0.95,2)), draw = FALSE))
ellipseL = data.frame(ellipse(colMeans(lagodna), cov(lagodna), radius = sqrt(qchisq(0.95,2)), draw = FALSE))

#rysujemy wykres
qplot() +
  geom_polygon(data = ellipseZ, mapping = aes(x=x,y=y), color = "red", fill = "rosybrown2", size = 1.02, alpha = 0.3) +
  geom_polygon(data = ellipseL, mapping = aes(x=x,y=y), color = "blue", fill = "lightblue2", size = 1.02, alpha = 0.3) +
  geom_hline(yintercept = 0, linetype ="dashed") +
  geom_vline(xintercept = 0, linetype ="dashed") +
  geom_point(data = zlosliwa, mapping = aes(PC1, PC2, color = "red")) +
  geom_point(data = lagodna, mapping = aes(PC1, PC2, color = "blue")) +
  geom_point(data = srodek, mapping = aes(x=x,y=y), color = "black", shape = 15) +
  ylab("PC2") + 
  xlab("PC1") +
  scale_color_identity(guide = "legend", labels = c("�agodny", "z�o�liwy"))+
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_blank()
  )


#====================================
#=============czesc II===============
#====================================

library(factoextra)

#zadanie 10.1

#wczytujemy dane
dane <- read.csv("C:\\Users\\Szymon Tokarski\\Desktop\\danePCA.csv", sep = ';')
head(dane)

X = matrix(unlist(dane[,3:32]), ncol = 30)

#zadanie 10.2

#Obliczamy PCA
Y = prcomp(X, scale = TRUE, center = TRUE)

#zadanie 10.3

#obliczamy wariancje
eval = Y$sdev^2

#wektory wlasne
evec = Y$rotation

#zadanie 10.4

#przeprowadzamy analize dekompozycji
tabela = get_eigenvalue(Y)

#zadanie 10.5

#rysujemy wykresy analogiczne do zadania 5 w czesci I
fviz_eig(Y, choice = c("variance"),geom = c("line"), ncp = 30 )+
  ylab("wyjasniana wariancja") + 
  xlab("skladowe glowne") +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

fviz_eig(Y, choice = c("eigenvalue"),geom = c("line"), ncp = 30 )+
ylab("wariancja") + 
  xlab("skladowe glowne") +
  geom_hline(yintercept = 1, colour="red", linetype ="dashed")+
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#zadanie 10.6

#rysujemy wykres analogiczny do zadania 6 w czesci I
fviz_pca_var(Y, repel = FALSE, col.var = "black",geom = c("text", "point"), labelsize = 4 )+
  ylab("PC2") + 
  xlab("PC1") +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#zadanie 10.7

#tworzymy sobie zmienne pomocnicze
Z = get_pca_ind(Y)$coord
z1 =  Z[,1]
z2 =  Z[,2]
z3 =  Z[,3]

#wybieramy wartosci gdzie, wariancja jest wieksza od 1
WiekszaOd1 = Z[,eval > 1] * 100

#rysujemy wykres analogiczny do zadania 7 w czesci I
qplot(c(1:30), tabela[,3] * 100) + 
  geom_hline(yintercept = tabela[dim(WiekszaOd1)[2],3] * 100, colour="red", linetype ="dashed") +
  geom_vline(xintercept = dim(WiekszaOd1)[2], colour="red", linetype ="dashed") +
  geom_line() +
  geom_point(size=5, colour="white") + 
  geom_point(size=2) +
  ylab("wyjasniana wariancja") + 
  xlab("skladowe glowne") +
  scale_x_continuous(breaks=seq(0,30,by = 1)) +
  scale_y_continuous(breaks=seq(50, 100,by = 10)) +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))


#zadanie 10.8

#rysujemy wykresy analogiczne do zadania 8 w czesci I
stripchart(z1, pch = 16, main = "z1")
stripchart(z2, pch = 16, main = "z2")
stripchart(z3, pch = 16, main = "z3")

plot(z1,z2, pch = 16)
plot(z1,z3, pch = 16)
plot(z2,z3, pch = 16)

plot3d(z1,z2,z3,pch = 16)

#zadanie 10.9

#wyznaczamy punkty srodkow zbiorow
Z_sr = colMeans(Z[dane$diagnosis=="M",1:2])
L_sr =  colMeans(Z[dane$diagnosis=="B",1:2])
srodek = data.frame(x = c(Z_sr[1], L_sr[1]), y =c(Z_sr[2], L_sr[2]))

#rysujemy wyykres analogiczny do zadania 9 w czesci I
fviz(Y, element="ind", geom="point",  pointsize = 1,pointshape = 19,
     habillage=dane$diagnosis, addEllipses=TRUE, ellipse.level=0.95, col.var = "black")+
      geom_point(data = srodek, mapping = aes(x=x,y=y), color = "black", shape = 15)+
      ylab("PC2") + 
      xlab("PC1") +
      scale_fill_manual(labels=c("�agodny", "z�o�liwy"), values=c("blue", "red")) +
      scale_color_manual(labels =c("�agodny", "z�o�liwy"), values=c("blue", "red"))+
     theme_classic() +
     theme(panel.background = element_rect(color = "black"),
        legend.position = c(.3, .3),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_blank()
     )
