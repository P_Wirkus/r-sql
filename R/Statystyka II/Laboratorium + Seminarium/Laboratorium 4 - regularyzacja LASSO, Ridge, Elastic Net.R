# Wczytujemy zbi�r danych:
gene_data <- read.csv("C:/Users/Patryk/Desktop/gene expression.csv")
# I zmienna m�wi o wyst�powaniu choroby, pozosta�e 5000 zwi�zane s� z genami
# Dane s� znormalizowane. bez brak�w danych, s� gotowe do u�ycia

# Naszym zadaniem b�dzie sprawdzenie zale�no�ci (zdudowanie modelu statystycznego) pomi�dzy
# informacj� genetyczn�, a indeksem chorobowym

# Poniewaz zmienna 'diseaseindicator' przyjmuje warto�ci z przestrzeni liczb rzeczywistych
# mogliby�my u�y� znanego nam ju� modelu regresji liniowej.

# Uwaga, dzielimy na zb. testowy - by testowa� model i treningowy - by go optymalizowa�

set.seed(1234)
train_rows <- sample(1:nrow(gene_data), 0.66*nrow(gene_data))
gene_data_train <- gene_data[train_rows, ]
gene_data_test <- gene_data[-train_rows, ]

gene_data_lm <- lm(
  formula = disease_indicator ~ ., data = gene_data_train
)

print(paste("R^2 dla modelu:", summary(gene_data_lm)$r.squared))
print(paste("Suma kwardrat�w reszt dla zb. treningowego:", sum(gene_data_lm$residuals^2)))
print(paste("Suma kwardrat�w reszt dla zb. testowego:", sum((gene_data_test$disease_indicator - predict(gene_data_lm, gene_data_test))^2)))

# Wnioski: wyniki s� bardzo dziwne, dalczego ?
# I pomys� - underfitting: nasz model NIE mo�e dopasowa� si� do �adnych danych
#            ale wtedy powinni�my widze� du�y b��d na obu zbiorach, tu to NIE wyst�puje,
#            wi�c nie jest tak, �e u�yli�my zbyt prostego modelu
# II pomys� - overfitting: model zbyt zaawansowany, model �wietnie dopasowany do zb. tren.,
#             a na zb. test. wypada fatalnie, tu to jest w�a�nie 

# Z�o�one wnioski:
# Od razu widzimy, �e co� jest nie tak. Na zbiorze treningowym osiaga idealny - zerowy - 
# b��d, na zbiorze testowym jednak wypada on fatalnie. Jest to typowy przypkad overfittingu, 
# czyli zbyt wysokiej wariancji modelu. Overfitting spowodowany jest uzyciem zbyt 
# elastycznego modelu, jednak�e zazwyczaj rozumiemy to poprzez u�ycie zbyt elastycznej 
#architektury jak np. sieci nauronowego do prostego problemu. Model regresji liniowej 
#jest bardzo prostym modelem wi�c nie powinno byc z tym problemu.

# Overfitting mo�e jednak wyst�powa� tak�e dla prostych modeli w przypadku gdy posiadamy
# wi�cej predyktor�w ni� obserwacji: p???N. We wspomianym przypadku estymatory oparte na 
# metodzie najwi�kszej wiarygodno�ci jak i na metodzie najmniejszych kwadrat�w nara�one 
# sa na przeuczenie modelu.

# Zwizualizujmy co si� dzieje na prostym przyk�adzie zak�adaj�c, �e mamy w naszym zbiorze 
# treningowym tylko 2 obserwacje i u�ywamy jednego predyktora (dodaj�c wyraz wolny mamy 
# p=N). W takim przypadku �atwo zauwa�y�, �e model regresji liniowej da nam po prostu 
# wz�r na lini� przechodz�c� przez obie obserwacje:
library(dplyr)
set.seed(2222)
sample_data <- tibble(
  x = 1:7,
  y = 2*x + 1 + rnorm(7),
  set = c("train", "test", "train", rep("test", 4))
)
test_model <- lm(data = sample_data %>% filter(set == "train") %>% select(-set), formula = y ~ .)
test_model_intercept <- test_model$coefficients["(Intercept)"]
test_model_slope <- test_model$coefficients["x"]
library(ggplot2)
ggplot(sample_data, aes(x, y, color = set)) + theme_bw() + geom_point() +
  geom_abline(intercept = test_model_intercept, slope = test_model_slope)


# Aby rozwiaza� ten problem musimy zastanowi� si� jak zmnieszy� wariancj� naszego modelu. 
# Wariancja i obci��enie (bias) s� ze sob� �cisle powi�zane. Zmniejszaj�� jedno 
# zwi�kszamy drugie - bias-variance tradeoff, tak wi�c mogliby�my sprawdzi� co stanie si� 
# gdy do naszego modelu dodamy obcia�anie.
# S�u�y do tego REGULARYZACJA, 1 z jej rodzaj�w to 'regresja grzbietowa'

# Regresja grzebietowa:
# dokonamy tego dodaj�c regularyzacji naszego modelu, karaj�c model za zbyt wysokie 
# wsp�czynniki �. M�wi�c dok�adniej w regularyzowanej wersji naszego modelu 
# minimalizowa� b�dziemy nie loglikelihood (lub b��d kwadratowy), a poni�sze wyra�enie:
# beta = argmin_beta(-l(beta)+lambda*sum_i=2^pbeta_p^2)

# Zwizualizujmy rozwi�zanie regresji grzbietowej dla kilku warto�ci ??:
X <- cbind(1, sample_data %>% filter(set == "train") %>% dplyr::select(x)) %>% as.matrix()
# cbind- tworzy tablic� z podanych wektor�w
y <- sample_data %>% filter(set == "train") %>% dplyr::select(y) %>% as.matrix()
ridge_intercept <- sample_data %>% filter(set == "train") %>% pull(y) %>% mean()
solution_lambda_1 <- solve(t(X)%*%X + 1*diag(2))%*%t(X)%*%y
solution_lambda_5 <- solve(t(X)%*%X + 5*diag(2))%*%t(X)%*%y
solution_lambda_10 <- solve(t(X)%*%X + 10*diag(2))%*%t(X)%*%y
ggplot(sample_data, aes(x, y, color = set)) + theme_bw() + geom_point() +
  geom_abline(intercept = test_model_intercept, slope = test_model_slope, color = "blue") +
  geom_text(aes(x = 1.5, y = 14, label = "lambda=0"), color = "blue") +
  geom_abline(intercept = solution_lambda_1[1,1], slope = solution_lambda_1[2,1], color = "green") +
  geom_text(aes(x = 1.5, y = 13, label = "lambda=1"), color = "green") +
  geom_abline(intercept = solution_lambda_5[1,1], slope = solution_lambda_5[2,1], color = "red") +
  geom_text(aes(x = 1.5, y = 12, label = "lambda=5"), color = "red") +
  geom_abline(intercept = solution_lambda_10[1,1], slope = solution_lambda_10[2,1], color = "black") +
  geom_text(aes(x = 1.5, y = 11, label = "lambda=10"), color = "black")

# Wniosek: zwi�kszaj�c lambd� zmniejszaj� si� wsp�czynniki B_i przy X_i, w tym 
#          przypadku jest tylko beta_1, wtedy jak B_i maleje, to ro�nie bias, wi�c
#          overfitting maleje

# Oczywi�cie pojawia si� pytanie, jak wybra� wielko�� lambda ? Parametr ten wybierany 
# jest w procesie cross-walidacji (CV). CV jest prost� lecz skuteczn� metod� estymacji 
# b��du generalizacji dlatego te� doskonale nadaje si� do estymacji warto�ci wszelkich 
# hiperparametr�w.

# W procesie CV dzielimy nasz zbi�r treningowy na foldy r�wnej wielko�ci. Nastepnie 
# budujemy K modeli za nowy zbi�r treningowy bior�c K-1 fold�w - w kazdym z modeli 
# pozostaj�cy fold jest traktowany jako zbi�r walidacyjny. Nastepnie u�redniamy wyniki
# (np. b�ad sredniokwadratowy) ze zbior�w walidacyjnych otrzymuj�c estymacj� zbioru 
# generalizacyjnego.
install.packages("glmnet")
library(glmnet)
gene_data_train_x <- gene_data_train %>% select(-disease_indicator) %>% as.matrix()
gene_data_train_y <- gene_data_train %>% pull(disease_indicator)
gene_data_test_x <- gene_data_test %>% select(-disease_indicator) %>% as.matrix()
gene_data_test_y <- gene_data_test %>% pull(disease_indicator)

gene_data_ridge <- cv.glmnet(gene_data_train_x, gene_data_train_y, type.measure = "mse", 
                             alpha = 0, family = "gaussian") #alpha=0 -> regresja grzbietowa

print(paste("Warto�� lambda.1se - warto�� kary, kt�ra daje nam najprostszy model (najmniej niezerowych parametr�w) i jest w granicy 1 b��du standardowego lambdy z najmniejsz� sum� kwadrat�w (lambda.min) wynosi:", gene_data_ridge$lambda.1se))
print(paste("Warto�� lambda.min - warto�� kary daj�ca najmniejsz� suw� kwadrat�w:", gene_data_ridge$lambda.min))
# lambda.min - daje najmniejsz� sum� kwadrat�w wszystkich testowanych lambd
plot(gene_data_ridge)
plot(gene_data_ridge$glmnet.fit)

# Mo�emy teraz policzy� predykcje:
gene_data_ridge_predicted_train <- 
  predict(gene_data_ridge, s = gene_data_ridge$lambda.1se, newx = gene_data_train_x)
gene_data_ridge_predicted_test <- 
  predict(gene_data_ridge, s = gene_data_ridge$lambda.1se, newx = gene_data_test_x)
print(paste("Suma kwardrat�w reszt dla zb. treningowego:", sum((gene_data_train_y - gene_data_ridge_predicted_train)^2)))
print(paste("Suma kwardrat�w reszt dla zb. testowego:", sum((gene_data_test_y - gene_data_ridge_predicted_test)^2)))

# Wnioski:
# na zb. treningowy mamy ju� b��d i to NIEzerowy
# a na zb. testowym b��d si� zmniejszy�, wi�c jest lepiej

# Sprawd�my predykcj� jeszcze dla lambda.min:
gene_data_ridge_predicted_train <- 
  predict(gene_data_ridge, s = gene_data_ridge$lambda.min, newx = gene_data_train_x)
gene_data_ridge_predicted_test <- 
  predict(gene_data_ridge, s = gene_data_ridge$lambda.min, newx = gene_data_test_x)
print(paste("Suma kwardrat�w reszt dla zb. treningowego:", sum((gene_data_train_y - gene_data_ridge_predicted_train)^2)))
print(paste("Suma kwardrat�w reszt dla zb. testowego:", sum((gene_data_test_y - gene_data_ridge_predicted_test)^2)))
# Wnioski: du�e r�nice mi�dzy tet. a tren., czyli zn�w overfitting

# Regresja grzbietowa jest przyk�adem regularyzacji norm� L^q gdzie g=2::
library(purrr)
gen_norm <- function(x, q) {(1 - abs(x^q))^(1/q)}
q_norms <- c(0.3, 0.5, 1, 2, 3, 5, 20) %>% map_df(~ {
  q <- .x
  points <- tibble(
    x = seq(0, 1, by = 0.001),
    y = gen_norm(x, q),
    q = q
  )
  bind_rows(points, points %>% mutate(y = -y),
            points %>% mutate(x = -x),
            points %>% mutate(x = -x, y = -y))
}) %>% mutate(q = as.factor(q)) %>%
  arrange(q, y, x)
ggplot(q_norms, aes(x, y, color = q, group = q)) + theme_bw() + geom_point()

# Regularyzacja LASSO:
# robimy anal., tylko alpha=1, a nie 0
gene_data_lasso <- cv.glmnet(gene_data_train_x, gene_data_train_y, type.measure = "mse", 
                             alpha = 1, family = "gaussian")
print(paste("Warto�� lambda.1se - warto�� kary, kt�ra daje nam najprostszy model (najmniej niezerowych parametr�w) i jest w granicy 1 b��du standardowego lambdy z najmniejsz� sum� kwadrat�w (lambda.min) wynosi:", gene_data_lasso$lambda.1se))
print(paste("Warto�� lambda.min - warto�� kary daj�ca najmniejsz� suw� kwadrat�w:", gene_data_lasso$lambda.min))
plot(gene_data_ridge)
plot(gene_data_lasso$glmnet.fit)

# R�nica jest taka, �e w LASSO otrzymujemy argumenty = 0

# Analogicznie policzmy predykcje i �redni b��d:
gene_data_lasso_predicted_train <- 
  predict(gene_data_lasso, s = gene_data_lasso$lambda.1se, newx = gene_data_train_x)
gene_data_lasso_predicted_test <- 
  predict(gene_data_lasso, s = gene_data_lasso$lambda.1se, newx = gene_data_test_x)
print(paste("Suma kwardrat�w reszt dla zb. treningowego:", sum((gene_data_train_y - gene_data_lasso_predicted_train)^2)))
print(paste("Suma kwardrat�w reszt dla zb. testowego:", sum((gene_data_test_y - gene_data_lasso_predicted_test)^2)))

# Wniosek: uda�o si� jeszcze bardziej zmniejszy� b��d, dzi�ki czemu mamy lepszy model.

# Jak widzimy dopasowanie jest jeszcze lepsze w przypadku LASSO ni� regresji grzbietowej. 
# Wida� takze pewn� r�nic� jesli chodzi o warto�ci wsp�czynnik�w �. W regresji 
# grzbietowej niekt�re warto�ci s� bliskie 0, ale nigdy nie osi�gaj� tej warto�ci - 
# metoda regresji grzbietowej pozwala tylko na doj�cie asymptotycznie blisko do 0. W 
# przypadku LASSO dostajemy zerowe wsp�czynniki - daje nam to bardzo przydatn� w�asno��, 
# bo LASSO dzia�a jak selektor zmiennych.

# Uwaga: Lasso lepsze ni� grzbietowa, gdy s� zmienne NIEistotne

# III typ - elastic Net:
# LASSO daje z regu�y lepsze wyniki gdy w naszym zbiorze mamy nieistotne predyktory 
# (zmienne kt�re nie wnosz� informacji do modelu), LASSO potrafi si� ich pozby�. 
# Regresja grzbietowa dzia�a lepiej gdy predyktory s� istotne. Je�li chcemy korzysta� z
# zalet obu podej�� mo�emy u�y� regresji Elastic Net:

gene_data_elastic_net <- cv.glmnet(gene_data_train_x, gene_data_train_y, type.measure = "mse", 
                                   alpha = 0.5, family = "gaussian")
print(paste("Warto�� lambda.1se - warto�� kary, kt�ra daje nam najprostszy model (najmniej niezerowych parametr�w) i jest w granicy 1 b��du standardowego lambdy z najmniejsz� sum� kwadrat�w (lambda.min) wynosi:", gene_data_elastic_net$lambda.1se))
print(paste("Warto�� lambda.min - warto�� kary daj�ca najmniejsz� suw� kwadrat�w:", gene_data_elastic_net$lambda.min))
plot(gene_data_ridge)

# Wnioski: wyniki troch� gorsze ni� w  LAsso, mo�e warto by sprawdzi� alpha z (0.5, 1)

# Predykcja:
gene_data_elastic_net_predicted_train <- 
  predict(gene_data_elastic_net, s = gene_data_elastic_net$lambda.1se, newx = gene_data_train_x)
gene_data_elastic_net_predicted_test <- 
  predict(gene_data_elastic_net, s = gene_data_elastic_net$lambda.1se, newx = gene_data_test_x)
print(paste("Suma kwardrat�w reszt dla zb. treningowego:", sum((gene_data_train_y - gene_data_elastic_net_predicted_train)^2)))
print(paste("Suma kwardrat�w reszt dla zb. testowego:", sum((gene_data_test_y - gene_data_elastic_net_predicted_test)^2)))
# Uwaga: print(paste("Suma kwardrat�w reszt dla zb. testowego:", 
# sum((gene_data_test_y - gene_data_elastic_net_predicted_test)^2)))


# H2O:
library(tidyverse)
install.packages("h2o")
library(h2o)
# Zacznijmy od wczytania zbioru danych bankowych. Naszym zadaniem b�dzie klasyfikacja 
# tranzakcji i odnalezienie oszustw:
load("C:/Users/Patryk/Desktop/creditcard.RData")
creditcard_train_Y <- factor(creditcard_train_Y)
table(creditcard_train_Y)
creditcard_test_Y <- factor(creditcard_test_Y)
table(creditcard_test_Y)
# zbi�r jest b. NIEzbalansowany
# Zbi�r ten zawiera 29 predyktor�w (28 zakodowanych zmiennych bankowych przy pomocy PCA
# + znormalizowan� wielko�� tranzakcji). Jest on ekstremalnie niezbalansowany.

# Nie ma brak�W danych:
# ani treningowy
colSums(is.na(creditcard_train_X))
# ani testowy
colSums(is.na(creditcard_test_X))

# Zacznijmy od wizualizacji
creditcard_temp <- creditcard_train_X %>% 
  as.tibble() %>%
  bind_rows(as.tibble(creditcard_test_X)) %>%
  mutate(fraud = factor(c(creditcard_train_Y, creditcard_test_Y)))
ggplot(creditcard_temp, aes(x = V1, y = V2, color = fraud)) + geom_point() + theme_bw()
ggplot(creditcard_temp, aes(x = V1, y = V28, color = fraud)) + geom_point() + theme_bw()
ggplot(creditcard_temp, aes(x = V1, y = Amount, color = fraud)) + geom_point() + theme_bw()

# Poniewa� zmienne s� zakodowane i jest ich sporo mo�e by� trudno wyci�gn�� jakies 
# informacje z wykres�w. Tak jak poprzednio zacznijmy od modelu referencyjnego - 
# regresji logistycznej:
set.seed(1234)
creditcard_reg_log <- glm(formula = creditcard_train_Y ~ creditcard_train_X, family = binomial(link = "logit"))
summary(creditcard_reg_log)

# Jak wida� otrzymali�my duz� ilo�� niesistotnych mziennych. Naszym kolejnym krokiem 
# powinno by� odrzucenie nieistotnych zmiennych (pojedynczo!), sprawdzenie 
# wsp�liniowo�ci miedzy zmiennymi, do czasu a� dojdziemy do modelu, kt�ry jest 
# poprawnie skonstruowany i wida� w nim objaw�w overfittingu. Zamiast robi� to wszystko
# r�cznie i krok po kroku, spr�bujmy czego� innego.


# Regularyzacja LASSO, Ridge i Elastic Net w H2O
creditcard_train <- creditcard_train_X %>%
  bind_cols(fraud = creditcard_train_Y)
creditcard_test <- creditcard_test_X %>%
  bind_cols(fraud = creditcard_test_Y)
# Tworzymy po��czenie z H2O
localH2O <- h2o.init(ip = "localhost",
                     port = 54321,
                     nthreads = -1,
                     min_mem_size = "4g")

# Przenosimy dane do H2O
creditcard_train_h2o <- as.h2o(creditcard_train, destination_frame = "creditcard_train")
creditcard_test_h2o <- as.h2o(creditcard_test, destination_frame = "creditcard_test")

# To nam m�wi, co teraz znajduje si� na klastrze Jaovovym:
h2o.ls()

# Nast�pnie zbudujemy 3 modele:

# LASSO - r.logistyczna, st�d 'family = "binomial"'
card_lasso_balanced <- h2o.glm(x = 1:28, # Nazwy lub indeksy
                               y = "fraud", # Nazwa lub indeks
                               training_frame = "creditcard_train",
                               family = "binomial",
                               alpha = 1,
                               lambda_search = TRUE,
                               model_id = "card_lasso_balanced",
                               nfolds = 5,
                               balance_classes = TRUE, # Over/under sampling
                               class_sampling_factors = c(0.5, 0.5),
                               # chcemy mie� w stosunku 1 do 1, st�d 0.5 i 0.5
                               seed = 1234,
                               score_each_iteration = TRUE)

# Ridge
card_ridge_balanced <- h2o.glm(x = 1:28, # Nazwy lub indeksy
                               y = "fraud", # Nazwa lub indeks
                               training_frame = "creditcard_train",
                               family = "binomial",
                               alpha = 0,
                               lambda_search = TRUE,
                               model_id = "card_ridge_balanced",
                               nfolds = 5,
                               balance_classes = TRUE, # Over/under sampling
                               class_sampling_factors = c(0.5, 0.5),
                               seed = 1234,
                               score_each_iteration = TRUE)

# Elastic Net
card_elastic_net_balanced <- h2o.glm(x = 1:28, # Nazwy lub indeksy
                                     y = "fraud", # Nazwa lub indeks
                                     training_frame = "creditcard_train",
                                     family = "binomial",
                                     alpha = 0.5,
                                     lambda_search = TRUE,
                                     model_id = "card_elastic_net_balanced",
                                     nfolds = 5,
                                     balance_classes = TRUE, # Over/under sampling
                                     class_sampling_factors = c(0.5, 0.5),
                                     seed = 1234,
                                     score_each_iteration = TRUE)

# Sprawd�my teraz jak wygl�daj� wsp�czynniki naszych modeli:
h2o.coef(card_lasso_balanced)
h2o.coef(card_ridge_balanced)
h2o.coef(card_elastic_net_balanced)

# Oraz miary dopasowania:
pred_lasso_balanced <- h2o.predict(card_lasso_balanced, creditcard_test_h2o)
perf_lasso_balanced <- h2o.performance(card_lasso_balanced, creditcard_test_h2o)

# Liczymy wsp�czynniki:
h2o.auc(perf_lasso_balanced)
h2o.giniCoef(perf_lasso_balanced)
h2o.aic(card_lasso_balanced)

# Liczymy confusionMatrix dla konkretnej metryki:
cm_lasso_balanced <- h2o.confusionMatrix(card_lasso_balanced,
                                         newdata = creditcard_test_h2o,
                                         metrics = "f2")

fpr <- h2o.fpr(perf_lasso_balanced)[['fpr']]
tpr <- h2o.tpr(perf_lasso_balanced)[['tpr']]
ggplot(data.frame(fpr = fpr, tpr = tpr), aes(fpr, tpr)) +
  geom_line() + theme_bw()

card_lasso_balanced@model$lambda_best

pred_ridge_balanced <- h2o.predict(card_ridge_balanced, creditcard_test_h2o)

perf_ridge_balanced <- h2o.performance(card_ridge_balanced, creditcard_test_h2o)

h2o.auc(perf_ridge_balanced)

h2o.giniCoef(perf_ridge_balanced)

h2o.aic(card_ridge_balanced)

cm_ridge_balanced <- h2o.confusionMatrix(card_ridge_balanced,
                                         newdata = creditcard_test_h2o,
                                         metrics = "f2")

fpr <- h2o.fpr(perf_ridge_balanced)[['fpr']]
tpr <- h2o.tpr(perf_ridge_balanced)[['tpr']]
ggplot(data.frame(fpr = fpr, tpr = tpr), aes(fpr, tpr)) +
  geom_line() + theme_bw()

card_ridge_balanced@model$lambda_best

pred_elastic_net_balanced <- h2o.predict(card_elastic_net_balanced, creditcard_test_h2o)

perf_elastic_net_balanced <- h2o.performance(card_elastic_net_balanced, creditcard_test_h2o)

h2o.auc(perf_elastic_net_balanced)

h2o.giniCoef(perf_elastic_net_balanced)

h2o.aic(card_elastic_net_balanced)

cm_elastic_net_balanced <- h2o.confusionMatrix(card_elastic_net_balanced,
                                               newdata = creditcard_test_h2o,
                                               metrics = "f2")

fpr <- h2o.fpr(perf_elastic_net_balanced)[['fpr']]
tpr <- h2o.tpr(perf_elastic_net_balanced)[['tpr']]
ggplot(data.frame(fpr = fpr, tpr = tpr), aes(fpr, tpr)) +
  geom_line() + theme_bw()

card_elastic_net_balanced@model$lambda_best

# Na zako�czenie zapiszmy modele:
h2o.saveModel(card_lasso_balanced, path = getwd())
h2o.saveModel(card_ridge_balanced, path = getwd())
h2o.saveModel(card_elastic_net_balanced, path = getwd())

# I zakmnijmy cluster:
h2o.shutdown(prompt = FALSE)