##### Czyszczenie konsoli, srodowiska, wykres�w #####
cat("\014") 
rm(list=ls())
dev.off()

library(datasets)
data(iris)
dane <-  iris

summary(dane)
View(iris)

##### Podstawy #####
#install.packages('ggplot2')
library(ggplot2)

## ggplot2 cheat sheet - arkusz ze wszystkimi najwa�niejszymi funkcjonalnoociami paczki
## https://rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf 
## dokumentacja paczki
## https://cran.r-project.org/web/packages/ggplot2/ggplot2.pdf 

# funkcja ggplot() tworzy uklad wsp�lrzednych , kt�ry nastepnie mozemy modyfikowac (dodawac warstwy)
# funkcja aes() pozwala zmapowac, kt�re dane i jak maja zostac zwizualizowane
# ggplot(data = NULL, mapping = aes())
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length))
ggplot(dane, aes(x = Petal.Length/2, y = log(Sepal.Length)))

# funkcja geom_point() pozwala utworzyc wykres punktowy
# do podstawy utworzonej przez ggplot() dodajemy nowe warstwy za pomoca znaku "+" 
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point()
# alternatywna skladnia to mapowanie danych w kazdej warstwie osobno
ggplot(dane) +
  geom_point(aes(x = Petal.Length, y = Petal.Width))


##### Punkciki #####
# dodanie opcji - kolor
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point(color = "darkgreen")

# ksztalt
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point(shape = "diamond")

# wielkosc
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point(size = 2)

#razem
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point(size = 2,shape = "diamond",color = "darkgreen")

# lista opcji graficznych dostepnych w paczce
vignette("ggplot2-specs")


##### Kwestie wizualne czesc II #####
# funkcja theme, pozwala na uzywanie motyw�w/ modyfikowanie tytulu, legendy, podpis�w

# przyklady wbudowanych motyw�w:
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point()+theme_bw()
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point()+theme_light()
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point()+theme_minimal()

# tytul i podpisy
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point() + 
  labs( x = "Podpis osi x", y = "Podpis osi y", title= 'Tytul wykresu',subtitle = 'Podtytul wykresu', caption = 'podpis wykresu') + 
  annotate(geom = "text", x = 5, y = 4, label = "adnotacja")

# funkcje element_ pozwalaja modyfikowac odpowiednie elementy wykresu 
# element_blank tworzy pusta przestrzen 
# element_rect modyfikuje ramki i tlo 
# element_lines odpowiada za linie 
# element_text modyfikuje tekst
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point() + 
  labs( x = "Podpis osi x", y = "Podpis osi y", title= 'Tytul wykresu',subtitle = 'Podtytul wykresu', caption = 'podpis wykresu') + 
  annotate(geom = "text", x = 3, y = 4, label = "adnotacja") + 
  theme(plot.title=element_text(hjust=0.5))

# legenda
# pozycja
ggplot(dane, aes(x = Petal.Length, fill = Species)) +
  geom_bar(alpha = 0.5) + theme(legend.position = "bottom")

# nazwy
ggplot(dane, aes(x = Petal.Length, fill = Species)) +
  geom_bar(alpha = 0.7) + scale_fill_discrete(name = "Species",
                                              labels = c("A", "B", "C"))
# osie
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point() + 
  xlim(0,max(dane$Petal.Length)+0.5) + # zakres osi x
  ylim(4,max(dane$Sepal.Length)+0.5) + # zakres osi y
  theme(axis.line.y = element_line(colour = "darkred"),  # kolor osi y
        axis.title.x = element_text(color = "Darkblue")) # kolor tytulu osi x

# tlo
ggplot(dane, aes(x = Petal.Length, y = Sepal.Length)) +
  geom_point() + theme(plot.background=element_rect(fill="darkblue"), # kolor tla wykresu
                       panel.background=element_rect(fill="lightblue"), # tla panelu (to co otacza wykres)
                       panel.grid=element_line(color='black'), # kolor linii siatki
                       axis.text = element_text(colour = "white", size=10),
                       axis.title = element_text(color = "white"))

##### Czesc III #####
# kolor
ggplot(dane, aes(x = Petal.Length, y = Petal.Width, color = Species)) +
  geom_point()

# ksztalt
ggplot(dane, aes(x = Petal.Length, y = Petal.Width, shape = Species)) +
  geom_point()

# wielkosc
ggplot(dane, aes(x = Petal.Length, y = Petal.Width, size = Species)) +
  geom_point() # warto czytac tresci ostrzezen XD

#____________________________________________________________________________
#dplyr reminder
#____________________________________________________________________________
library(dplyr)
sorted <- dane %>%
  group_by(Petal.Width) %>%
  mutate(cnt = n()) %>% 
  select(Species, Petal.Width, Petal.Length, cnt) %>% 
  distinct() %>% 
  ungroup() %>% 
  arrange(Petal.Width)

# wielkosc podejscie drugie
ggplot(sorted, aes(x = Petal.Length, y = Petal.Width, size = cnt)) +
  geom_point()

# alpha - przezroczystosc
ggplot(sorted, aes(x = Petal.Length, y = Petal.Width, alpha = cnt,color=Species)) +
  geom_point()

# funkcja geom_jitter dodaje element losowosci
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_jitter()

##### Podzial danych, linie trendu #####
# funkcja facets dzieli wykres na podwykresy wedlug jednej lub wiecej zmiennych
# funkcja facet_[nazwa ukladu]()

# wrap - dopasuj
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point() +
  facet_wrap(~ Species)

# grid - obok siebie
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point() +
  facet_grid(~ Species)

# linia terndu
# funkcja geom_smooth() tworzy linie trendu
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point() +
  geom_smooth() +
  facet_wrap(~ Species)

# linia trendu z kolorami
ggplot(dane, aes(x = Petal.Length, y = Petal.Width)) +
  geom_point(aes(color = Species)) +
  geom_smooth()
##### Wykres kolumnowy #####
# wybieramy dane do wykresu
W <- dane %>%  group_by(Sepal.Width) %>% 
  mutate(cnt = n()) %>% 
  filter(cnt > 3) %>% 
  select(Sepal.Width, cnt) %>% 
  distinct() %>% 
  ungroup()

Width <- dane %>% 
  inner_join(W)

# wykres kolumnowy pokazujacy ile bylo kwiat�w o danej szerokosci platk�w
ggplot(Width, aes(x = Sepal.Width)) +
  geom_bar()
# domyslnie liczona statystyka jest liczebnosc - stat = "count"

# proporcje
ggplot(Width, aes(x = Sepal.Width, y = ..prop.., group = 1)) +
  geom_bar()

# kolorowanie slupk�w
# obram�wki
ggplot(Width, aes(x = Sepal.Width, color = Species)) +
  geom_bar()
# wypelnienie
ggplot(Width, aes(x = Sepal.Width, fill = Species)) +
  geom_bar()

# utworzenie dodatkowej kolumny

Width <- Width %>% 
  mutate(XX = case_when(Sepal.Width <= 3.1 ~ "dobre",
                        TRUE ~ "zle"))
# dodanie dodatkowej zmiennej do wykresu za pomoca:
# color - koloru
ggplot(Width, aes(x = Species, color = XX)) +
  geom_bar()
# fill - wypelnienie
ggplot(Width, aes(x = Species, fill = XX)) +
  geom_bar()

# alpha - przezroczystosc
ggplot(Width, aes(x = Species, fill = XX)) +
  geom_bar(alpha = 0.5)

# pozycje slupk�w
# obok siebie
ggplot(Width, aes(x = Species, fill = XX)) +
  geom_bar(position = "dodge")

# narastajaco
ggplot(Width, aes(x = Species, fill = XX)) +
  geom_bar(position = "fill")

# odwr�cenie wykresy
ggplot(Width, aes(x = Species, fill = XX)) +
  geom_bar() +
  coord_flip()

##### inne przyklady #####
#geom_path - linia laczaca wspo�lrzedne
ggplot(dane[1:10,], aes(x = Sepal.Width, y = Sepal.Length)) + geom_path(lineend="butt", linejoin="round",linemitre=3)

#geom_abline - linia prosta
ggplot(dane[1:10,], aes(x = Sepal.Width, y = Sepal.Length)) + geom_abline(aes(intercept=0, slope=1))

#geom_density - rozklad pr�by
ggplot(Width, aes(cnt)) + geom_density(kernel = "gaussian")

#geom_text() - wartooci zamiast punkt�w
ggplot(mpg, aes(cty, hwy)) + geom_text(aes(label = cty),nudge_x = 1, nudge_y = 1, check_overlap = TRUE)

#geom_boxplot - wykres pudelkowy
ggplot(Width, aes(x = Species, y = Sepal.Length)) + geom_boxplot()

#geom_bin2d - heatmapa  - dzieli wykres na prostok�ty o podanym wymiarze i zlicza wyst�pienia w ka�dym z nich nadaj�c na tej podstawie odpowiedni kolor
ggplot(dane, aes(x = Sepal.Width, y = Sepal.Length)) + geom_bin2d(binwidth = c(0.25, 0.25))

#geom_text - etykiety danych
ggplot(dane[1:20,], aes(x = Sepal.Width, y = Sepal.Length))  + geom_point() + geom_text(nudge_y=-0.08,aes(label=round(Sepal.Length,2)))
