# ztrl+shift+end - zaznaczenie ca�ej linii
# ctrl+l - czyszczenie konsoli
# ctrl + enter - kompilacja zaznaczonej linii kodu
# File -> New Project - rozpocz�cie nowego  projektu 
# Tools -> Global Options... - g��wne opcje
# prawe g�rne okno - workspace, historia polece� i pliki w bie��cym folderze
# lewe dolne okno - konsola R
# prawe dolne okno - wykresy, informacje o pakietach i help

# UWAGA: zbiory danych b�d� ze �cie�k pulpitu, natomiast znajduj� si� one w osobnym folderze


# 3 - Pakiety R (37-42)

# pakiet = biblioteka przygotowanego kodu do realizacji jakiego� zadania
# pakiet survival - analiza prze�ycia
# ggplot2 - tworzenie wykres�w 
# sp - dane przestrzenne
# Pakiety wa�ne: ggplot2, tidyr, dplyr

# Instalacja pakietu "coefplot": install.packages("coefplot")
# �adowanie pakietu "coefplot": library(coefplot)


# 4 - Podstawy R (43-62)

# Nie trzeba deklarowa� typu zmiennej
# Operatory przypisania zmiennych: '<-' i '=' 
# i mo�na przypisa� ta sama warto�� od razu do obu zmiennych, np.
a <- b <- 7
# Usuwanie zmiennej (remove):
rm(a)
# Uwaga: wielko�� liter jest rozr�niana w nazwach zmiennych, np a != A

# G��wne typy danych: 
# numeric (liczbowy), character (ci�g znak�w), Date (data) i logical (TRUE/FALSE)

# Typ zmiennej sprawdzamy fuckj� 'class', np.
class(b)
# Sprawdzamy, czy b ma typ numeric, odp to TRUE lub FALSE
is.numeric(b)
# Gdy chcemy by zmienna by�a ca�kowita, to dodajemy 'L' po warto�ci, np. i <- 5L

# Dane znakowe:
x <- "data"         # Tu x = "data"
y <- factor("data") # Tu y = data
# Sprawdzanie d�ugo�ci danych characte/numeric:
nchar(x)
nchar("hello")
nchar(44)

# Daty:
# Date - przechowujedzie�, a POSIXct - dzie� i czas
date1 <- as.Date("2012-06-28")
as.numeric(date1) # liczba dni od 1.01.1970
date2 <- as.POSIXct("2012-06-28 17:42")

# Warto�ci logiczne:
# TRUE = 1 i FALSE = 0 w sensie logiki matematycznej oraz arytmetyki, np. TRUE + 2 = 3
k <- TRUE # anal. dzia�a 'class' i 'is.logical'
# operator por�wnania: '==', np. 2 == 4 da FALSE

# Wektory ; UWAGA: w 1 wektorze mog� by� zmienne jednego typu
# Tworzenie wektora - u�ywamy do tego funkcji c
# Przyk�ady wektor�w: 1. c(1,3,2,1,5)  2. c("R", "SAS", "Python")
x <- c(1,2,3,4) # mo�na wykonyw�c obliczenia na wektorach, np. x*4 lub sqrt(x) lub x^2
# Dwukropek ":" generuje sekwencj� kolejnych liczb w okre�lonej kolejno�ci, np.
10:1
-7:5
# Na wektorach r�wnej d�ugo�ci mo�na przeprowadza� operacje arytmetyczne
# length(x) - sprawdzenie d�ugo�ci wektora x
# Jak s� wektory r�nej d�ugo�ci, to kr�tszy wektir zostaje u�yty cyklicznie, czyli
# np. I wketor ma d�. 5, a II to e = (2,3), to przy dzia�aniach mi�dzy I a II e (2,3,2,3,2)
x <- 1:10
x <= 5 # wektor mo�na u�y� w por�wnaniach
# all - spawdza, czy wszystkie el. maj� warto�� TRUE 
# any - sprawdza, czy kt�rykolwiek el. ma warto�� TRUE
y <- -5:4
nchar(y) # wynik to 2 2 2 2 2 1 1 1 1 1, gdy� pierwsze 5 liczb jest ujemnych, wi�c ma "-"
x[1] # Tak dostajemy I el. z x
x[1:3]# Tak pierwsze 3, a tak 4 dowolne: x[c(1,3,7,8)]
# Tak tworz� wektor, w kt�rym jego elementom nadaje nowe nazwy, np.:
w <- 1:3
names(w) <- c("a", "b", "c")
w
# To jest wynik kompilacji w:
#a b c 
#1 2 3

# Wektory typu Factor: strony 56-57, na razie nie rozumiem

# Wywo�anie f-cji:
# mean(X) - �rednia arytmetyczna z x
?mean # Tak otwieramy help dookre�lonej rzeczy.
# Jak nie znamy pe�nej nazwy f-cji, to przy u�yciu f-cji 'apropos' mo�emy napisa�
# fragment nazwy kt�r� pami�tamy i poka�� si� nam wszystkie f-cje z tym fragmentem w nazwie
# np. 
apropos("mea")

# Brakuj�ce dane - s� 2 typy: NA i NULL
z <- c(1, 2, NA) # inny przyk�ad z 'NA': z <- c("ww", "ee", NA)
is.na(z) # I mamy info, �e na iii miejscu jest "TRUE" 
mean(z) # wyjdzie NA, bo 1 z el. z to NA
mean(z, na.rm=TRUE) # wynik to 1.5, bo jest z bez el. NA
# NULL - oznacza nieistnienie czego�, a NA - brak informacji, czyli np.
w <- c(1, NULL, 4) # da: 1 4

# Potoki: strony 61-62, na razie nie rozumiem


# 5 - Zaawansowane struktury danych 63-83

# Struktury/sposoby przechowywania danych (najcz�stsze):
# 1. data.frame (ramka danych, znane w arkuszach kalkulacyjnych) 2. matrrix (macierze) 
# 3. list 4. array

# data.frame (ramki danych)
# data.frame zawiera kolumny (zmienne) i obserwacje (wiersze)
# Ka�da kolumna jest zmienn� typu vector
# Przyk�ad data.frame - u�yjemy funkcji data.frame
x <- 10:1
y <- -4:5
q <- c("Hockey", "Football", "Baseball", "Curling", "Rugby", "Lacrosse", "Basketball",
       "Tennis", "Cricket", "Soccer")
theDF <- data.frame(x, y, q)
theDF # Teraz w konsoli poka�e mi si� moja ramka danych
# Domy�lnie nazwy kolumn = nazwy wektor�w. J�li chcemy to zmieni�, to:
theDF <-data.frame(First=x, Second=y, Sport=q)
theDF
# Atrybuty: 1.nrow(theDF) - liczba  wierszy, 2. ncol(theDF), 
# 3. dim(theDF) - naraz liczba W i K
dim(theDF)
# Sprawdzamy nazwe 3. kolumny: names(theDF)[3] ; bez '[3] by�yby nazwy wszystkich kolumn
rownames(theDF) # automatycznie b�dzie to: "1" "2" ... "10"
# Ustawiam nowe nazwy wierszy: rownames(theDF) <- c("one", "two", itp.)
# head - zwraca kilka pocz�tkowych wierszy
# tail - zwraca kilka ko�cowych wierszy
head(theDF) # head(theDF, n=7) - 7 pierwszych wierszy
# theDF$First - wy�wietlamy tylko kolumn� 'First'
# theDF[3, 2] - wyraz co jest w 3. W i 2. K
# theDF[3, 2:3]
theDF[3, 2:3] # dostaniemy 3. wiersz z�o�ony z kolum 2. i 3. z podspisanymi nazwami K
theDF[c(3, 5), 2] # tu wiersz z�o�ony z 2. K  i wierszy: 3, 5 bez nazw K, bo jest tylko 1
theDF[,3] # dostajemy ca�a 3. kolumn� # inny przyk�ad: theDF[,2:3]
# Uwaga: theDF[,2:3] == theDF[,c(2,3)] == theDF[,c("Second", "Sport")]
theDF[,3]
theDF[, "Sport", drop=FALSE] # tak mamy to co w "theDF[,3]", tylko w formie tabeli 1-kolumnowej
# theDF[, "Sport", drop=FALSE] == theDF[, 3, drop=FALSE]
# UWAGA: Strony 70-71 dotycz�ce 'Data.frame + faktor' na razie zostawiam

# Listy
# zmienna typu list mo�e zawiera� same dane numeryczne, znakowe mixy, dataframe itp
# s� tworzone przy u�yciu f-cji list, np.
list(1,2,3) # lista 3-el.
list(c(1,2,3)) # lista 1-el.
list(c(1,2,3), 3:7) # lista 2-el., inny przyk�ad: list(theDF, 1:10)
list(theDF, 1:10)
# Tworzenie pustej listy: u�ywamy do tego f-cji 'vector'
(emptyList <- vector(mode="list", length=3))
# Tak bierzemy drugi lement listy: list4[[2]]

# Macierze
A <- matrix(1:10, nrow=5) # macierz 5x2
A
B <- matrix(21:30, nrow=5)
B
# sprawdzenie czy elementy A i B s� r�wne:
A == B
A * B # mno�enie element�w macierzy A i B
t(b) # transpozycja B
A %*% t(B) # mno�enie macierzy A i B

# Tablice
# tablica 'array' - to wielowymiarowy wektor
theArray <- array (1:12, dim=c(2,3,2))
theArray
# Mamy taki wynik bo:
# Wektor 'dim' m�wi nam o tym, �e dzielimy wektor 1:12 na:
# 2 macierze (m�wi o tym iii el. 'dim'), a one maj� wymiar 2(i el. m�wi o tym) x 3(ii)
theArray[1,,]
# bierzemy:
# I wiersz (wiemy dzi�ki i el.) i 
# wszystkie kolumny (wiemy dzi�ki iii el.)
# z wszystkich macierzy (wiemy dzi�ki iii el.)
theArray[1,,1] # bierzemy: I W, wszystkie K i I macierz
theArray[,,1] # bierzemy : wszystkie W i K z I macierzy


# 6 - Wczytywanie danych do R 83 - 103

# Czytanie plik�w CSV
# robimy to f-cj� 'read.table' lub 'read.csv'
z <- "http://www.jaredlander.com/data/TomatoFirst.csv"
tomato <- read.table(file=z, header=TRUE, sep=",") 
# I argument 'read.table' to nazwa pliku w cudzys�owie
# II arg., czyli header m�wi, �e I wiersz plikuto nazwy kolumn, czyli nag��wki
# sep ',', bo  w Excelu zmienne oddzielone przecinkami
head(tomato) # za pomoc� f-cji 'head' mo�emy zobaczy� zawarto�� zbioru danych
# do du�ych tabel read.table s�abe, bo d�ugo si� wczytuj� dane
# Lepsze s� f-cje: read_delim z pakietu readr i fread z data.table
# Funckje z domy�lnymi argumentami sep/dec s� w tabeli 6.1 na str. 85
# read_delim_
# S�u�y do CSV, I arg. 'read_delim' to pe�ana nazwa pliku
# arg. 'col_names' jako TRUE oznacza, �e I wiersz pliku to nazwy kolumn
library(readr)
z1 <- "http://www.jaredlander.com/data/TomatoFirst.csv"
tomato2 <- read_delim(file=z1, delim=',')
# fread
install.packages("dane.table")
library(dane.table)
z1 <- "http://www.jaredlander.com/data/TomatoFirst.csv"
tomato3 <- fread(input=z1, sep=',', header=TRUE)
# UWAGA: pakiet 'dane.table' NIE jest dost�pny dla mojej wersji R

# Dane Excela 
# g�ownie u�ywamy f-cji 'read_excel', czytamy pliki i '.xls' i '.xlsx' 
# read_excel NIE mo�e wczyta� danych bezpo�rednio z internetu, wi�c s�u�y do plik�w pobranych
install.packages("readxl")
library(readxl)
tomatoXL <- read_excel('C:/Users/Patryk/Desktop/ExcelExample.xlsx')
tomatoXL
#
# Inny, te� dobry spos�b otwarcia tego pliku, za pomoc� f-cji 'read.xlsx'
install.packages("openxlsx")
library(openxlsx)
tomatoXL <- read.xlsx("C:/Users/Patryk/Desktop/ExcelExample.xlsx")
head(tomatoXL)
#
# Inny przyk�ad na 'read_excel'
wineXL1 <- read_excel('C:/Users/Patryk/Desktop/ExcelExample.xlsx', sheet=2)
head(wineXL1)

# Wczytywanie z BD
# str. 91-93, na razie odpuszczam
# pliki baz danych pobieramu f-cj� 'download.file'
download.file("http://www.jaredlander.com/data/diamonds.db", 
              destfile = "data/diamonds.db", mode='wb')

# Dane z innych narz�dzi statystycznych
# Wczytywanie plik�w sasowych:
install.packages("RevoScaleR")
library(RevoScaleR) 
# i u�ywamy wtedy f-cji 'RxSasData'

# Pliki binarne j�zyka R (str. 95-97, na razie odpuszczam)

# Dane do��czone do R
# Niekt�re pakiety maj� wbudowane w siebie zbiory danych, 
# np. w ggplot2 jest zbi�r diamonds, u�ywamy do tego f-cji data
data(diamonds, package='ggplot2')
head(diamonds)
data() # Tak dostaniemy list� wszystkich zbior�w dla f-cji data

# Wydobywanie danych z witryn sieci Web (str. 98-101, na razie odpuszczam)
# pakiet - XML, f-cja: readHTMLTable; do plik�w w tabeli HTML
install.packages("XML")
library(XML)
theURL <- "http://www.jaredlander.com/2012/02/another-kind-of-super-bowl-pool/"
bowlPool <- readHTMLTable(theURL, which=1, header=FALSE, stringsAsFactors=FALSE)
bowlPool

# Wczytywanie danych JSON (str. 101-103, na razie odpuszczam)


# 7 - Grafika statystyczna 105 - 123
# Pakiety dodatkowe: 1. ggplot2  2. lattice

# Podstawowe f-cje graficzne
# Stosujemy zbi�r wbudowany w R w pakiet ggplot2, st�d jego instalacja, ale
# w tym podrozdziale ggplot2 nie b�dzie u�ywany
library(ggplot2)
data("diamonds") # wy�wietlamy tabel� w praym g�rnym oknie
head(diamonds) # wy�wietlamy tabel� w konsoli
hist(diamonds$carat, main="Carat Histogram", xlab="Carat")
# Wyja�nienie: 
# I arg - tworzymy histogram dla kolumny carat ze zbioru diamond
# II arg - wykres nazywamy 'Carat Histogram'
# II arg - na osi x mamy warto�ci (waga dament�w w karatach) z kolumny carat i o� ta 
#          nazywamy 'carat' a na osi y cz�stotliwo�� wyst�powania warto�ci
# Wykres punktowy (scatter plot) - pokazuje zale�no�� mi�dzy 2 zmiennymi (o� x a o� y)
plot(price ~ carat ,data=diamonds) # zale�no�� diamnet�w od ich wagi
# price - o� y, carat - o� x
plot(diamonds$carat ,diamonds$price) # II opcja wykresu pkt
# Wykres skrzynkowy (pude�kowy) - s�u�y do tego f-cja boxplot
boxplot(diamonds$carat)
# Intepretacja: �rodkowa gruba linia to MEDIANA

# ggplot2 
# u�ywa si� w nim m. in. f-cji ggplot, potem kolejne warstwy dodaje si� za pomoc� '+'
# wartswy geometryczne = punkty, linie i hstogramy itp.
# Odpowiedaj�ce im f-cje: geom_point, geom_line i geom_histogram
# histogramy i wykresy g�sto�ci
# wykres dystrybucji wag diament�w, czyli histogram w ggplot2:
# histogram jest 1-wymiaarowy, wi�c musimy maposwa� f-cj� tylko raz, na osi x:
install.packages("ggplot2")
library(ggplot2)
ggplot(data=diamonds) + geom_histogram(aes(x=carat))
# Podobny efekt b�dzie przy u�yciu geom_density
# fill - okre�la kolor wype�nienia wykresu; jest poza 'aes' by ca�y wykres mia� ten kolor
ggplot(data=diamonds) + geom_density(aes(x=carat), fill="grey50")
# wykres g�2sto��i pokazuje prawdopodobie�stwo, a histogram liczebno�� w koszykach
# Wykresy pkt. w ggplot2
ggplot(diamonds, aes(x=carat, y=price)) + geom_point()
# same osie i siatka wsp�rz�dnych: ggplot(diamonds, aes(x=carat, y=price))
# Dlatego ten kod zadeklarujemy jako zmienna g, by ci�gle nie wklepywa� tego:
g <- ggplot(diamonds, aes(x=carat, y=price))
g
g + geom_point(aes(color=color)) 
# Dzielimy w ten spos�b punkty (nak�adaj�c kolejn� warstw� w f-cji 'aes') wedle zmiennej 'color'
# Wykresy kafelkowe - f-cje: 'facet_wrap' i 'facet_grid' 
# facet_wrap - wykonuje oddzielny wykres dla ka�dej warto�ci konkretnej zmiennej
g + geom_point(aes(color=color)) + facet_wrap(~color)
# facet_grid - dzieli jeszcze wed�ug kolejnych 2 zmiennych: cut i claroty
g + geom_point(aes(color=color)) + facet_grid(cut~clarity)
ggplot(diamonds, aes(x=carat)) + geom_histogram() + facet_wrap(~color) # histogramy wg kolor�w
# Wykresy skrzynkowe i skrzypcowe (str. 116 - 119)
# Wykres liniowy, np.:
ggplot(economics, aes(x=date, y=pop)) + geom_line()
# bardziej z�0o�one (str.119-121)
# Motywy (str. 121-123)


# 8 - Tworzenie funkcji w j�zyku R (125-131)
say.hello <- function() # Wa�ne: kropka '.' to zwyk�y znak, bez szczeg�lnego znaczenia
{                       # Po 'function' jest nawias, mo�e by� pusty (bez argument�w)
                        # albo mo�e zawiera� dowoln� liczb� argument�w.
  print("Hello, World!") # Cia�o f-cji (kod g��wny) jest zawarty w '{}'
}

# Argumenty funkcji
# Dodajemy je w nawiasie '()'
# sprintf - f-cja, kt�rej I arg. to �a�cuch zawieraj�cy specjalne znaczniki wskazuj�ce 
# miejsce na dane wej�ciowe, kolejne arg. zostan� wstawione zamiast kolejnych znacznik�w
# Przyk�ady:
sprintf("Hello %s", "Jared") # Wynik w konsoli: "Hello Jared"
sprintf("Hello %s, today is %s", "Jared", "Sunday") # Wynik w konsoli: "Hello Jared, today is Sunday"
hello.person <- function(name)
{
  print(sprintf("Hello %s", name))
}
hello.person("Jared") # CZyli s�owo 'Hello' jest sta�e i na I miejscu
# Dwa lub wi�cej argument�w:
hello.person <- function(first, last)
{
  print(sprintf("Hello %s %s", first, last)) # 2 razy '%s', bo s� 2 argumenty
}
hello.person("Jared", "Lander")
hello.person <- function(first, last)
{
  print(sprintf("Hello  %s", first, last)) # raz napisano '%s', wi�c ii arg. si� nie wydrukuje
}
hello.person("Jared", "Lander")
# II sp. przypisania arg. w f-cji:
hello.person(first="Jared", last="Lander")
# Inne mo�liwo�ci wywo�ywania f-cji s� na str. 127

# Argumenty domy�lne:
# Chodzi o to, �e np. jeden z argument�w ma z g�ry ustalon� warto�� domy�ln�, np.
hello.person <- function(first, last="Doe")
{
  print(sprintf("Hello %s %s", first, last)) 
}
hello.person("Jared")
# Ale arg, domy�lny jest <==>, gdy nie podamy do drukowania jego warto�ci, je�li 
# napisali�my hello.person "(first="Jared", last="Lander")", to nadpiszemy nasz arg.

# Zwracane warto�ci
# F-cje zwykle maj� obliczy� jak�� warto��, wi�� musz� odes�a� wynik z powrotem do kodu 
# wywo�uj�cego, i to jest w�a�nie zwracanie
# Przyk�ad 
double.num <- function(x)
{
  x*2
}
double.num(5)
# Mo�na u�y� do tego te� polecenia 'return', wskazuje ono warto�� jaka powinna by� zwr�cona
double.num <- function(x)
{
  return(x*2)
}
double.num(5)
# Funkcja do.call - nie przerabia�em, str. 130-131


# 9 - Wyra�enia steruj�ce (w-kowe) (133-140)
# G��wne f-cje: IF, ELSE, IFELSE, SWITCH

# IF i ELSE
# sprawdzenie r�wno�ci operatorem: '=='
as.numeric(TRUE) # wynik to 1, dla 'false' to 0
a <- 1 # je�li a=1, to napisz 'hello'
if(a==1)
{
  print("hello")
}
if(a==9) # Tu nic si� nie wy�wietli, bo a != 9
{
  print("hello")
}
# IF + ELSE w funkcji
funkcja <-function(x)
{
  if (x==1)
  {
    print("hello")
  } else # Wa�ne: ELSE musi by� w tej linii kodu, a nie w nast�pnej
  {
  print("bye")
  }
}
funkcja(5)
funkcja(TRUE)
# Dok�adamy 'ELSE IF'
funkcja <-function(x)
{
  if (x==1)
  {
    print("hello")
  } else if (x==0) # Wa�ne: ELSE musi by� w tej linii kodu, a nie w nast�pnej
  {
    print("bye")
  } else
  {
    print("confused")
  }
}
funkcja(1)
funkcja(0)
funkcja(9)

# switch
# Wygodniejsze jest switch, ni�eli else if, gdy mamy wiele przypadk�w
# I arg. twgo wyra�enia to testowana zmienna, kolejne to okr. warto�ci ,zkt�rymi 
# por�wnywana jest ta zmienna
# Przyk�ad
use.switch <- function(x)
{
  switch(x, "a"="first", "b"="second", "z"="last", "c"="third", "other")
}
use.switch("a")
# Gdy I arg. jest numeryczny, jest dopasowany pozycyjnie do kolejnych arg., np.
use.switch(1) # Wynik to  "first", bo 1=="a"
# Jest 5 arg., wi�c ka�dy kolejny NIC NIE zwr�ci
use.switch(5)


# ifelse
# I arg. to w-k do przetestowania, II to warto�� zwracana przy spe�nieniu w-ku,
# III to warto�� zwracana przy braku spe�nienia w-ku
ifelse (1==1, "Yes", "No") # Wynik: "Yes"
Test <- c(1, 1, 0, 1, 0, 1)
ifelse(Test == 1, "Yes", "No")
ifelse(Test == 1, Test*3, Test)
ifelse(Test == 1, Test*3, "Zero")

# Z�o�one testy
a <- c(1, 1, 0, 1)
b <- c(2,1,0,1)
ifelse(a==1 & b==1, "Yes", "No")
ifelse(a==1 && b==1, "Yes", "No") # Podw�jny '&&' powoduje sprawdzenie tylko I elementu


# 10 - P�tle - nieR metoda iteracji (141-144)
# G��wnie: for i while

# P�tle for
# wykonuje iteracj� poprzez indeks - dostarczany jako zmienna typu vector
# Kolejne 10 pierwszych liczb naturalnych:
for(i in 1:10)
{
  print(i) + 1
}
# R�wnowa�ny spos�b:
print(1:10)
# Zaawansowany przyk�ad:
fruit <- c("apple", "banana", "pomegranate")
fruitLength <- rep(NA, length(fruit))
fruitLength 
names(fruitLength) <- fruit
fruitLength 
for (a in fruit)
{
  fruitLength[a] <- nchar(a)
}
fruitLength 
# II sp. na "Zaawansowany przyk�ad", du�o prostszy:
fruit <- c("apple", "banana", "pomegranate")
fruitLength2 <- nchar(fruit)
names(fruitLength2) <- fruit
fruitLength2

# P�tle while
# powoduje powtarzanie wykonanego kodu dop�ki sprawdzany w-k jest spe�niany
x <- 1
x
while (x<=5)
{
  print(x)
  x <- x+1
}

# Sterowanie p�tlami
# M. in. do pomijania kt�rej� iteracji lub do przerwania p�tli
# Polecenia: next, break
# 1.
for(i in 1:10)
{
  if(i == 3)
  {
    next +  
  }
    print(i) + 
}
# 3 nie by�a wpisana
# 2.
for(i in 1:10)
{
  if(i == 4)
  {
    break  
  }
  print(i) +}
# iteracja zosta�a zatrzymana na liczbie 3


# 17 - Rozk�ady prawdopodobie�stwa (243-260)
# Zmienna losowa � funkcja przypisuj�ca zdarzeniom elementarnym liczby
# Rozk�ad prawdopodobie�stwa okre�la prawdopodobie�stwo przyj�cia ka�dej mo�liwej
# warto�ci przez zmienn� losow�
# G�sto�� - prawdopodobiie�stwo wyst�pienia okre�lonej warto�ci dla rozk�adu prawd.
# Dystrybuanta - prawd., �e wylosowana liczba jest <= danej warto�ci

# Rozk�ad normalny (Gaussa), mi - �rednia, sigma - odch. std.
# rnorm - do wybierania okkre�lonej ilo�ci liczb losowych o r. normalnym,
#         pozwala te� okre�li� parametry mi i sigma
# losowanie 10 liczb ze standartowego(0-1) r. normalnego
rnorm(n=10)
rnorm(n=10, mean=100, sd=20) # zmiana parametr�w
# dnorm - oblicza g�sto��
randNorm10 <- rnorm(10)
randNorm10
dnorm(randNorm10)
dnorm(c(-1, 0, 1))
# generowanie zmiennych + wykres
randNorm <- rnorm(300)
randNorm
density <- dnorm(randNorm) # wyznaczenie g�sto�ci
library(ggplot2)
ggplot(data.frame(x=randNorm, y=density)) + aes(x=x, y=y) + geom_point() 
# pnorm - dystrybuanta
pnorm(randNorm10)
# obliczanie prawd., �e zmienna znajdzie si� mi�dzy pkt. a i b: 
# obliczenie prawd. w a i b i odj�cie od siebie
pnorm(1) - pnorm(-1)

# Rozk�ad dwumianowy

# Rozk�ad Poissona

# Inne rozk�ady