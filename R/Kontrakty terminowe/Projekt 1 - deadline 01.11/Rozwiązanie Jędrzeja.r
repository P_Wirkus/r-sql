# isprimroot to jest do sprawdzenia czy sa pierwiastkiem pierwotnym
# primroot na samym dole szuka pierwiastka pierwotnego

#funkcja modulo
mod<-function(x,m){
  x<-x-floor(x/m)*m
  return(x)
}
mod(7,5)
#funckja skaluj�ca warto�ci do warto�ci z przedzia�u [0,1]
scale01 <- function(x){
  (x-min(x))/(max(x)-min(x))
}
scale01(0.7)
#funkcja sprawdzaj�ca, czy a jest pierwiastkiem pierwotnym modulo m
isprimroot<-function(a,m){
  t<-seq(1,m-1)                 #stworzenie wektora, do kt�rego b�d� por�wnywa�
  q<-seq(1,m-1)                 #wektor, do kt�rego b�d� przypisane kolejne reszty
  for(i in 1:(m-1)){
    q[i]<-mod(a^i,m)            #przypisanie kolejnych reszt
  }
  q<-sort(q)                    #posortowanie rosn�co (potrzebne do por�wnania)
  if(all(q==t))return(TRUE)
  return(FALSE)
}
#funkcja generuj�ca warto�ci z rozk�adu jednostajnego z przedzia�u [0,1]
#przy u�yciu metody kongurencji liniowej
unif_gen<-function(x0,n,m=2^31-1,a=16807){
  m<-abs(m)
  a<-abs(a)
  if(a>=sqrt(m)){               #sprawdzenie odpowiednich warunk�w
    cat('Mo�liwo�� wyst�pienia przeci��enia \n a<sqrt(m)\n')
  }
  if(m<2^20){
    if(isprimroot(a,m)==FALSE){
     cat('parametr a nie jest pierwiastkiem pierwotnym modulo m \n wyst�pi mniejsza dok�adno��')
    }
  }
  if(x0==0){
    cat('seed x0 nie mo�e by� r�wny 0\n')
    break
  }
  gen<-c(1:n)                   
  gen[1]<-x0
  q<-floor(m/a)
  r<-mod(m,a)
  if(gen[1]<0){
    gen[1]<-gen[1]+m
  }
  for(i in 1:n){
    k<-floor(gen[i]/q)
    gen[i+1]<-a*(gen[i]-k*q)-k*r
    if(gen[i+1]<0){
      gen[i+1]<-gen[i+1]+m
    }
  }
  gen<-scale01(gen)
  par(mfrow=c(1,2))
  hist(gen,main="g�sto�� empiryczna")
  plot(ecdf(gen),main='dystrybuanta empiryczna')
  return(gen)
}
a<-unif_gen(1,100000)
b<-runif(100000)
par(mfrow=c(1,2))
hist(a)
hist(b)
unif_gen(1,100000,7,3)
#funkcja do znajdowania pierwiastkow pierwotnych do testowania
primroot<-function(m){
  m<-abs(m)
  for(j in 2:(m-1)){
    a<-j
    t<-seq(1,m-1)                 
    q<-seq(1,m-1)                 
    for(i in 1:(m-1)){
      q[i]<-mod(a^i,m)            
    }
    q<-sort(q)                    
    if(all(q==t))return(a)
  }
  cat('brak pierwiastk�w pierwotnych')
}
primroot(19)

