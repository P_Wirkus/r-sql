library(MASS)
library(glmnet)
library(Metrics)

set.seed(123)

#zad 1

train_size = 0.7 *dim(Boston)[1]
test_size = 0.3 * dim(Boston)[1]

train_index = sample(1:nrow(Boston), train_size)

train_set = Boston[train,]
test_set = Boston[-train,]

X_train = train_set[-c(14)]
X_train_mat = as.matrix(X_train)

y_train = train_set$medv


X_test = test_set[-c(14)]
X_test_mat = as.matrix(X_test)

y_test = test_set$medv
  
#zad 2 / 3

linear = lm(medv ~ ., data=Boston)
summary(linear)
coef(linear)

ridge_reg = glmnet(X_train_mat, y_train, alpha = 0, lambda = 0.7)
summary(ridge_reg)
coef(ridge_reg)

lasso_reg = glmnet(X_train_mat, y_train, alpha = 1, lambda = 0.02)
summary(lasso_reg)
coef(lasso_reg)

#zad 4

#linera
linear_pred_train = predict(linear, X_train)
linear_pred_train_mse = mse(linear_pred_train, y_train)

linear_pred_test = predict(linear, X_test)
linear_pred_test_mse = mse(linear_pred_test, y_test)

#ridge
ridge_pred_train = predict(ridge_reg, X_train_mat)
ridge_pred_train_mse = mse(ridge_pred_train, y_train)

ridge_pred_test = predict(ridge_reg, X_test_mat)
ridge_pred_test_mse = mse(ridge_pred_test, y_test)

#lasso
lasso_pred_train = predict(lasso_reg, X_train_mat)
lasso_pred_train_mse = mse(lasso_pred_train, y_train)

lasso_pred_test = predict(lasso_reg, X_test_mat)
lasso_pred_test_mse = mse(lasso_pred_test, y_test)

#zad 5 TO SAMO CO WYRZEJ TYLKO INNE PROPORCJE ZBIORU!

#zad 6 (dodatkowe) jak mi starczy czasu