#wykasuj dane z poprzedniej sesji
ls() #lista obecnie zdefiniowanych obiektów
rm() #usuwanie obiektu
rm(list=ls()) #usunięcie wszystkich obiektów
cat("\014")

#instalacja paczek
install.packages('dplyr')
library(dplyr)
require(dplyr)
base::print('hello world')

##### Lista #####
x <- c(2,1,8,16,4) #wektor
y <- rep("an",5) #sekwencja
y2<-rep(c("an","yy","xx"),c(1,2,3))
z <- as.Date("03-02-1997",format="%d-%m-%Y") #zapis daty
dataseq<- seq(from=as.Date("2003-02-19"),to=as.Date("2010-02-19"),by="1 year")
lista <- list(liczby=x, s?owa=y, data = dataseq)
lista
names(lista)
summary(lista)

lista[[2]] #odwoływanie się do obiektów w listach
lista[["s?owa"]]
lista$s?owa

lista[[1]][[3]] #pierwsza współrzędna odwołanie do wektora, a druga do elementu
lista[["liczby"]][[3]]

append(lista,list(dodatek=x)) #dodawanie wektora do listy
lista$mama<-c(1,2,3)

data1<-as.POSIXlt('2019-02-28')
data1$year
data1$mon
data1$year*12+data1$mon-(data1$year*12+data1$mon) #rozwiązanie problemu różnicy miesięcy/lat

#### Tabele danych ####
iris<-iris
class(iris)
iris$Sepal.Length #tabela danych zachowuje się jak macierz, mo?e jednak zawierać dowolny typ danych
colnames(iris)
rownames(iris)
iris[1,]
iris[1,1]
iris["Sepal.Length"]
iris[iris$Sepal.Width>3,] #filtrowanie wierszy
iris$Sepal.Length[iris$Sepal.Width>3] #filtrowanie wartości z jednej kolumny

df1<-c(1,2,3,45,5)
df2<-letters[1:5]
df3<-rep(as.Date('2019-12-31'),5)
df<-data.frame(numery=df1,text=df2,daty=df3) #tworzenie tabeli danych
df
dfm<-as.data.frame( matrix((1:9), nrow = 3, ncol=3, byrow=TRUE))
dfm
dfm['mama',]<-c(1,2,3)

#dodawanie kolumn i wierszy
df$dodatek<-c(5,4,3,2,1)
df$suma<-df$numery+df$dodatek
union_all(df,df) #union_all dodaje wszystkie rekordy, union dodaje unikalne rekordy
union_all(df,as.data.frame(diag(5))) #dołączenie rekordów jako nowe kolumny 
rbind(df,df) #dołączane obserwacje muszą mieć te same kolumny
cbind(df,diag(5)) #dołączanie kolumn, musi być identyczna liczba wierszy

##### wyrażenie warunkowe #####

#if(test){wyrażenie}else{wyrażenie}

c <- TRUE
if(c==TRUE) { 
  print("tak")
} else { 
  print("nie")
} 

#zagnieżdżone wyrażenie
xyz<-1
if(xyz==2) { 
  print("xyz wynosi 2")
} else {
  if(xyz==1){
    print('xyz wynosi 1')
  }else{
    print("xyz nie wynosi 1 ani 2")
  }
} 

#bardziej przystępna wizualnie forma dzięki else if
if(xyz==2) { 
  print("xyz wynosi 2")
} else if (xyz==1){
  print('xyz wynosi 1')
}else{
  print("xyz nie wynosi 1 ani 2")
}

##### Pętle #####

#for(instrukcja){wyra?enie} 
#instrukcja ma sk?adnie "i in wektor"

x <- 1
#wyrażenie zostanie wykonane dla każdego i w wektorze
for(i in 1:5){
  x <- c(x,i)
}
x

for(i in y){print(i)} # i może być nie tylko liczbą

tmp<-proc.time() #petla w pętli
n=seq(1,100000)
while(n[length(n)]<=100500){ #pętla while będzie wykonywana dopóki warunek jest spełniony
  for(i in 1:length(n)){
    n[i]=n[i]+1
  }
}
proc.time() - tmp

tmp<-proc.time()
n=seq(1,100000) #używanie pętli jest zasobożerne, kiedy jest to możliwe unikajmy używania
while(n[length(n)]<=100500){
  n=n+1
}
proc.time() - tmp


##### Funkcje #####

#nazwa <- function(argumenty){wyrażenie}

taknie <- function(pop) {
  if(class(pop)=="logical"){
    if(pop==TRUE) { 
      u <- "Tak"
    } else { 
      u <- "Nie"
    }
    return(u)
  }else{
    print("zły argument")
  }
}


TF2=1==2
TF3=1==1
taknie(TF2)

pochodna<-function(f,x,t){
  return((f(x+t)-f(x-t))/(2*t))
}

f1<-function(x){
  sin(x)
}

pochodna(f1,3,10^-5)

gradient=function(f,x,t){  #implementacja gradientu
  y<-c()
  for(i in 1:length(x)){
    p<-rep(0,length(x))
    p[i]=t
    y[i]=(f(x+p)-f(x-p))/(2*t)
  }
  return(y)
}

f2=function(x){
  x[1]^2+x[2]^3
}
f3=function(x){
  sin(x[1])+cos(x[2])
}

gradient(f2,c(2,3),10^-5)
gradient(f3,c(pi,pi/2),10^-5)

#apply - operacje na tabelach (data.frame)
X <- data.frame(rnorm(30), nrow=5, ncol=6)
apply(X, 2, sum) #kolumny
apply(X, 1, sum) #wiersze

#lapply - ogólniona wersja apply - możliwość operowania na listach i wektorach
MyList <- list(matrix(c(1,2,3,4,5,6,7,8,9),3,3),matrix(c(1,2,9,4,3,6,7,8,9),3,3,byrow = FALSE),matrix(c(1,10,3,4,10,6,7,10,9),3,3))
lapply(MyList,"[", , 2)
lapply(MyList,"[", 1, )
lapply(MyList,"[", 1, 1)
lapply(MyList,sum)
lapply(lapply(MyList,"[", 1,),sum)
unlist(lapply(MyList,"[", 2, 1 ))
#sapply - działa podobnie do lapply, ale wynikiem jest możliwie najprostsza reprezentacja danych
sapply(MyList,"[", , 2)
sapply(MyList,"[", 2, 1, simplify=F)
sapply(MyList,"[", 2, 1, simplify=T)
# mapply - powtórzenie do wektora
Q1 <- matrix(c(rep(1, 4), rep(2, 4), rep(3, 4), rep(4, 4)),4,4)
print(Q1)
#lub
Q2 <- mapply(rep,1:4,4)
print(Q2)

#PRACA DOMOWA
#W pliku word
#Wskaz?wki
# 1. Utwórz wektory dla każdego koloru.
# 2. Dla każdego rodzaju kości pobieraj wynik tyle razy ile potrzebujesz
# 3. Pamiętaj, aby utworzyć pusty wektor do przechowywania wartości z symulacji.
# 4. Przydatne funkcje:
sample()
sum()
c()
length()
which()