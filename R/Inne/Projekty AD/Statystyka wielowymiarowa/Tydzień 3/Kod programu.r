
#dane
x = matrix(c(-7,-6,-4,-3,-2,1,2,5,6,8, -4,-2,-2,-3,1,-1,2,1,5,3), 10, 2)

#wykres rozrzutu
plot(-6:6, -2:10, type = "n")
points(x, col = "black", pch = 19)

#wyznaczenie kata
s = cov(x)
wektory = eigen(s)$vectors
theta = acos(abs(wektory[1,1]))
if(wektory[1,1] < 0)
{
  theta = theta * -1
}

#macierz obrotu
RotationMatrix <- function(theta)
{
    return(matrix(c(cos(theta),sin(theta),-sin(theta), cos(theta)), 2,2))
}

#wartosc probki po obrocie o kat theta
Q = matrix(c(mean(x[,1]),mean(x[,2])), 2,1)
n_x = matrix(data=NA, nrow=dim(x)[2], ncol=dim(x)[1])
for(i in 1 : dim(x)[1])
{
  n_x[,i] = RotationMatrix(theta) %*% (as.matrix(x[i,]) - Q) + Q  
}               
n_x = t(n_x)

#wykres rozrzutu nowej probki
plot(-6:6, -2:10, type = "n")
points(n_x, col = "black", pch = 19)

#odleglosc dla probki w nowym ukladzie
n_s = cov(n_x)
n_d = sqrt(sum((n_x[3,] - Q)**2 / c(n_s[1,1], n_s[2,2])))

#odleglosc dla probki w starym ukladzie
theta = -theta

m1 = s[1,1] * cos(theta)**2 + 2 * s[1,2]*sin(theta)*cos(theta) + s[2,2] * sin(theta)**2
m2 = s[2,2] * cos(theta)**2 - 2 * s[1,2]*sin(theta)*cos(theta) + s[1,1] * sin(theta)**2

a11 = cos(theta)**2 / m1 + sin(theta)**2 / m2
a22 = sin(theta)**2 / m1 + cos(theta)**2 / m2
a12 = cos(theta)*sin(theta) / m1 - cos(theta)*sin(theta) / m2

x1y1 = (x[3,1] - Q[1])
x2y2 = (x[3,2] - Q[2])

d = sqrt(a11 * x1y1**2 + 2 * a12 * x1y1 * x2y2 + a22 * x2y2**2)

#Porownanie
d
n_d
#d = n_d czyli odleglosci wychodza takie same
