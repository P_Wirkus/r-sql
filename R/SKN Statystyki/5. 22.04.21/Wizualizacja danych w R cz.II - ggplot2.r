##### Czyszczenie konsoli, srodowiska, wykres�w #####
cat("\014") 
rm(list=ls())

#library(datasets)
data(mpg)

summary(dane)
str(mpg)
View(iris)
## ggplot2 cheat sheet - arkusz ze wszystkimi najwa�niejszymi funkcjonalnoociami paczki
## https://rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf 
## dokumentacja paczki
## https://cran.r-project.org/web/packages/ggplot2/ggplot2.pdf 
# lista opcji graficznych dostepnych w paczce
vignette("ggplot2-specs")

####skale####
#skale kontroluj� jak na wykresie mapowane s� dane
#scale_**_() - podstawowa funkcja do kontroli funkcja
#** oznacza element, kt�ry chcemy modyfikowa� np o� x lub o� y, albo wype�nienia
#()  odwo�uje si� do typu danych
#scale_*_continuous() - dane to Zmienna ci�g�a np wiek, wysoko�c 
#scale_*_discrete() - zmienne dyskretne np oceny w szkole
#scale_*_identity() - inne skale np gdy kolumna zawiera informacje o kolorach
#scale_*_date(date(date_labels = "%m/%d"), date_breaks = "2weeks") - skala do dat
#scale_*_datetime() - skala dat w uk�adzie data-godzina
#scale_*_manual(values = c()) - dodanie skali w postaci wektora

#skala osi x i y
#zamienienie zmiennej ci�g�ej na dyskretn�
ggplot(mtcars) +
  geom_bar(aes(mpg)) +
  scale_x_binned( n.breaks = 5)
#ustawienia osi dla zmiennych ci�g�ych
p1 <- ggplot(mpg, aes(displ, hwy)) +
  geom_point()
p1
p1 +
  scale_x_continuous("Engine displacement (L)") +
  scale_y_continuous("Highway MPG")
p1 + scale_x_continuous(limits = c(0, 10))
p1 + scale_x_continuous(breaks = c(2, 4, 6))
p1 + scale_x_continuous(
  breaks = c(2, 4, 6),
  label = c("two", "four", "six")
)
#skale
?scales
p1+scale_y_continuous(labels = scales::percent)
p1 + scale_y_continuous(labels = scales::dollar)
p1 + scale_y_log10()
p1 + scale_y_sqrt()
p1 + scale_y_reverse()

#ustawienia osi dla zmiennych dyskretnych
ggplot(diamonds, aes(cut)) + geom_bar() #histogram
d <- ggplot(subset(diamonds, carat > 1), aes(cut, clarity)) +
  geom_jitter()
d
d + scale_x_discrete("Cut", labels = c("Fair" = "F","Good" = "G",
                                       "Very Good" = "VG","Perfect" = "P","Ideal" = "I"))
d + scale_x_discrete(limits = c("Fair","Ideal"))
d + xlim("Fair","Ideal", "Good")
#sortowanie
?reorder
ggplot(mpg, aes(manufacturer, cty)) + geom_point()
ggplot(mpg, aes(reorder(manufacturer, cty), cty)) + geom_point()
#skr�towce
?abbreviate
ggplot(mpg, aes(reorder(manufacturer, cty), cty)) +
  geom_point() +
  scale_x_discrete(labels = abbreviate)
ggplot(mpg, aes(reorder(manufacturer, cty), cty)) +
  geom_point() +
  scale_x_discrete(label=function(x) abbreviate(x, minlength=3))
#szeregi czasowe
last_month <- Sys.Date() - 0:29 # szybkie wybranie n dat wstecz
df <- data.frame(
  date = last_month,
  price = runif(30)
)
base <- ggplot(df, aes(date, price)) +
  geom_line()

base + scale_x_date(date_breaks = "1 week", date_labels = "%W")
base + scale_x_date(limits = c(Sys.Date() - 7, NA))
#skala lini
#https://ggplot2.tidyverse.org/reference/scale_linetype-3.png
base <- ggplot(economics_long, aes(date, value01))
base + geom_line(aes(group = variable))
base + geom_line(aes(linetype = variable))
#r�czne ustalanie skali
p <- ggplot(mtcars, aes(mpg, wt)) +
  geom_point(aes(colour = factor(cyl)))
p
p + scale_colour_manual(values = c("yellow", "red", "black"))
cols <- c("8" = "red", "4" = "yellow", "6" = "orange", "10" = "black")
p + scale_colour_manual(values = cols)
p + scale_colour_manual(
  values = c("yellow", "red", "black"),
  breaks = c("4", "6", "8"),
  labels = c("four", "six", "eight")
)
p + scale_colour_manual(values = cols, limits = c("4", "8"))
p + scale_colour_manual(values = cols, limits = c("4", "6", "8", "10"))
#skala kszta�ty
#https://ggplot2.tidyverse.org/reference/scale_shape-6.png
dsmall <- diamonds[sample(nrow(diamonds), 100), ]
d <- ggplot(dsmall, aes(carat, price)) + geom_point(aes(shape = cut))
d
d + scale_shape(solid = FALSE)

#####scale_colour, scale_fill - kolor#####
v<-ggplot(faithfuld, aes(waiting, eruptions, colour = density)) +
  geom_tile()
v + scale_colour_continuous(type = "viridis")
v + scale_colour_viridis_c(option = "plasma" )

df <- data.frame(
  x = runif(100),
  y = runif(100),
  z1 = rnorm(100)
)
#gradient
dsamp <- diamonds[sample(nrow(diamonds), 1000), ]
d <- ggplot(dsamp, aes(carat, price)) + geom_point(aes(colour = clarity))
#ko�o kolor�w
d + scale_colour_hue("clarity")
d + scale_colour_hue(l=65,c=200,h = c(0, 360)) # l - luminacja(nat�enie �wiat�a) - bazowo 65 c- nasycenie kolor�w - bazowo 100
#kolor stopnie
v<-ggplot(df, aes(x, y)) +
  geom_point(aes(colour = z1)) 
  v+scale_colour_steps() # gradient low-high
  v+scale_colour_steps(low='red',high='green')
  v+scale_colour_steps(low='red',high='green',guide='legend') # dla zmiennych dyskretnych
  v+scale_colour_steps2() # gradient low-mid-high
  v+scale_colour_stepsn(colours = terrain.colors(10)) #gradient n stopniowy
  v+scale_colour_stepsn(colours = rainbow(5)) 
  v+scale_colour_stepsn(colours = heat.colors(5)) 
  v+scale_colour_stepsn(colours = topo.colors(5))
  v+scale_colour_stepsn(colours = cm.colors(5)) 
  v + scale_colour_brewer()
  # scale_alpha - prze�roczysto��
  p <- ggplot(mpg, aes(displ, hwy)) +
    geom_point(aes(alpha = year))
p
    p + scale_alpha(range = c(0.4, 0.8))
  #colorbrewer - https://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3
    #paleta przenaczona g�ownie do wy�wietlania danych na mapach , przydatna do wy�wietlania danych dyskretnych
d <- ggplot(mpg, aes(displ, cty)) +
  geom_point(aes(colour = as.factor(cyl)))
d
d + scale_colour_brewer()
d + scale_colour_brewer(palette = "Greens")
d + scale_colour_brewer(palette = "Set1")

#modyfikacja do zmiennych ci�g�ych
v <- ggplot(faithfuld) +
  geom_tile(aes(waiting, eruptions, fill = density))
v + scale_fill_brewer(palette = "Set1")
v + scale_fill_distiller(palette="Set1")
v + scale_fill_fermenter(palette="Set1")
#Diverging
#BrBG, PiYG, PRGn, PuOr, RdBu, RdGy, RdYlBu, RdYlGn, Spectral
#
#Qualitative
#Accent, Dark2, Paired, Pastel1, Pastel2, Set1, Set2, Set3
#
#Sequential
#Blues, BuGn, BuPu, GnBu, Greens, Greys, Oranges, OrRd, PuBu, PuBuGn, PuRd, Purples, RdPu, Reds, YlGn, YlGnBu, YlOrBr, YlOrRd

#skala szaro�ci
p <- ggplot(mtcars, aes(mpg, wt)) + geom_point(aes(colour = factor(cyl)))
p + scale_colour_grey()
p + scale_colour_grey(start=0,end =1) #0 - czarny 1 - bia�y
p + scale_colour_grey() + theme_bw()
miss <- factor(sample(c(NA, 1:5), nrow(mtcars), replace = TRUE))
ggplot(mtcars, aes(mpg, wt)) +
  geom_point(aes(colour = miss)) +
  scale_colour_grey(na.value = "blue")
#skale jako dane wej�ciowe
df <- data.frame(
  x = 1:4,
  y = 1:4,
  colour = c("red", "green", "blue", "yellow")
)
ggplot(df, aes(x, y)) + geom_tile(aes(fill = colour))

ggplot(df, aes(x, y)) +
  geom_tile(aes(fill = colour)) +
  scale_fill_identity()

ggplot(mtcars, aes(mpg, wt)) +
  geom_point(aes(size = cyl)) +
  scale_size_identity()

df_shapes <- data.frame(shape = 0:24)
ggplot(df_shapes, aes(0, 0, shape = shape)) +
  geom_point(aes(shape = shape), size = 5, fill = 'red') +
  scale_shape_identity() +
  facet_wrap(~shape) +
  theme_void()

#####dodatkowe wartsty#####
#adnotacje
p
p+ annotate("text", x = 12, y = 5, label = "Some text")
p+ annotate("text", x = c(5,10,15,20), y = 4, label = "Some text")
p + annotate("rect", xmin = 12.5, xmax =20 , ymin = 3, ymax = 4.5,
             alpha = 0.2) # obszar
p + annotate("segment", x = 12.5,  y = 4 ,ymin= yend = 4.5,
             colour = "blue") #linia
p + annotate("pointrange", x = 12.5, y = 4, ymin = 3 , ymax = 4.5,
             colour = "red", size = 1.5) #linia pionowa z punktem
p + annotate("text", x = 12, y = 5, label = "italic(R) ^ 2 == 0.75",
             parse = TRUE)
p + annotate("text", x = 12, y = 5, label = "bold(\'R    R\')^2 == 0.75",
             parse = TRUE)

#histogramy
ggplot(diamonds, aes(price, fill = cut)) +
  geom_histogram(binwidth = 500)
ggplot(diamonds, aes(price, fill = cut)) +
  geom_histogram(binwidth = 500, position = "fill")

#wykresy ko�owe
pie <- ggplot(mtcars, aes(x = factor(1), fill = factor(cyl))) +
  geom_bar(width = 1)
pie + coord_polar(theta = "y")
#coxcomb plot
cxc <- ggplot(mtcars, aes(x = factor(cyl))) +
  geom_bar(width = 1, colour = "black")
cxc + coord_polar()
#The bullseye chart
pie + coord_polar()
#pac man :)
df <- data.frame(
  variable = c("mouth", "body"),
  value = c(20, 80)
)

pac<-ggplot(df, aes(x = "", y = value, fill = variable)) +
  geom_col(width = 1) +
  scale_fill_manual(values = c("yellow", "gray")) +
  coord_polar("y", start = 1) +
  labs(title = "Pac man")


#zapisywanie
#eps", "ps", "tex" (pictex), "pdf", "jpeg", "tiff", "png", "bmp", "svg" or "wmf"
?ggsave()
ggsave('pac.png',pac,dpi=1080)
ggsave('pac.pdf',pac)
