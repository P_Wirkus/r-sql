install.packages("transformr")
install.packages("png")
install.packages("gganimate")
install.packages("gifski")

library(ggplot2)
library(gganimate)
theme_set(theme_bw())

Symulacja <- function(wyniki)
{
  p <- ggplot(wg1, aes(x, y)) +
    geom_line() +
    labs(title = 'Czas: {frame_time}', x = "X", y = "Y") +
    transition_time(t)
  
  return(animate(p, nframes = 100, fps = 30 ,detail = 10 ,duration = 10,renderer = gifski_renderer()))
}

#Przyklad 1
wg1 = read.csv("C:\\Users\\Patryk\\Desktop\\STUDIA\\SEMESTR PI�TY\\Metody numeryczne\\Laboratorium\\Zadanie_6(P_5)\\Obliczenia w Excelu\\g1.csv")
an1 = Symulacja(wg1)
anim_save("AnimationG1.gif", animation = an1, path = "C:\\Users\\Patryk\\Desktop")

#Przyklad 2
wg2 = read.csv("C:\\Users\\Patryk\\Desktop\\STUDIA\\SEMESTR PI�TY\\Metody numeryczne\\Laboratorium\\Zadanie_6(P_5)\\Obliczenia w Excelu\\g2.csv")
an2 = Symulacja(wg2)
anim_save("AnimationG2.gif", animation = an2, path = "C:\\Users\\Patryk\\Desktop")

#Przyklad 3
wg3 = read.csv("C:\\Users\\Patryk\\Desktop\\STUDIA\\SEMESTR PI�TY\\Metody numeryczne\\Laboratorium\\Zadanie_6(P_5)\\Obliczenia w Excelu\\g3.csv")
an3 = Symulacja(wg3)
anim_save("AnimationG2.gif", animation = an3, path = "C:\\Users\\Patryk\\Desktop")