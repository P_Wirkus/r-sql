---
title: "part VII - Recurrent Neural Networks"
author: "Michał Maj"
output: html_notebook
---

```{r packages}
library(keras)
library(tidyverse)
```

**Rekurencyjne sieci neuronowe** mogą być używane do wielu różnych zadań, takich jak tłumaczenie z języka na język, prognozowanie szeregów czasowych, tłumaczenie tekstu mówionego i pisanego, generowanie tekstu / muzyki, klasyfikacja sekwencji / problemy z regresją. Zaczniemy od prostej analizy nastrojów (klasyfikacji). Wykorzystamy dane z Twittera:


```{r sentiment140}
load("data/sentiment140.RData")
sentiment140_X %>% head()
```

Pierwszą rzeczą, którą musimy zrobić, jest zamiana zwykłego tekstu na tokeny. Zaczniemy od stworzenia tokenizera:

```{r sentiment140_tokenizer}
tokenizer <- text_tokenizer(
  num_words = 20000, # Max number of unique words to keep
  filters = "!\"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n", # Signs to filter out from text
  lower = TRUE, # Schould everything be converted to lowercse
  split = " ", # Token splitting character
  char_level = FALSE, # Should each sign be a token
  oov_token = NULL # Token to replace out-of-vocabulary words
)
sentiment140_X <- iconv(sentiment140_X, to = "UTF-8")
tokenizer %>% fit_text_tokenizer(sentiment140_X)
```

Teraz za pomocą tokenizera możemy zamienić surowe zdania w tokeny:

```{r sentiment140_tokens}
sequences <- texts_to_sequences(tokenizer, sentiment140_X)
```

Jak zapewne pamiętasz, w Kerasie każda próbka musi być zapisana jako tensor o tym samym kształcie. W przypadku RNN wstawiamy sekwencje i każda sekwencja musi być tej samej długości. Musimy dopełnić/przyciąć nasze sekwencje:

```{r sentiment140_pad}
maxlen <- 50
sequences_pad <- pad_sequences(sequences, maxlen = maxlen)
```

Teraz możemy podzielić sekwencje na zestaw i zbiór testowy:

```{r sentiment140_split}
train_factor <- sample(1:nrow(sequences_pad), nrow(sequences_pad) * 0.8)
sentiment140_X_train <- sequences_pad[train_factor, ]
sentiment140_X_test <- sequences_pad[-train_factor, ]
sentiment140_Y_train <- sentiment140_Y[train_factor]
sentiment140_Y_test <- sentiment140_Y[-train_factor]
```

Pomyślmy trochę o reprezentacji słów. W naszym przypadku każde słowo można przedstawić jako wartość z przedziału od 0 do 20000, ta wartość reprezentuje klucz w słowniku. Do obliczeń numerycznych to nie wystarczy… każde słowo będzie reprezentowane jako zakodowany na gorąco wektor o wymiarowości 20000. To duża liczba. Jest lepszy sposób na przedstawianie słów (i nie tylko), możemy użyć tzw. **embeddingów**. W Kerasie możemy łatwo dodać do naszej sieci neuronowej warstwę embeddingów:

```{r sentiment140_embedding}
model <- keras_model_sequential() %>%
  layer_embedding(input_dim = 20000,
                  output_dim = 128, # Represent each word in 128-dim space
                  input_length = maxlen)
```

Po warstwie embeddingów możemy dodać warstwę rekurencyjną do warstwy wyjściowej:

```{r sentiment140_rnn}
model %>%
  layer_simple_rnn(units = 32) %>%
  layer_dense(units = 1, activation = "sigmoid")
```

Po ukończeniu architektury możemy skompilować model:

```{r sentiment140_compile}
model %>% compile(
  optimizer = "rmsprop",
  loss = "binary_crossentropy",
  metrics = c("accuracy")
)
```

I dopasować go:

```{r sentiment140_fit}
history <- model %>% fit(
  sentiment140_X_train,
  sentiment140_Y_train,
  epochs = 10,
  batch_size = 128,
  validation_split = 0.2
)
```

Widzimy nadmierne dopasowanie, zamiast tego wypróbujmy teraz jednostki LSTM lub GRU:

```{r sentiment140_lstm}
# Ex. Expand exsisting model by changing simple RNN units for stacked LSTM (or GRU) layers.
# 1. Model architecture:
# Use same embedding layer
# Add LSTM layer with 15 units, recurrent dropout with 0.5 rate and don't forget to return sequences to LSTM on top
# Add LSTM layer with 7 units, recurrent dropout with 0.5 rate
# Add dense layer as output with 'sigmoid' activation
model2 <- ___

# 2, Comile the model

# 3. Fit the model

```

Teraz twoja kolej, użyj zestawu danych Stack Overflow, aby sklasyfikować każde pytanie w języku programowania:

```{r so_questions}
load("data/stack_overflow.RData")

# Ex. Using knowlege from previous chapters create model for stack overflow question tags classification.
```

