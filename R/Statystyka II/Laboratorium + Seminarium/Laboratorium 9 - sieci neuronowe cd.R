# part III - Stochastic Gradient Descent and Backpropagation

library(tidyverse)
library(gridExtra)

# Zacznijmy od prostej funkcji f (x) = x ^ 2 + 1 i jej pochodnej f'(x) = 2x. Znalezienie minimum
# f(x) jest proste w tej sytuacji: minimalna warto�� jest r�wna 1, gdy x == 0. Zauwa�, �e nasza 
# pochodna f'(x) r�wna si� 0 dok�adnie w tym punkcie (je�li warto�� pochodnej f'(x)jest r�wna 0 
# w punkcie x == x0, funkcja f(x) ma lokalne / globalne minimum / maksimum / punkt zwrotny w x0).
f <- function(x) x^2 + 1
grad_f <- function(x) 2*x
library(dplyr)
sample_data = tibble(
  x = seq(-2, 2, by = 0.05),
  y = f(x),
  grad = grad_f(x)
)
library(ggplot2)
base_plot <- ggplot(sample_data, aes(x, y)) + geom_line(color = "red") + geom_line(aes(y = grad), color = "blue") +
  theme_bw()
base_plot 
# Wniosek, je�li nie mo�emy bezpo�rednio znale�� minimum funkcji f(x), zawsze mo�emy spr�bowa� 
# sprawdzi�, gdzie pochodna f'(x) jest r�wna 0 (a dla wielu rozwi�za� sprawd�, kt�ra z nich jest
# minimum). To �wietnie, ale co zrobi�, gdy nie mo�emy rozwi�za� r�wnania f'(x) == 0? �aden 
# problem, zawsze mo�emy u�y� bardzo prostego algorytmu o nazwie Gradient Descent:
# 1. Zacznij od warto�ci pocz�tkowej parametr�w (x, y, beta, cokolwiek, �)
# 2. Uaktualnij parametry formu�� param_new := param_old - LR * f'(param_old) gdzie LR jest 
# hiperparametrem zwanym learning rate. Wypr�bujmy to dla naszej funkcji. Wypr�buj r�ne 
# warto�ci learning rate, takie jak 0.1, 0.01 i 1
x0 <- 1.345 # Start point
lr <- 0.1 # Learning rate
epochs <- 30 # Nr of epochs (updates)
GD_ordinary_fun <- function(f, grad_f, x0, lr, epochs) {
  x <- x0
  results <- tibble(
    x = x0, y = f(x), grad = grad_f(x)
  )
  for (i in 1:epochs) {
    x <- x - lr * grad_f(x) # GD
    results[i + 1, ] <- list(x, f(x), grad_f(x))
    print(paste("Updated x value:", round(x, 8), ". Updated f(x) value:", round(f(x), 8)))
  }
  plot(base_plot + geom_point(data = results, color = "black"))
  results
}
task1 <- GD_ordinary_fun(f, grad_f, x0, lr, epochs)

# Je�li teraz mo�emy zaimplementowa� Gradient Descent dla prostej funkcji z jednym parametrem, 
# mo�emy spr�bowa� rozwi�za� podstawowy problem uczenia maszynowego - regresj� liniow�.
set.seed(666)
sample_data <- tibble(x = runif(50, -3, 3), y = x + rnorm(50, 3, 1.2))
base_plot <- ggplot(sample_data, aes(x, y)) + geom_point() + theme_bw()
base_plot

# Naszym zadaniem jest znalezienie parametr�w b0 i b1 modelu liniowego y = b0 + b1*x takich 
# aby b��d �redniokwadratowy (mean squared error) by� minimalny. Zanim zaczniemy, sprawd�my 
# rozwi�zanie z R:
lm_model <- lm_model <- lm(y ~ x, sample_data)
summary(lm_model)
mean(lm_model$residuals^2) # MSE from model

# Nasz model liniowy mo�na zapisa� w notacji macierzowej jako y = Xb. Zacznijmy od stworzenia
# macierzy predyktor�w X i wektora przewidywanych warto�ci y z naszych oryginalnych danych:
X <- tibble(x0 = 1, x1 = sample_data$x) %>% as.matrix()
y <- sample_data$y

# Teraz musimy zaimplementowa� MSE:
MSE <- function(beta, X, y) mean((beta%*%t(X) - y)^2)
MSE(c(2.95039, 0.93806), X, y)

# i jego pochodn�:
MSE_grad <- function(beta, X, y) 2*((beta%*%t(X) - y)%*%X)/length(y)

# Szczerze m�wi�c, nie musimy tutaj u�ywa� Gradient Descent, r�wnanie MSE_grad == 0 ma 
# rozwi�zanie numeryczne:
solve(t(X)%*%X)%*%t(X)%*%y

# Ale powiedzmy, �e naprawd� chcemy:
beta00 <- c(5, -0.2) # Start point
lr <- 0.1 # Learning rate
epochs <- 30 # Nr of epochs
GD_linear_regression <- function(beta00, X, y, lr, epochs) {
  beta <- beta00
  results <- tibble(
    b0 = beta00[1], b1 = beta00[2], mse = MSE(beta00, X, y), epoch = 0
  )
  for (i in 1:epochs) {
    beta <- beta - lr * MSE_grad(beta, X, y) # GD
    results[i + 1, ] <- list(beta[1], beta[2], MSE(beta, X, y), i)
    print(paste("Updated b0 value:", round(beta[1], 8),
                "Updated b1 value:", round(beta[2], 8),
                "Updated MSE value:", round(MSE(beta, X, y), 8)))
  }
  # Diagnostic plots
  p1 <- base_plot + geom_abline(intercept = beta00[1], slope = beta00[2], color = "blue") +
    geom_abline(intercept = beta[1], slope = beta[2], color = "red")
  p2 <- ggplot(results, aes(epoch, mse)) + theme_bw() + geom_line(color = "red")
  lin_space <- expand.grid(seq(beta[1] - 2, beta[1] + 2, by = 0.1),
                           seq(beta[2] - 2, beta[2] + 2, by = 0.1)) %>%
    as.data.frame() %>% set_names(c("b0", "b1")) %>% rowwise() %>%
    mutate(mse = MSE(c(b0, b1), X, y))
  p3 <- ggplot(lin_space, aes(b0, b1)) + theme_bw() + geom_raster(aes(fill = mse)) +
    geom_contour(colour = "white", aes(z = mse)) + scale_fill_gradient(low = "blue", high = "red") +
    geom_line(data = results, color = "black", linetype = "dashed") +
    geom_point(data = results, color = "black")
  plot(grid.arrange(p1, p2, p3, layout_matrix = rbind(c(1, 2), c(3, 3))))
  results
}
task2 <- GD_linear_regression(beta00, X, y, lr, epochs)

# Wiemy teraz, czym jest Gradient Descent i jak go u�ywa� do rozwi�zywania problem�w ML. W 
# rzeczywisto�ci cz�sto b�dziemy u�ywa� zaawansowanej wersji gradientu o nazwie Stochastic 
# Gradient Descent lub SGD w skr�cie. Wprowadzimy jedn� ma�� zmian� w naszym algorytmie. 
# Jak pami�tasz, nasz gradient MSE MSE_grad bierze pod uwag� macierz oblicze� X i vecotr y 
# i mno�y je na r�ne sposoby. Za��my na chwil�, �e mamy miliony obserwacji, te mno�enia 
# mog� zaj�� du�o czasu i pami�ci, mo�e by� nawet niemo�liwe. Istnieje proste rozwi�zanie 
# tego problemu. Nasze dane mo�emy podzieli� na tzw. batche. Je�li mamy np. 50 obserwacji, 
# mo�emy je podzieli� na 5 batchy - po 10 obserwacji ka�da. Po tych 5 batchach SGD zobaczy 
# wszystkie obserwacje, kt�re mieli�my - minie pierwsza epoka i proces rozpocznie si� od 
# pocz�tku. Mo�na o tym my�le� jako o dodatkowej p�tli nad partiami wewn�trz p�tli epok z 
# implementacji GD.
beta00 <- c(5, -0.2) # Start point
lr <- 0.1 # Learning rate
epochs <- 50 # Nr of epochs
batch_size <- 3 # Batch size
SGD_linear_regression <- function(beta00, X, y, lr, epochs, batch_size) {
  beta <- beta00
  results <- tibble(
    b0 = beta00[1], b1 = beta00[2], mse = MSE(beta00, X, y), epoch = 0
  )
  batches_per_epoch <- ceiling(length(y) / batch_size)
  for (i in 1:epochs) {
    for (b in 1:batches_per_epoch) {
      indexes <- ((b - 1) * batch_size + 1):min((b * batch_size), length(y))
      X_b <- X[indexes, , drop = FALSE]
      y_b <- y[indexes]
      beta <- beta - lr * MSE_grad(beta, X_b, y_b) # SGD
      results <- rbind(results, c(beta[1], beta[2], MSE(beta, X, y), i + b / batches_per_epoch))
    }
    print(paste("Updated b0 value:", round(beta[1], 8),
                "Updated b1 value:", round(beta[2], 8),
                "Updated MSE value:", round(MSE(beta, X, y), 8)))
  }
  # Diagnostic plots
  p1 <- base_plot + geom_abline(intercept = beta00[1], slope = beta00[2], color = "blue") +
    geom_abline(intercept = beta[1], slope = beta[2], color = "red")
  p2 <- ggplot(results, aes(epoch, mse)) + theme_bw() + geom_line(color = "red")
  lin_space <- expand.grid(seq(beta[1] - 2, beta[1] + 2, by = 0.1),
                           seq(beta[2] - 2, beta[2] + 2, by = 0.1)) %>%
    as.data.frame() %>% set_names(c("b0", "b1")) %>% rowwise() %>%
    mutate(mse = MSE(c(b0, b1), X, y))
  p3 <- ggplot(lin_space, aes(b0, b1)) + theme_bw() + geom_raster(aes(fill = mse)) +
    geom_contour(colour = "white", aes(z = mse)) + scale_fill_gradient(low = "blue", high = "red") +
    geom_line(data = results, color = "black", linetype = "dashed") +
    geom_point(data = results, color = "black")
  plot(grid.arrange(p1, p2, p3, layout_matrix = rbind(c(1, 2), c(3, 3))))
  results
}
task3 <- SGD_linear_regression(beta00, X, y, lr, epochs, batch_size)

# W przypadku SGD obliczenia potrzebne do aktualizacji parametr�w s� szybsze ni� w GD, ale 
# SGD mo�e potrzebowa� wi�cej krok�w ni� GD, aby zminimalizowa� funkcj�. Jest jeszcze jedna 
# wa�na przewaga SGD nad GD - SGD mo�e �wydosta� si� z lokalnego minimum.
# Kolejnym krokiem w zrozumieniu procesu optymalizacji b�dzie implementacja SGD dla regresji
# logistycznej. W przypadku regresji logistycznej nie ma rozwi�zania nuerycznego, wi�c musimy 
# u�y� jakiego� algorytmu
sample_data <- readRDS("C:/Users/Patryk/Desktop/spirals.RDS")
base_plot <- ggplot(sample_data, aes(x, y, color = as.factor(class))) + geom_point() + theme_bw()
base_plot

# Jak zawsze mo�emy sprawdzi� rozwi�zanie w R. R u�ywa algorytmu scoringu Fishera lub 
# algorytmu scoringu Newtona - algorytmy te wykorzystuj� nie tylko pierwsz�, ale tak�e drug�
# pochodn� do aktualizacji parametr�w. Korzystanie z drugiej pochodnej ma zalety, ale 
# obliczenia s� naprawd� czasoch�onne i poch�aniaj� pami��.
logistic_model <- glm(class ~ x + y, sample_data, family = "binomial")
summary(logistic_model)

# Podobnie jak w przypadku regresji liniowej utworzymy macierz predykcji i wektor 
# przewidywanych warto�ci:
X <- tibble(x0 = 1, x1 = sample_data$x, x2 = sample_data$y) %>% as.matrix()
y <- sample_data$class

# Nast�pnym krokiem jest zaimplementowanie funkcji sigmoid u�ywanej w regresji logistycznej:
sigmoid <- function(x) 1 / (1 + exp(-x))

# i jej pochodnej:
sigmoid_grad <- function(x) sigmoid(x) * (1 - sigmoid(x))

# W przypadku klasyfikacji binarnej nasz� funkcj� straty b�dzie binary crossentropy:
binary_crossentropy <- function(beta, X, y) {
  z <- sigmoid(beta%*%t(X))
  -mean(y * log(z) + (1 - y) * log(1 - z))
}

# Potrzebujemy r�wnie� gradientu. Tutaj zastosujemy tak zwan� regu�� �a�cuchow� dla
# pochodnych:
binary_crossentropy_grad <- function(beta, X, y) {
  z <- sigmoid(beta%*%t(X))
  dL <- (-y / z - (1 - y) / (z - 1)) / length(y)
  dV <- sigmoid_grad(beta%*%t(X))
  dx <- X
  (dL * dV) %*% dx
}

# Teraz mamy wszystko do zaimplementowania SGD do regresji logistycznej:
beta00 <- c(0.3, 1, -0.2) # Start point
lr <- 0.1 # Learning rate
epochs <- 50 # Nr of epochs
batch_size <- 20 # Batch size
SGD_logistic_regression <- function(beta00, X, y, lr, epochs, batch_size) {
  beta <- beta00
  results <- tibble(
    b0 = beta00[1], b1 = beta00[2], b2 = beta00[3], log_loss = binary_crossentropy(beta00, X, y), epoch = 0
  )
  batches_per_epoch <- ceiling(length(y) / batch_size)
  for (i in 1:epochs) {
    for (b in 1:batches_per_epoch) {
      indexes <- ((b - 1) * batch_size + 1):min((b * batch_size), length(y))
      X_b <- X[indexes, , drop = FALSE]
      y_b <- y[indexes]
      beta <- beta - lr * binary_crossentropy_grad(beta, X_b, y_b) # SGD
      results <- rbind(results, c(beta[1], beta[2], beta[3], binary_crossentropy(beta, X, y), i + b / batches_per_epoch))
    }
    print(paste("Updated b0 value:", round(beta[1], 8),
                "Updated b1 value:", round(beta[2], 8),
                "Updated b2 value:", round(beta[3], 8),
                "Updated LogLoss value:", round(binary_crossentropy(beta, X, y), 8)))
  }
  # Diagnostic plots
  p1 <- ggplot(results, aes(epoch, log_loss)) + theme_bw() + geom_line(color = "red")
  lin_space <- expand.grid(seq(-6, 6, by = 0.1), seq(-6, 6, by = 0.1)) %>%
    as.data.frame() %>% set_names(c("x", "y")) %>% rowwise() %>%
    mutate(proba = sigmoid(beta%*%c(1, x, y)),
           class = ifelse(proba > 0.5, 1, 0))
  p2 <- base_plot + geom_point(data = lin_space, alpha = 0.1)
  plot(grid.arrange(p1, p2, ncol = 2))
  results
}
task4 <- SGD_logistic_regression(beta00, X, y, lr, epochs, batch_size)

# Naszym ostatnim zadaniem jest zaimplementowanie podstawowego perceptronu jednowarstwowego 
# dla tego samego zadania klasyfikacyjnego. R�nica polega na tym, �e b�dziemy musieli 
# zaktualizowa� wagi dla ka�dej ukrytej warstwy sieci neuronowej za pomoc� regu�y �a�cuchowej 
# dla pochodnych, jak w przyk�adzie regresji logistycznej. Wagi n-tej warstwy s� zale�ne od 
# wag n-1 poprzednich warstw. Ta wersja SGD w sieciach neuronowych nosi nazw� algorytmu 
# wstecznej propagacji.
# Zaczniemy od implementacji forward step, czyli wyliczenia outputu sieci dla danego zestawu
# wag:
forward_propagation <- function(X, w1, w2) {
  # Linear combination of inputs and weights
  z1 <- X %*% w1
  # Activation function - sigmoid
  h <- sigmoid(z1)
  # Linear combination of 1-layer hidden units and weights
  z2 <- cbind(1, h) %*% w2
  # Output
  list(output = sigmoid(z2), h = h)
}

# Teraz czas na wsteczn� propagacj�. Dla uproszczenia u�yjemy MSE jako b��du:
backward_propagation <- function(X, y, y_hat, w1, w2, h, lr) {
  # w2 gradient
  dw2 <- t(cbind(1, h)) %*% (y_hat - y)
  # h gradient
  dh  <- (y_hat - y) %*% t(w2[-1, , drop = FALSE])
  # w1 gradient
  dw1 <- t(X) %*% ((h * (1 - h) * dh))
  # SGD
  w1 <- w1 - lr * dw1
  w2 <- w2 - lr * dw2
  list(w1 = w1, w2 = w2)
}

# ��cz�c wszystko razem:
hidden_units <- 5
set.seed(666)
w1 <- matrix(rnorm(3 * hidden_units), 3, hidden_units)
w2 <- as.matrix(rnorm(hidden_units + 1))
lr <- 0.1 # Learning rate
epochs <- 50 # Nr of epochs
batch_size <- 20 # Batch size
SGD_single_layer_perceptron <- function(w100, w200, X, y, lr, epochs, batch_size) {
  w1 <- w100
  w2 <- w200
  results <- tibble(
    mse = mean((forward_propagation(X, w1, w2)$output - y)^2), epoch = 0
  )
  batches_per_epoch <- ceiling(length(y) / batch_size)
  for (i in 1:epochs) {
    for (b in 1:batches_per_epoch) {
      indexes <- ((b - 1) * batch_size + 1):min((b * batch_size), length(y))
      X_b <- X[indexes, , drop = FALSE]
      y_b <- y[indexes]
      ff <- forward_propagation(X_b, w1, w2)
      bp <- backward_propagation(X_b, y_b,
                                 y_hat = ff$output,
                                 w1, w2,
                                 h = ff$h,
                                 lr = lr)
      w1 <- bp$w1
      w2 <- bp$w2
      results <- rbind(results, c(mean((forward_propagation(X, w1, w2)$output - y)^2), i + b / batches_per_epoch))
    }
    print(paste("Updated MSE value:", round(mean((forward_propagation(X, w1, w2)$output - y)^2), 8)))
  }
  # Diagnostic plots
  p1 <- ggplot(results, aes(epoch, mse)) + theme_bw() + geom_line(color = "red")
  lin_space <- expand.grid(seq(-6, 6, by = 0.1), seq(-6, 6, by = 0.1)) %>%
    as.data.frame() %>% set_names(c("x", "y")) %>% rowwise() %>%
    mutate(proba = forward_propagation(c(1, x, y), w1, w2)$output,
           class = ifelse(proba > 0.5, 1, 0))
  p2 <- base_plot + geom_point(data = lin_space, alpha = 0.1)
  plot(grid.arrange(p1, p2, ncol = 2))
  results
}
task5 <- SGD_single_layer_perceptron(w1, w2, X, y, lr, epochs, batch_size)


# Sieci konwolucyjne:
library(keras)
library(tidyverse)
library(grid)
library(gridExtra)
# Zanim zbudujemy CNN w Keras, musimy zrozumie� konwolucj� i pooling. Za�aduj obraz 
# �data/zebra.jpg�
tensorflow::install_tensorflow(extra_packages='pillow')
zebra <- image_load("C:/Users/Patryk/Desktop/zebra.jpg", grayscale = TRUE, target_size = c(200, 300)) %>%
  image_to_array() %>% `/`(255)
image(t(zebra[, , 1])[, nrow(zebra):1], col = grey(seq(0, 1, length = 256)), axes = F)

# Teraz mo�emy zaimplementowa� prost� konwolucj�:
sobel_filter_x <- matrix(c(1, 2, 1, 0, 0, 0, -1, -2, -1), 3, 3, byrow = FALSE)
kernel_shape <- 3
padding <- 0
stride <- 1
input_height <- nrow(zebra)
input_width <- ncol(zebra)
activation_map_height <- (input_height + 2 * padding - kernel_shape) / stride + 1
activation_map_width <- (input_width + 2 * padding - kernel_shape) / stride + 1
activation_map <- matrix(0, nrow = activation_map_height, ncol = activation_map_width)

for (w in 1:ncol(activation_map)) {
  for (h in 1:nrow(activation_map)) {
    activation_map[h, w] <- sum(sobel_filter_x * zebra[h:(h + kernel_shape - 1), w:(w + kernel_shape - 1), 1])
  }
}
image(t(activation_map)[, nrow(activation_map):1], col = grey(seq(0, 1, length = 256)), axes = F)

# oraz pooling:
pool_shape <- 2
pool_stride <- 2
activation_map2_height <- activation_map_height / pool_shape
activation_map2_width <- activation_map_width / pool_shape
activation_map2 <- matrix(0, nrow = activation_map2_height, ncol = activation_map2_width)

for (w in 1:ncol(activation_map2)) {
  for (h in 1:nrow(activation_map2)) {
    activation_map2[h, w] <- max(activation_map[(2 * (h - 1) + 1):((2 * (h - 1) + 1) + pool_shape - 1), (2 * (w - 1) + 1):((2 * (w - 1) + 1) + pool_shape - 1)])
  }
}

image(t(activation_map2)[, nrow(activation_map2):1], col = grey(seq(0, 1, length = 256)), axes = F)

# Zaczniemy od zbudowania prostego CNN dla zbioru danych dotycz�cych mody. Na pierwszym 
# spotkaniu stworzyli�my MLP do tego zadania. Za�adujmy zbi�r danych:
load("C:/Users/Patryk/Desktop/fashion_mnist.RData")
xy_axis <- data.frame(x = expand.grid(1:28, 28:1)[, 1],
                      y = expand.grid(1:28, 28:1)[, 2])
plot_theme <- list(
  raster = geom_raster(hjust = 0, vjust = 0),
  gradient_fill = scale_fill_gradient(low = "white", high = "black", guide = FALSE),
  theme = theme_void()
)

sample_plots <- sample(1:nrow(fashion_mnist_train_X), 100) %>% map(~ {
  plot_data <- cbind(xy_axis, fill = data.frame(fill = fashion_mnist_train_X[.x, ]))
  ggplot(plot_data, aes(x, y, fill = fill)) + plot_theme
})

do.call("grid.arrange", c(sample_plots, ncol = 10, nrow = 10))

# Aby przes�a� dane do modelu CNN w keras, musimy przekszta�ci� nasze je w odpowiednie 
# tensory. Podobnie jak w przypadku MLP musimy przekszta�ci� wektor etykiet do macierzy z 
# kodowaniem one-hot-encoding. W przypadku naszych obraz�w musimy przedstawi� je jako 
# tensor 4-wymiarowy (pr�bki, wysoko��, szeroko��, kana�y). Musimy r�wnie� pami�ta� o 
# znormalizowaniu warto�ci pikseli:
fashion_mnist_train_Y <- fashion_mnist_train_Y %>% to_categorical(., 10)
fashion_mnist_test_Y <- fashion_mnist_test_Y %>% to_categorical(., 10)

fashion_mnist_train_X <- fashion_mnist_train_X / 255
fashion_mnist_test_X <- fashion_mnist_test_X / 255

fashion_mnist_train_X <- array_reshape(fashion_mnist_train_X, c(nrow(fashion_mnist_train_X), 28, 28, 1))
fashion_mnist_test_X <- array_reshape(fashion_mnist_test_X, c(nrow(fashion_mnist_test_X), 28, 28, 1))

dim(fashion_mnist_train_X)

# Dane s� w poprawnej formie tensorowej, mo�emy przyst�pi� do budowy modelu. Jak zawsze 
# b�dzie to model sekwencyjny. Jako pierwsz� warstw� u�yjemy warstwy konwolucyjnej:
fmnist_model1 <- keras_model_sequential() %>%
  # 2D convolution, 32 filters of size 3x3, input c(28, 28, 1) - grayscale
  layer_conv_2d(filters = 32, kernel_size = c(3, 3), activation = 'relu',
                input_shape = c(28, 28, 1))
fmnist_model1

# Dlaczego mamy do wytrenowania 320 parametr�w?
32 * (3 * 3 * 1 + 1) # 32 filters of size 3x3(x1) + bias for each of them

# Dlaczego output ma kszta�t (None, 26, 26, 32) ?
((28 - 3 + 2 * 0) / 1) + 1 # 28 - input image size, 3 - kernsl size, 0 - padding, 1 - stride

# Po warstwie konwolucyjnej mo�emy doda� kolejn�. U�yjmy warstwy max pooling:
fmnist_model1 %>%
  # 2D max pooling size 2x2(x1)
  layer_max_pooling_2d(pool_size = c(2, 2))
fmnist_model1

# Dlaczego output ma kszta�t (None, 12, 12, 32) ?
26 / 2 # 26 - input of the activation map shape, 2 - pool size

# Powiedzmy, �e chcemy doko�czy� nasz� architektur� i doda� warstw� wyj�ciow�. zrobimy to w 
# taki sam spos�b jak w MLP, ale zanim to zrobimy, musimy sp�aszczy� nasz� ostatni� map� 
# aktywacji do wektora:
fmnist_model1 %>%
  # Tensor flattening into vector form
  layer_flatten() %>%
  # Output layer - 10 classes, softmax activation
  layer_dense(units = 10, activation = 'softmax')

fmnist_model1

# Dlaczego output ma kszta�t (None, 5408) ?
13 * 13 * 32 # Check dimmentions of previous layer

# Dlaczego mamy 54090 parametr�w do trenowania w warstwie wyj�ciowej?
5408 * 10 + 10 # 5408 - from layer_flatten * 10 neurons + biases

# Architertura CNN jest uko�czona, mo�emy teraz skompilowa� model:
fmnist_model1 %>% compile(
  loss = "categorical_crossentropy",
  optimizer = optimizer_adadelta(),
  metrics = c('accuracy')
)

# i go wytrenowa�:
history <- fmnist_model1 %>% fit(
  fashion_mnist_train_X,
  fashion_mnist_train_Y,
  batch_size = 128,
  epochs = 30,
  validation_split = 0.2,
  callbacks = c(callback_model_checkpoint(monitor = "val_accuracy",
                                          filepath = "models/fmnist_model1.hdf5",
                                          save_best_only = TRUE))
) 
# w MLP accuracy by�o ~0.8, a tu 0.9 

# oraz zewaluowa� na zbiorze testowym:
fmnist_model1 %>% evaluate(fashion_mnist_test_X, fashion_mnist_test_Y)

# Czas na stworzenie bardziej zaawansowanej wersji tego modelu:
# Ex. Expand model by adding batch normalization. Add early stopping and Tensorboard callbacks.
# 1. Model architecture:
# 2D convolution with 64 filters of size 3x3, 1x1 stride, 'linear' activation, "same" padding
# Batch normalization layer
# "relu" activation layer
# 2D max pooling size 2x2, 2x2 stride
# dropout layer with 25% drop rate
# Flattening layer
# dense layer with 512 neurons and "relu" activation
# dropout layer with 25% drop rate
# Choose correct layer as output
# 2. Compile model with Adadelta optimizer - set learning rate 0.01, decay = 1e-6.
# 3. Fit the model - beside standart settings add callbacks:
# model checkpoint - save model as "fmnist_model2.hdf5" in "models" folder
# early stopping - will stop training if there's no progress (monitor "val_accuracy" and don't wait more than 5 epochs)
# tensorboard - save logs to tensorboard in "tensorboard" folder - callback_tensorboard
# 4. Evaluate the model on test set
fmnist_model2 <- keras_model_sequential() %>%
  layer_conv_2d(
    filter = 64, kernel_size = c(3, 3), padding = "same",
    input_shape = c(28, 28, 1), activation = "linear") %>%
  layer_batch_normalization() %>%
  layer_activation("relu") %>%
  layer_max_pooling_2d(pool_size = c(2, 2), strides = c(2, 2)) %>%
  layer_dropout(0.25) %>%
  layer_flatten() %>%
  layer_dense(512, activation = "relu") %>%
  layer_dropout(0.25) %>%
  layer_dense(10, activation = "softmax")

fmnist_model2 %>% compile(
  loss = "categorical_crossentropy",
  optimizer = optimizer_adadelta(lr = 0.01, decay = 1e-6),
  metrics = "accuracy"
)
if (!dir.exists("tensorboard")) dir.create("tensorboard")
history <- fmnist_model2 %>% fit(
  fashion_mnist_train_X,
  fashion_mnist_train_Y,
  batch_size = 128,
  epochs = 10,
  validation_split = 0.2,
  callbacks = c(callback_model_checkpoint(monitor = "val_accuracy",
                                          filepath = "models/fmnist_model2.hdf5",
                                          save_best_only = TRUE),
                callback_early_stopping(monitor = "val_accuracy", patience = 5),
                callback_tensorboard(log_dir = "tensorboard"))
)
tensorboard("tensorboard")
fmnist_model2 %>% evaluate(fashion_mnist_test_X, fashion_mnist_test_Y)