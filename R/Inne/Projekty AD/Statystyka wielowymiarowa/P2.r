
library("ggplot2")
library("ClusterR")
library("rgl")

#zadanie 1.1
wiek = c(18, 21, 22, 24, 26, 26, 27, 30, 31, 35, 39, 40, 41, 42, 44, 46, 47, 48, 49, 54)
wydatki = c(10, 11, 22, 15, 12, 13, 14, 33, 39, 37, 44, 27, 29, 20, 28, 21, 30, 31, 23, 24)

dane = data.frame(wiek, wydatki)

qplot(wiek, wydatki) + 
  geom_point(size=2) + 
  ylab("Wydatki") + 
  xlab("Wiek") +
  scale_x_continuous(breaks=seq(0,55,by = 5)) +
  scale_y_continuous(breaks=seq(0,45,by = 5)) +
  theme_classic() +
  theme(panel.background = element_rect(colour = "black"))

#Na wykresie mo�emy zauwa�y�, �e dane uk�adaj� si� w 3 grupy: do 29 lat, miedzy 30 a 39 oraz 40 i wi�cej.

#zadanie 1.2
klasteryzacja = kmeans(dane, 3)

centroidy = data.frame(klasteryzacja$centers)
grupa = klasteryzacja$cluster
dane = cbind(dane, grupa)

#zadanie 1.3
#Na podstawie wykresu z zadania 1.1 mo�emy zauwa�y�, �e dane wyra�nie uk�adaj� si� w 3 grupy i dlatego wybrali�my k = 3.

#zadanie 1.4
Grupy = as.factor(grupa)

qplot() +
  geom_point(dane, mapping = aes(x=wiek, y=wydatki, color=Grupy) ,size = 2) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
  geom_point(centroidy, mapping = aes(x = wiek, y = wydatki), size = 2, colour = "black")+
  ylab("Wydatki") + 
  xlab("Wiek") +
  scale_fill_manual("legend_title",values=c("orange","red"))+
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )


#zadanie 1.5
#wspolrzedne centroidow
centroidy

#zmiennosc wewnatrz grupowa
klasteryzacja$withinss

#calkowita zmiennosc
klasteryzacja$totss

#zadanie 2.1
dane <- read.csv("C:\\Users\\Szymon Tokarski\\Desktop\\dane_zad2.csv", sep = ';', dec = ',')
plot(dane$V1, dane$V2, pch = 16)

#Na wykresie mo�emy zauwa�y�, �e dane grupuj� si� w 2 zbiory. Jeden to ko�o o �rodku w punkcie (0,0) a drugi to pier�cie� do ko�a tego ko�a o tym samym �rodku.

#zadanie 2.2
klasteryzacja = kmeans(dane, 2)
centroidy = data.frame(klasteryzacja$centers)

Grupy = as.factor(klasteryzacja$cluster)

qplot() +
  geom_point(dane, mapping = aes(x=V1, y=V2, color=Grupy) ,size = 2) +
  scale_color_manual(values = c("#00AFBB", "#E7B800"))+
  geom_point(centroidy, mapping = aes(x = V1, y = V2), size = 2, color = "red")+
  ylab("Wydatki") + 
  xlab("Wiek") +
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )

#zadanie 2.3
#Wynik grupowania jest niepoprawny, poniewa� metoda k-�rednich bazuje na odleg�o�ci euklidesowej i przy takiej reprezentacji danych np. odleg�o�ci miedzy przeciwleg�ymi punktami w 2 grupie jest wi�ksza ni� miedzy jednym z tych punkt�w a punktem le��cym w 1 grupie.

#zadanie 2.4
r = sqrt(dane$V1^2 + dane$V2^2)

oblicz_fi <- function(x,y)
{
  if(x > 0 && y >= 0)
  {
    return(atan(y/x))
  }
  else if(x > 0 && y < 0)
  {
    return(atan(y/x) + 2 * pi)
  }
  else if(x < 0)
  {
    return(atan(y/x) + pi)
  }
  else if(x == 0 && y > 0)
  {
    return(pi/2)
  }
  else
  {
    return(3 * pi / 2)
  }
}


fi = c()
for(i in 1:dim(dane)[1])
{
  fi[i] = oblicz_fi(dane$V1[i], dane$V2[i])
}

klasteryzacja = kmeans(data.frame(r, fi), 2)
centroidy = data.frame(klasteryzacja$centers)

centroidy[1,] = c(centroidy[1,1] * cos(centroidy[1,2]), centroidy[1,1] * sin(centroidy[1,2]))
centroidy[2,] = c(centroidy[2,1] * cos(centroidy[2,2]), centroidy[2,1] * sin(centroidy[2,2]))

Grupy = as.factor(klasteryzacja$cluster)

#zadanie 2.5
qplot() +
  geom_point(dane, mapping = aes(x=V1, y=V2, color=Grupy) ,size = 2) +
  geom_point(centroidy, mapping = aes(x = r, y = fi), size = 2, color = "red")+
  ylab("Wydatki") + 
  xlab("Wiek") +
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )

#zadanie 3.1
data(wine,package="rattle")
dane = data.frame(scale(wine[13:14]))

#zadanie 3.2

tot.withinss = c()
for(i in 1:10)
{
  k = kmeans(as.matrix(dane), i)
  tot.withinss[i] = k$tot.withinss
}


qplot(c(1:10), tot.withinss) + 
  geom_line(color = "blue", linetype = 2) +
  geom_text(aes(label=round(tot.withinss,2)),size = 4, hjust = 0.4, vjust = -0.5)+
  ylab("wyjasniana wariancja") + 
  xlab("klastry") +
  scale_x_continuous(breaks=seq(0,10,by = 1)) +
  scale_y_continuous(breaks=seq(min(tot.withinss),max(tot.withinss),length.out = 10)) +
  theme_bw()


Optimal_Clusters_KMeans(as.matrix(dane), 10, criterion = "WCSSE")

#zadanie 3.3
#Optymalna liczba klastr�w to taka po kt�rej nie nast�puje gwa�towny spadek warto�ci wariancji. W naszym przypadku K powinno wynosi� 3.


#zadanie 3.4

klasteryzacja = kmeans(dane, 3)
centroidy = data.frame(klasteryzacja$centers)

Grupy = as.factor(klasteryzacja$cluster)


qplot() +
  geom_point(dane, mapping = aes(x=Dilution, y=Proline, color=Grupy) ,size = 2) +
  ylab("Wydatki") + 
  xlab("Wiek") +
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )

#zadanie 4.1
data(mtcars)

#W tej podstacji zbi�r nie nadaje si� do klasteryzacji, poniewa� zawiera zmienne dyskretne. Je�eli chcieliby�my analizowa� zbi�r w takiej podstacji to powinni�my skorzysta� np. z algorytmu k-prototyp�w.

#zadanie 4.2
dane = mtcars[, c(1,3,4,5,6,7)]
dane = scale(dane)


#Wybrali�my zmienne; 1,3,4,5,6,7 , poniewa� s� to zmienne numeryczne, poniewa� tylko takie zmienne powinny by� uwzgl�dniane w metodzie k-�rednich.

#zadanie 4.3
Optimal_Clusters_KMeans(dane, 10)
klasteryzacja = kmeans(dane, 3)

Grupy = as.factor(klasteryzacja$cluster)

qplot() +
  geom_point(aes(x=klasteryzacja$cluster, y=row.names(dane), color=Grupy) ,size = 2) +
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.85, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )

#zadanie 4.4

#centroidy
klasteryzacja$centers

#zmiennosc wewnatrz grupowa
klasteryzacja$withinss

#przynaleznosc objektow do grup
klasteryzacja$cluster

#zadanie 4.5
qplot() +
  geom_point(aes(x=dane[,1], y=dane[,2], color=Grupy) ,size = 2) +
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )


#zadanie 5.1
data(ais, package="DAAG")

data = ais[1:11]

#zadanie 5.2
Y = prcomp(data, scale = FALSE, center = FALSE)

plot(Y$x[,1], Y$x[,2], col=ais$sex, pch = 16)
plot(Y$x[,1], Y$x[,3], col=ais$sex, pch = 16)
plot(Y$x[,2], Y$x[,3], col=ais$sex, pch = 16)

plot3d(Y$x[,1],Y$x[,2],Y$x[,3], col=as.integer(ais$sex))


#Tak, dostrzegam lekkie grupowanie si� danych miedzy zmiennymi 1 i 2 (rys. 13) oraz 1 i 3. Mo�emy zauwa�y�, ze te dane dziel� si� na 2 grupy (na m�czyzn i kobiety), jednak�e ten podzia� nie jest zbyt wyra�ny.

#zadanie 5.3
Y = prcomp(data, scale = TRUE, center = TRUE)

plot(Y$x[,1], Y$x[,2], col=ais$sex, pch = 16)
plot(Y$x[,1], Y$x[,3], col=ais$sex, pch = 16)
plot(Y$x[,2], Y$x[,3], col=ais$sex, pch = 16)

plot3d(Y$x[,1],Y$x[,2],Y$x[,3], col=as.integer(ais$sex))
rgl.snapshot('C:\\Users\\Szymon Tokarski\\Desktop\\zadanie53_4.png', fmt = 'png')

#Wykonanie analizy PCA na przeskalowanych danych powoduj�, �e dane s� lepiej pogrupowane zw�aszcza na rys. 16 i rys. 17. Mo�emy teraz zauwa�y� znacznie wyra�niejszy podzia� pomi�dzy kobietami a m�czyznami. 

#zadanie 5.4
klasteryzacja = kmeans(Y$x[, 1:2], 2)
Grupy = as.factor(klasteryzacja$cluster)
Plec = ais$sex

levels(Plec) = c("K", "M")

#zadanie 5.5
qplot() +
  geom_point(aes(x=Y$x[,1], y=Y$x[,2], color = Grupy, shape = Plec) ,size = 2) +
  ylab("PC1") + 
  xlab("PC2") +
  theme_classic() +
  theme(panel.background = element_rect(color = "black"),
        legend.position = c(.95, .95),
        legend.justification = c("right", "top"),
        legend.box.just = "right",
        legend.margin = margin(6, 6, 6, 6),
        legend.box.background = element_rect(color="black", size=1),
        legend.box.margin = margin(6, 6, 6, 6),
        legend.title = element_text()
  )

#Na podstawie powy�szego wykresu mo�emy stwierdzi�, �e otrzymane grupowanie dobrze wyja�nia zmienna sex pomimo paru b��d�w. Dostrzegam 8 z�ych dopasowa�, po 4 w ka�dej grupie.