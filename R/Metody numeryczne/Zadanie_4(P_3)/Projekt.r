# Funkcja rozwi�zuje uk�ad r�wna� liniowych z macierz� tr�jprzek�tniow�
RozwiazMacierz <- function(macierz)
{
  n = length(macierz[,1])
  for(j in 1 : n)
  {
    for(i in 1 : n)
    {
      if(i != j)
      { 
        if(macierz[j,j] < 0.00000000000001)
          return(NULL)
        
        m = macierz[i,j] / macierz[j,j]
        
        for(k in 1 : (n + 1))
        {
          macierz[i,k] = macierz[i,k] - m*macierz[j,k]
        }
      }
    }
  }
  wynik <- c()
  for(i in 1 : n)
  {
    wynik[i] = macierz[i,n + 1]/macierz[i,i]
  }
  return(wynik)
}


# Funkcja wybiera odcinek nale��cy do dziedziny funkcji
ZnajdzPodzial <- function(t, x)
{
  if(t[length(t)] == x)
    return(length(t) - 1)
  for(i in 1 : (length(t) - 1))
  {
    if(t[i] <= x && x < t[i+1])
      return(i)
  }
  return(NULL)
}


# Funkcja s�u�y do wyznaczenia pochodnych
Pochodne <- function(t, y)
{
  h <- c()
  b <- c()
  for(i in 1 : (length(t) - 1))
  {
    h[i] = t[i+1]-t[i]
    b[i] = (y[i+1]-y[i]) * 6/h[i]
  }
  u <- c()
  v <- c()
  for(i in 2 : (length(t) - 1))
  {
    u[i-1] = 2*(h[i-1] + h[i])
    v[i-1] = b[i] - b[i-1]
  }
  wymiar = length(t) - 2
  macierz = matrix(1:(wymiar*(wymiar + 1)), nrow = wymiar, ncol = (wymiar + 1))
  macierz[,(wymiar+1)] = v
  for(i in 1 : wymiar)
  {
    for(j in 1 : wymiar)
    {
      if(i == j)
        macierz[i,j] = u[i]
      else if(i == j - 1 || i - 1 == j)
        macierz[i,j] = h[i]
      else
        macierz[i,j] = 0
    }
  }
  wynik = c(0,RozwiazMacierz(macierz),0)
  return(wynik)
}


# Funkcja daje funkcj� sklejan�
Rozwiazanie <- function(t,y,z ,x)
{
  i = ZnajdzPodzial(t,x)
  h = t[i+1] - t[i]
  return(z[i]/6 * ((t[i+1]-x)^3/h - h*(t[i+1] -x)) - z[i+1]/6 * (((t[i] - x)^3)/h - h*(t[i] - x)) + (y[i]*(t[i+1] - x) - y[i+1]*(t[i]-x))/h)
}


# Test
library(ggplot2)
# Przyk�adowa funkcja, z kt�r� b�dziemy por�wnywa� wyniki
Test <- function(x)
{
  return(x^3 - x - 1)
}


# Generujemy punkty za pomoc� kt�rych utworzymy przedzia�y
Podzial = seq(-5,5,2)


# Liczymy warto�� przyk�adowej funkcji w tych punktach
Wartosc = Test(Podzial)


# Genetujemy punkty na podstawie kt�rych b�dziemy przewidywali ich warto��
Punkty = seq(-5, 5,0.2)


# Obliczamy pochodne potrzebne do algorytmu
z = Pochodne(Podzial, Wartosc)


# Obliczamy warto�ci punkt�w
Poprawne = c()
for(i in 1 : length(Punkty))
{
  Poprawne[i] = Rozwiazanie(Podzial, Wartosc, z, Punkty[i])
}


# Rysujemy wykres por�wnawczy
ggplot() + stat_function(aes(Podzial) ,fun = Test) + geom_point(aes(Punkty,Poprawne))

