\documentclass[12pt,a4paper]{article}

\usepackage{mathptmx}
\usepackage{amsmath}
\usepackage{authblk}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{pgfplots}
\usepackage{polski}
\usepackage{comment}

\renewcommand{\familydefault}{\sfdefault}
\usepackage[left=2.5cm, right=2.5cm, top=2.5cm]{geometry}

\title{\huge{\textbf{Analiza ryzyka i bezpiecze�stwa w technice - projekt drugi}}}
\author{
        Szymon Tokarski \\
        Patryk Wirkus\\
       }

\begin{document}
\maketitle
\thispagestyle{empty}
\clearpage 
\pagenumbering{arabic}


\section{Specyfikacja modelu}

\subsection{S�owny opis modelu}
Przedstawiony na poni�szym rysunku uk�ad z�o�ony jest z siedmiu niezale�nych element�w, w tym z jednego wyst�puj�cego dwukrotnie. G��wnym tematem projektu jest remont domu wykonany przez poszczeg�lne ekipy remontowe wykonuj�ce zlecenia. Schemat zawiera sze�� pojedynczych element�w: architekt, murarz, ogrodnik, elektryk, hydraulik i malarz, oraz ekip� sprz�taj�c�, zar�wno wn�trze domu, jak i ogr�d.

\subsection{Schemat}
\begin{figure}[ht!]
\centering
\includegraphics[width=150mm]{Schemat1.png}
\caption{Schemat \label{overflow}}
\end{figure}

\subsection{Opis element�w}
(1)architekt - jego zadaniem jest zaprojektowanie wn�trza domu, a tak�e ogrodu\\
(2)murarz - osoba ta zajmowa� si� murowaniem �cian zewn�trznych domu, przede wszystkim gara�u oraz piwnicy\\
(3)ogrodnik - ma za zadanie doprowadzi� ogr�d do stanu u�ywalno�ci, wzbogacaj�c go o\linebreak r�znego rodzaju ozdoby dekoracyjne\\
(4)elektryk - odpowiedzialny jest za wymian� i monta� zepsutych instalacji elektrycznych w �azience i kuchni \\
(5)hydraulik - g��wnym jego zadaniem jest naprawa instalacji wodoci�gowej i kanalizacyjnej w �azience i toalecie\\
(6)malarz - osoba wynaj�ta do wymalowania pomieszcze� w stanie surowym, a tak�e\linebreak przemalowanie pokoj�w u�ytku codziennego, takich jak salon oraz pokoje dzieci�ce\\
(7)ekipa sprz�taj�ca - grupa ludzi wynaj�ta do prac sprz�taj�cych wn�trze domu i ogr�d, w trakcie pracy pozosta�ych cz�onk�w wynaj�tej ekipy\\

\subsection{Wyb�r rozk�ad�w prawdopodobie�stwa}
(1)architekt: r. jednostajny o parametrach $a = 0$ i $b = 32$ \\
arch = runif(1,0,32) \\
(2)murarz: r. jednostajny o parametrach $a = 10$ i $b = 60$ \\
mur = runif(1,10,60) \\
(3)ogrodnik: r. wyk�adniczy o parametrze $\lambda = 0.04$ \\ 
ogr = rexp(1,0.04) \\
(4)elektryk: r. wyk�adniczy o parametrze $\lambda = 0.025$ \\ 
ogr = rexp(1,0.025) \\
(5)hydraulik: r. gamma o parametrach $k = 1$ i $\theta = 0.05$ \\
hydr = rgamma(1,1,scale=0.05) \\
(6)malarz: r. jednostajny o parametrach $a = 15$ i $b = 45$ \\
mala = runif(1,15,45) \\
(7)ekipa sprz�taj�ca:  r. wyk�adniczy o parametrze $\lambda = 0.008$ \\ 
sprza = rexp(1,0.04) \\ 


\section{�cie�ki i ci�cia}

\subsection{�cie�ki minimalne}
(1,3,7) ; (1,2,4,6,7) ; (1,2,5,6,7)

\subsection{Ci�cia minimalne}
(1) ; (7) ; (2,3) ; (2,7) ; (6,3); (6,7); (7,3)

\section{Symulacja czasu dzia�ania uk�adu}

\subsection{Funkcja struktury}
$\Phi(x) = x1 \land ( (x2 \land (x4 \lor x5) \land x6 \land x7) \lor 
(x3 \land x7) )$
\subsection{Zamieszczenie wynik�w symulacji dla T = 30}
\begin{table}[h]
\begin{tabular}{llll}
N                  & 100              & 1000             & 10000            \\
Prawdopodobie�stwo & 0.02000             & 0.01600            & 0.01600           \\
�rednia            & 12.58413 & 11.75725 & 11.45598 \\
Wariancja          & 60.06041 & 51.21911 & 53.28738
\end{tabular}
\end{table}

\newpage
\subsection{Niezawodno��}
Niezawdono�� teoretyczna:\\
$E(\phi(x))$ = p1p2p4p6p7 + p1p2p5p6p7 + p1p3p7 - p1p2p4p5p6p7 -p1p2p4p6p7p3 - p1p2p5p6p7p3 +p1p2p3p4p5p6p7\\
\\
Symulacje w programie R:\\
NTeoretyczna $\xleftarrow{}$ function(t)\\
\{\\
  p1 = 1 - punif(t,0, 32)\\
  p2 = 1 - punif(t,10,60)\\
  p3 = 1 - pexp(t,0.04)\\
  p4 = 1 - pgeom(t, 0.025)\\
  p5 = 1 - pgamma(t,1,1/20)\\
  p6 = 1 - punif(t,15,45)\\
  p7 = 1 - pexp(t,0.008)\\
 return( p1*p2*p4*p6*p7 + p1*p2*p5*p6*p7 + p1*p3*p7 - p1*p2*p4*p5*p6*p7 - p1*p2*p4*p6*p7*p3 - p1*p2*p5*p6*p7*p3 + p1*p2*p3*p4*p5*p6*p7)\\
 \}

\subsection{Dystrybuanta empiryczna, a teoretyczna \newline (wykres oraz por�wnanie wynik�w)}
Dla ka�dej z przeprowadzonych symulacji wykonano po dwa wyjresy.
Pierwszy z nich\linebreak przedstawia wykres dystrybuanty teoretycznej i empirycznej, na drugim za� widoczna jest\linebreak prosta regresji liniowej. Pokazuje nam ona, czy dystrybuanty s� liniowo zale�ne.
\begin{figure}[ht!]
\centering
\includegraphics[width=70mm]{dystrybuanta-100.pdf}
\caption{Porownanie Dys. Teoretycznej do Empirycznej (n=100)}
\end{figure}
\newpage
\begin{figure}[ht!]
\centering
\includegraphics[width=70mm]{regresja-100.pdf}
\caption{Prosta regresji liniowej (n=100)}
\end{figure}
\begin{figure}[ht!]
\centering
\includegraphics[width=70mm]{dystrybuanta-1000.pdf}
\caption{Porownanie Dys. Teoretycznej do Empirycznej (n=1000)}
\end{figure}
\newpage
\begin{figure}[ht!]
\centering
\includegraphics[width=70mm]{regresja-1000.pdf}
\caption{Prosta regresji liniowej (n=1000)}
\end{figure}
\begin{figure}[ht!]
\centering
\includegraphics[width=70mm]{dystrybuanta-10000.pdf}
\caption{Porownanie Dys. Teoretycznej do Empirycznej (n=10000)}
\end{figure}
\newpage
\begin{figure}[ht!]
\centering
\includegraphics[width=70mm]{regresja-10000.pdf}
\caption{Prosta regresji liniowej (n=10000)}
\end{figure}

\subsection{Por�wnanie wynik�w symulacji z wynikami teoretycznymi\linebreak poprzez zastosowanie odpowiedniego testu statystycznego}

\subsection{Histogram czasu �ycia dla n=100,1000,10000}
\begin{figure}[ht!]
\centering
\includegraphics[width=65mm]{100.pdf}
\end{figure}
\begin{figure}[ht!]
\centering
\includegraphics[width=65mm]{1000.pdf}
\end{figure}
\begin{figure}[ht!]
\centering
\includegraphics[width=65mm]{10000.pdf}
\end{figure}

\newpage
\subsection{Wnioski}
Na podstawie wykres�w przedstawionych w punkcie 3.4 widzimy, �e za ka�dym razem\linebreak dystrybuanty s� zdecydowanie liniowo zale�ne. W szczeg�lno�ci widoczne jest to dla\linebreak coraz wi�kszej warto�ci n. Czyli im wi�ksz� mamy pr�b�, tym bardziej widoczne jest\linebreak zwi�zek pomi�dzy dystrybuant� teoretyczn� a empiryczn�.

\newpage 
\section{Wyliczenia wzgl�dnej istotno�ci strukturalnej\linebreak element�w, czyli miary istotno�ci element�w}
Miary niezawodno�ci istotno�ci element�w informuj� nas, kt�re z element�w uk�adu\linebreak maj� najwi�kszy wp�yw na niezawodono�� uk�adu.\\
Je�eli warto�� miary Birnbauma dla danego elementu jest du�a, w�wczas nast�puje ma�a\linebreak zmiana niezawodno�ci tego w�a�nie elementu skutkuje relatywnie du�� zmian� niezawodno�ci systemu.

\subsection{Teoretycznie}
Struktura $$\xleftarrow{}$$ function(x1,x2,x3,x4,x5,x6,x7)\\
\{
  return(x1*x2*x4*x6*x7 + x1*x2*x5*x6*x7 + x1*x3*x7 - x1*x2*x4*x5*x6*x7 - x1*x2*x4*x6*x7*x3\\ - x1 * x2 *x5 * x6 * x7 * x3 + x1 * x2 * x3* x4*x5 * x6 * x7)\\
\}
Miary $$\xleftarrow{}$$ function()\\
\{\\
  t = 30\\
  x1 = 1 - punif(t,0, 32)\\
  x2 = 1 - punif(t,10,60)\\
  x3 = 1 - pexp(t,0.04)\\
  x4 = 1 - pgeom(t, 0.025)\\
  x5 = 1 - pgamma(t,1,1/20)\\
  x6 = 1 - punif(t,15,45)\\
  x7 = 1 - pexp(t,0.008)\\
  # Potencja� poprawy niezawodno�ciowej\\
  IA1 = Struktura(1,x2,x3,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  IA2 = Struktura(x1,1,x3,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  IA3 = Struktura(x1,x2,1,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  IA4 = Struktura(x1,x2,x3,1,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  IA5 = Struktura(x1,x2,x3,x4,1,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  IA6 = Struktura(x1,x2,x3,x4,x5,1,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  IA7 = Struktura(x1,x2,x3,x4,x5,x6,1) - Struktura(x1,x2,x3,x4,x5,x6,x7)\\
  # Miara Birnbauma\\
  IB1 = Struktura(1,x2,x3,x4,x5,x6,x7) - Struktura(0,x2,x3,x4,x5,x6,x7)\\
  IB2 = Struktura(x1,1,x3,x4,x5,x6,x7) - Struktura(x1,0,x3,x4,x5,x6,x7)\\
  IB3 = Struktura(x1,x2,1,x4,x5,x6,x7) - Struktura(x1,x2,0,x4,x5,x6,x7)\\
  IB4 = Struktura(x1,x2,x3,1,x5,x6,x7) - Struktura(x1,x2,x3,0,x5,x6,x7)\\
  IB5 = Struktura(x1,x2,x3,x4,1,x6,x7) - Struktura(x1,x2,x3,x4,0,x6,x7)\\
  IB6 = Struktura(x1,x2,x3,x4,x5,1,x7) - Struktura(x1,x2,x3,x4,x5,0,x7)\\
  IB7 = Struktura(x1,x2,x3,x4,x5,x6,1) - Struktura(x1,x2,x3,x4,x5,x6,0)\\
  model $\xleftarrow{}$ c("Arch", "Mur", "Ogr", "Ele", "Hydr", "Mal","Spr")\\
  IA{teoret} $\xleftarrow{}$ c(IA1,IA2,IA3,IA4,IA5,IA6,IA7) \\
  IB{teoret} $\xleftarrow{}$ c(IB1,IB2,IB3,IB4,IB5,IB6,IB7)\\
  $\frac{IA}{IB} = IAteoret/IBteoret$ \\
  return(data.frame(model,IAteoret,IBteoret,IAdoIBteo)) \\ 
\}\\
\subsection{Empirycznie}
MiaryEmp$\xleftarrow{}$function()\\
\{\\
  n = 10000\\
  t = 30\\
  \\
  z $\xleftarrow{}$ c()\\
  arch = runif(n,0, 32)\\
  mur = runif(n,10,60)\\
  ogr = rexp(n,0.04)\\
  elek = rexp(n, 0.025)\\
  hydr = rgamma(n,1,1/20)\\
  mala = runif(n,15,45)\\
  sprza = rexp(n,0.008)\\
 \\
  for(i in 1 : n)\\
    z[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
  P = mean(z $>=$ t)\\
  \\
  czas1dziala $\xleftarrow{}$ c()\\
  czas2dziala $\xleftarrow{}$ c()\\
  czas3dziala $\xleftarrow{}$ c()\\
  czas4dziala $\xleftarrow{}$ c()\\
  czas5dziala $\xleftarrow{}$ c()\\
  czas6dziala $\xleftarrow{}$ c()\\
  czas7dziala $\xleftarrow{}$ c()\\
  \\
  czas1niedziala $\xleftarrow{}$ c()\\
  czas2niedziala $\xleftarrow{}$ c()\\
  czas3niedziala $\xleftarrow{}$ c()\\
  czas4niedziala $\xleftarrow{}$ c()\\
  czas5niedziala $\xleftarrow{}$ c()\\
  czas6niedziala $\xleftarrow{}$ c()\\
  czas7niedziala $\xleftarrow{}$ c()\\
  \newpage
  for(i in 1:n)\\
  \{\\
    czas1dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i],\\
    sprza[i]),min(ogr[i],sprza[i])), t)\\
    czas2dziala[i] = min(max(min(t,max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas3dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(t,sprza[i])), arch[i])\\
    czas4dziala[i] = min(max(min(mur[i],max(t,hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas5dziala[i] = min(max(min(mur[i],max(elek[i],t),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas6dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),t, sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas7dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], t),min(ogr[i],t)), arch[i])\\
    \\
    czas1niedziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i],\\
    sprza[i]),min(ogr[i],sprza[i])), 0)\\
    czas2niedziala[i] = min(max(min(0,max(elek[i],hydr[i]),mala[i],\\
    sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas3niedziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i],\\
    sprza[i]),min(0,sprza[i])), arch[i])\\
    czas4niedziala[i] = min(max(min(mur[i],max(0,hydr[i]),mala[i],\\
    sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas5niedziala[i] = min(max(min(mur[i],max(elek[i],0),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas6niedziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),0, sprza[i]),min(ogr[i],sprza[i])), arch[i])\\
    czas7niedziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], 0),min(ogr[i],0)), arch[i])\\
  \}\\
  \\
  P1dziala = mean(czas1dziala $>=$ t)\\
  P2dziala = mean(czas2dziala $>=$ t)\\
  P3dziala = mean(czas3dziala $>=$ t)\\
  P4dziala = mean(czas4dziala $>=$ t)\\
  P5dziala = mean(czas5dziala $>=$ t)\\
  P6dziala = mean(czas6dziala $>=$ t)\\
  P7dziala = mean(czas7dziala $>=$ t)\\
  \\
  P1niedziala $\xleftarrow{}$ mean(czas1niedziala $>=$ t)\\
  P2niedziala $\xleftarrow{}$ mean(czas2niedziala $>=$ t)\\
  P3niedziala $\xleftarrow{}$ mean(czas3niedziala $>=$ t)\\
  P4niedziala $\xleftarrow{}$ mean(czas4niedziala $>=$ t)\\
  P5niedziala $\xleftarrow{}$ mean(czas5niedziala $>=$ t)\\
  P6niedziala $\xleftarrow{}$ mean(czas6niedziala $>=$ t)\\
  P7niedziala $\xleftarrow{}$ mean(czas7niedziala $>=$ t)\\
  \\
  \newpage
  IAemp $\xleftarrow{}$ c(P1dziala - P,P2dziala - P,P3dziala - P,P4dziala - P,P5dziala - P, P6dziala - P,P7dziala - P)\\
  IBemp $\xleftarrow{}$ c(P1dziala - P1niedziala, P2dziala - P2niedziala,P3dziala - P3niedziala,P4dziala - P4niedziala,P5dziala - P5niedziala, P6dziala - P6niedziala,P7dziala - P7niedziala)\\
\\
  model $\xleftarrow{}$ c("Arch", "Mur", "Ogr", "Ele", "Hydr", "Mal","Spr")\\
  IAdoIBemp = IAemp/IBemp \\
  return(data.frame(model,IAemp,IBemp,IAdoIBemp))\\  
\}\\

\subsection{Por�wnanie wynik�w symulacji z wynikami teoretycznymi\linebreak poprzez zastosowanie odpowiedniego testu statystycznego}

\subsubsection{Wyznacz $\frac{I^{A}{i}}{I^{B}{i}}$}

\section{Warto�� oczekiwana i wariancja}
Niezawodno�� teoretyczna \newline
$p1 = 1 - punif(t,0,32)$\\
$p2 = 1 - punif(t,10,60)$\\
$p3 = 1 - pexp(t,0.04)$\\
$p4 = 1 - pexp(t,0.025)$\\
$p5 = 1 - pgamma(t,1,1/20)$\\
$p6 = 1 - punif(t,15,45)$\\
$p7 = 1 - pexp(t,0.008)$\\
$E(\Phi(t)) = p1*p2*p4*p6*p7 + p1*p2*p5*p6*p7 + p1*p3*p7 - p1*p2*p4*p5*p6*p7 - p1*p2*p3*p6*p7$
\subsection{Wyliczenie z definicji warto�ci oczekiwanej oraz wariancji czasu dzia�ania ca�ego uk�adu T (z funkcji niezawodno�ci)}
$ET = 13.34915$\\
$Var(T) = 67.01487$


\newpage
\section{Proces odnowy}
Proces odnowy przeprowadzili�my dla elementu nr. 3 (ogrodnik).\\
Czas naprawy jesto to zmienna losowa runif(1,0, 2)

\subsection{Por�wnanie wynik�w symulacji z odnow�, a wynikami dzia�ania uk�adu bez odnowy dla T = 30}

\begin{table}[h]
\begin{tabular}{lllll}
N                  & 100              & 1000             & 10000            & 10000 (Odnawialny) \\
Pp                 & 0.03             & 0.024            & 0.02030           & 0.0438                     \\
�rednia            & 12.55194         & 13.57879         & 13.24221 & 13.75986                  \\
Wariancja          & 70.85300         & 65.03570        & 67.28493 & 74.6683                  \\
�rednia odnowy     & -                & -                & -                & 0.0389                  
\end{tabular}
\end{table}

\subsection{Wnioski}
Proces odnowy w znacz�cy spos�b wp�ywa na prawdopodobie�stwo �e uk�ad dzia�a co najmniej do czasu T. �wiadczy o tym cho�by �rednia d�ugo�� pracy kt�ra wynosi ponad 26 gdzie wcze�niej nie przekroczy�a 20. Ponadto mo�emy zauwa�y� �e prawie w co drugim przypadku element jest naprawiony co �wiadczy o du�ej awaryjno�ci elementu nr. 3.

\end{document}
