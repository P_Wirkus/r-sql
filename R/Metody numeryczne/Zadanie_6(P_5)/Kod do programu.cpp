#include <vector>
#include <fstream>
#include <functional>
#include <iostream>
#include <future>

struct Wyniki
{
    double t, x, y;
};

void Rownanie(unsigned int n, double k, unsigned int M, const std::function<double(double)>& g, double a, double b, const std::string& url)
{
    std::fstream plik;
    plik.open(url, std::ios::out);
    plik << "\"" << "t" << "\",";
    plik << "\"" << "x" << "\",";
    plik << "\"" << "y" << "\"\n";

    std::vector<Wyniki> wyniki(M * (n + 1));

    double h = 1.0f / static_cast<double>(n + 1);
    double s = k / (h * h);

    std::vector<double> x(n + 1);
    std::vector<double> w(n + 1);
    std::vector<double> v(n + 1);
    v[0] = a;
    v[n] = b;

    for (unsigned int i = 0; i < (n + 1); i++)
    {
        w[i] = g(i * h);
        x[i] = i * h;
        wyniki[i] = { 0, i * h, x[i] };
    }

    double t = 0;
    for (unsigned int j = 1; j < M; j++)
    {
        t = j * k;

        for (unsigned int i = 1; i < n; i++)
            v[i] = s * w[i - 1] + (1 - 2 * s) * w[i] + s * w[i + 1];

        for (unsigned int i = 0; i < (n + 1); i++)
        {
            w[i] = v[i];

            plik << "\"" << t << "\",";
            plik << "\"" << x[i] << "\",";
            plik << "\"" << w[i] << "\"";
            plik << std::endl;
        }
    }

    plik.close();
}

int main()
{

    std::function<double(double)> g1 = [](double x) {return ((x - (x * x)) * expf(x)); };
    std::future<void> p1 = std::async(std::ref(Rownanie), 10, 0.0001, 3000, std::ref(g1), 0, 0, "C:\\Users\\Patryk\\Desktop\\g1.csv");
    
    std::function<float(float)> g2 = [](float x) {return (2 * x - (1 - x)); };
    std::future<void> p2 = std::async(std::ref(Rownanie), 10, 0.0001, 3000, std::ref(g2), 0, 0, "C:\\Users\\Patryk\\Desktop\\g2.csv");

    std::function<float(float)> g3 = [](float x) {return (1); };
    std::future<void> p3 = std::async(std::ref(Rownanie), 10, 0.0001, 3000, std::ref(g3), 0, 0, "C:\\Users\\Patryk\\Desktop\\g3.csv");

    p1.wait();
    p2.wait();
    p3.wait();
}