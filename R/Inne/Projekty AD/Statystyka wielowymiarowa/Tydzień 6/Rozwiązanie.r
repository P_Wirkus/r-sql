
library("mvtnorm")
library("matlib")
library("ggplot2")

#1.1

n = 1000
m = 1000
p = 4

mi = matrix(c(0,0,1,1),4, 1)
sigma2 = matrix(c(1,1,3/4,-1/2,1,2,0,-2/3, 3/4, 0,3,1,-1/2, -2/3,1,4), 4,4)


Lm = rep(1,m)
for(i in 1:m)
{
  proba = rmvnorm(n,mi, sigma2)
  mean = colMeans(proba)
  cov = cov(proba)
  
  lewa = n * t((mean - mi)) %*% inv(cov) %*% (mean - mi)
  Lm[i] = lewa
}


xfit <- seq(min(Lm), max(Lm), length = 80)
yfit <- dchisq(xfit, p)
h <- hist(Lm, breaks = 40, col = 'coral2', freq=FALSE, ylim=c(0,0.2))
lines(xfit,yfit, col="black", lwd=2)

x <- rchisq(m, p)
qqplot(x, Lm)
qqline(x, distribution = function(x) qchisq(x, df = p), col = "red", lwd=2)

#1.2

Mm <- list()
for(i in 1:m)
{
  obs = rmvnorm(n, rep(0, times = 4), sigma2)
  S = cov(obs)
  M = (n-1)*S
  
  Mm[[i]] = M
}


meanMm = Mm[[1]]
for(i in 2:m)
  meanMm = meanMm + Mm[[i]]

meanMm = 1/m * meanMm

nSigma = n * sigma2

#wyniki sa podobne

#2.1

nx = 4
ny = 6

probaX = rnorm(nx)
probaY = rnorm(ny)

sx = var(probaX)
sy = var(probaY)

sp = sqrt(((nx - 1)*sx + (ny - 1)*sy) / (nx + ny - 2))
t = (mean(probaX) - mean(probaY)) / (sp * sqrt(1/nx + 1/ny))

t.test(probaX, probaY, var.equal = TRUE)
#wyniki sa takie same

#-----------------

Tm = rep(0, times = m)
for(i in 1:m)
{
  probaX = rnorm(nx)
  probaY = rnorm(ny)
  
  sx = var(probaX)
  sy = var(probaY)

  sp = sqrt(((nx - 1)*sx + (ny - 1)*sy) / (nx + ny - 2))
  t = (mean(probaX) - mean(probaY)) / (sp * sqrt(1/nx + 1/ny))
  
  Tm[i] = t
}

xfit <- seq(min(Tm), max(Tm), length = 80)
yfit <- dt(xfit, nx +ny - 2)
h <- hist(Tm, breaks = 40, col = 'coral2', freq=FALSE, ylim=c(0,0.5))
lines(xfit,yfit, col="black", lwd=2)


#2.2

TmW = rep(0, times = m)
for(i in 1:m)
{
  probaWX = rmvnorm(n, rep(0, times = 4), sigma2)
  probaWY = rmvnorm(n, rep(0, times = 4), sigma2)
  
  xMean = colMeans(probaWX)
  yMean = colMeans(probaWY)
  
  Spooled = ((nx - 1) * var(probaWX) + (ny - 1) * var(probaWY)) / (nx + ny - 2)
  
  T2 = nx*ny / (nx + ny) * t((xMean - yMean)) %*% inv(Spooled) %*% (xMean - yMean) 
  TmW[i] = T2
}

TmWS = TmW * 75

xfit <- seq(0,20, length = 4000)
yfit <- df(xfit, p, nx+ny - 1 - p)
h <- hist(TmWS, breaks = 40, col = 'coral2', freq=FALSE, ylim=c(0,0.7), xlim = c(0,20))
lines(xfit,yfit, col="black", lwd=2)
