
library(MASS)
library(ISLR)
library(stats)
library(caret)
library(glmnetUtils)

#MOJE ZADANKO

akaike<-function(fit){
  dev<- -2*logLik(fit)[1]
  k <- summary.lm(fit)$df[1]+1
  n <- nobs(fit)
  AICc <- dev+2*k+2*k*(k+1)/(n-k-1)
  AIC_ <- dev+2*k
  BIC  <- dev+log(n)*k
  LL<-logLik(fit)[1]
  res=c(AIC_, BIC, AICc,LL)
  names(res)=c("AIC", "BIC", "AICc","LL")
  return(res)
}

dane = hills

AIC = matrix(0,nrow=8,ncol=8)
BIC = matrix(0,nrow=8,ncol=8)
AICc = matrix(0,nrow=8,ncol=8)
LL = matrix(0,nrow=8,ncol=8)

cross=matrix(0,nrow=8,ncol=8)
crossLOOCV=matrix(0,nrow=8,ncol=8)

for(i in 1:8)
{
  for(j in 1:8)
  {
    f <- bquote(time ~ poly(climb,.(i))+poly(dist,.(j)))
    fit_lm=lm(f,data=dane)
    
    ak = akaike(fit_lm)
    AIC[i,j] = ak[1]
    BIC[i,j] = ak[2]
    AICc[i,j] = ak[3]
    LL[i,j] = ak[4]
    
    set.seed(123)
    fit_lm_cv=train(as.formula(f), data = dane, method = "lm",trControl = trainControl('cv',number=5))
    cross[i,j]=fit_lm_cv$results$RMSE

    fit_lm_loocv=train(as.formula(f), data = dane, method = "lm",trControl = trainControl(method = "LOOCV"))
    crossLOOCV[i,j]=fit_lm_loocv$results$RMSE
    
    print(paste(i,j,sep=' '))
  }
}

which(AIC == min(AIC), arr.ind = TRUE)
which(BIC == min(BIC), arr.ind = TRUE)
which(AICc == min(AICc), arr.ind = TRUE)
which(LL == max(LL), arr.ind = TRUE)
which(cross == min(cross), arr.ind = TRUE)
which(crossLOOCV == min(crossLOOCV), arr.ind = TRUE)

